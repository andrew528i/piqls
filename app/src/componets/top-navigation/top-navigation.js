import React from "react";
import {StyleSheet, TouchableOpacity, View, Text, Image} from "react-native";
import IconChat from "../../asset/img/chat.png"

const TopNavigation = ({goBack}) => {
    return (
        <View style={classes.container}>
            <TouchableOpacity activeOpacity={1} onPress={goBack}>
                <Text style={classes.back}>←</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={1}>
                <View style={classes.wrapper_img}>
                    <Image source={IconChat}/>
                </View>
            </TouchableOpacity>
        </View>
    )

};

const classes = StyleSheet.create({
    container: {
        display: "flex",
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: "space-between",
        paddingHorizontal: 25,
        paddingVertical: 10,
        width: "100%"
    },
    back: {
        fontSize: 30
    },
    wrapper_img: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        borderRadius: 100,
        width: 40,
        height: 40
    }
});
export default TopNavigation
