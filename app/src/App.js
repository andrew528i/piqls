import React, {Component} from 'react';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {light as theme, mapping} from '@eva-design/eva';
import CustomerDrawerNavigator from './screens/CustomerMain';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SignIn from './screens/Auth/SignIn';
import WelcomeScreen from './screens/Welcome';
import SignUpPhoneScreen from './screens/Auth/SignUp/Phone';
import {store, persistor} from './store/store';
import LoadingScreen from './screens/Loading';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import {connect} from 'react-redux';

import DropdownAlert from 'react-native-dropdownalert';
import {AlertHelper} from './screens/AlertHelper';
import SignInPhone from './screens/Auth/SignIn/Phone';
import EmployeeMainNavigator from './screens/EmployeeMain';
import {Alert, StatusBar, View} from 'react-native';
import APIClient from './core/api/client';
import axios from './axios';
import {clearAndSetCookie} from './core/cookies';
import connectWebsocket from './core/api/socket';
import SplashScreen from './screens/Splash';
import messaging from '@react-native-firebase/messaging';

const Stack = createStackNavigator();

class AppStore extends Component {
    async registerAppWithFCM() {
        await messaging().registerDeviceForRemoteMessages();
    }

    componentDidMount() {
        // this.registerAppWithFCM().then().catch(e => {
        //     console.warn('!!!!!!!!', e)
        // })
    }

    render() {
        return <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App />
                <DropdownAlert
                    ref={ref => AlertHelper.setDropDown(ref)}
                    onClose={() => AlertHelper.invokeOnClose()}
                />
            </PersistGate>
        </Provider>
    }
}

class App extends Component {
    state = {
        showSplash: true,
        initialRouteName: 'Welcome',
    }

    componentDidMount() {
        let initialRouteName = 'Welcome'
        let state = store.getState();

        // this.messageListener = messaging().onMessage((message) => {
        //     Alert.alert('A new FCM message arrived!', JSON.stringify(message))
        // });

        if (state.user && state.user.authInfo && state.user.authInfo.csrf) {
            let csrf = state.user.authInfo.csrf;
            let sessionId = state.user.authInfo.sessionId;

            axios.defaults.headers['X-CSRFToken'] = csrf;

            clearAndSetCookie({
                csrftoken: csrf,
                sessionid: sessionId,
            }, () => {
                APIClient.user.detail(resp => {
                    let user = resp.data;

                    if (user.group === 'customer') {
                        initialRouteName = 'Main'
                    } else if (user.group === 'employee') {
                        initialRouteName = 'EmployeeMain'
                    }

                    if (APIClient.socket === null) {
                        APIClient.socket = connectWebsocket(sessionId)
                    }

                    this.setState({showSplash: false, initialRouteName})
                }, () => {
                    this.setState({showSplash: false, initialRouteName})
                })
            });
        } else {
            initialRouteName = 'SignIn'
            this.setState({showSplash: false, initialRouteName})
        }
    }

    componentWillUnmount() {
        this.messageListener && this.messageListener();
    }

    render() {
        if (this.state.showSplash) {
            return <SplashScreen />
        }

        return <View style={{
            width: '100%',
            height: '100%',
        }}>
            <StatusBar hidden={false} />
          {/* //TODO: cutout iconpack */}
            <IconRegistry icons={EvaIconsPack}/>
            <ApplicationProvider mapping={mapping} theme={theme}>
                <NavigationContainer>
                    <Stack.Navigator
                        headerMode={'none'}
                        initialRouteName={this.state.initialRouteName}
                    >
                        <Stack.Screen name="Welcome" component={WelcomeScreen} />
                        <Stack.Screen
                            name="SignIn"
                            component={SignIn}
                            options={{
                                gestureEnabled: false,
                            }}
                        />
                        <Stack.Screen name="SignInPhone" component={SignInPhone} />
                        <Stack.Screen name="SignUpPhone" component={SignUpPhoneScreen} />
                        <Stack.Screen
                            name="Main"
                            component={CustomerDrawerNavigator}
                            options={{
                                gestureEnabled: false,
                            }}
                        />
                        <Stack.Screen
                            name="EmployeeMain"
                            component={EmployeeMainNavigator}
                            options={{
                                gestureEnabled: false,
                            }}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </ApplicationProvider>

            {this.props.loading ? <LoadingScreen /> : null}
        </View>
    }
}

const stateToProps = state => ({
    loading: state.loading.state,
})

const dispatchToProps = dispatch => ({})

App = connect(stateToProps, dispatchToProps)(App)

export default AppStore;
