import axios from '../../axios';
import {AlertHelper} from '../../screens/AlertHelper';
import AsyncStorage from '@react-native-community/async-storage';
import {store} from '../../store/store';
import {setLoadingState} from '../../store/loading/actions';

export default class OrderAPI {
    areaList(success, error) {
        store.dispatch(setLoadingState(true))
        axios.get('/order/area/list').then(resp => {
            if (!resp.data.results) {
                AlertHelper.show('warn', 'Cannot load zones')
            } else {
                if (success) {
                    success(resp.data)
                }
            }
        }).catch(error)
    }

    create(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/create', order)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    change(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/change', order)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    rate(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/rate', order)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    extend(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/extend', order)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    extendConfirm(data, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/extend/confirm', data)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    list(params, success, error) {
        let requestParams = {};

        if (!!params) {
            requestParams.params = params
        }

        store.dispatch(setLoadingState(true))
        axios.get('/order/list', requestParams, {
            withCredentials: true,
        }).then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    tariffList(success, error) {
        // store.dispatch(setLoadingState(true))
        axios.get('/order/tariff/list')
            .then(success)
            .catch(error)
            // .finally(() => store.dispatch(setLoadingState(false)))
    }

    state(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/state', order)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    getState(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.get('/order/state/' + order.guid)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    cancel(order, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/order/cancel', order)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }
}
