import ReconnectingWebSocket from 'reconnecting-websocket'
import EventBus from '../event_bus'
import config from '../config';
import APIClient from './client'

const baseUrl = 'ws://' + config.api_url;


export default function connectWebsocket(sessionId, callback) {
    if (APIClient.socket) {
        return
    }

    const rws = new ReconnectingWebSocket(baseUrl + '/session/' + sessionId);

    rws.onmessage = event => {
        let data = null;

        try {
            data = JSON.parse(event.data);
        } catch(e) {
            data = event.data
        }

        if (data !== null) {
            // console.warn(data.type)
            data.guid && rws.send(JSON.stringify({
                type: 'ack',
                guid: data.guid,
                data_type: data.type,
            }));

            EventBus.fireEvent(data.type, data);
        }
    };

    rws.onopen = event => {
        callback && callback()
    };

    return rws
}
