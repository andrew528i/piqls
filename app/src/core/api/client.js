import UserAPI from './user';
import OrderAPI from './order';
import AlbumAPI from './album';

class APIClient {
    static getInstance() {
        if (typeof APIClient.instance === 'object') {
            return APIClient.instance;
        }
        return new APIClient();
    }

    constructor() {
        if (typeof APIClient.instance === 'object') {
            return APIClient.instance;
        }

        this.user = new UserAPI();
        this.order = new OrderAPI();
        this.album = new AlbumAPI();
        this.socket = null;

        APIClient.instance = this;
    }
}

export default APIClient.getInstance()
