import axios from '../../axios';
import {AlertHelper} from '../../screens/AlertHelper';
import AsyncStorage from '@react-native-community/async-storage';

export default class AlbumAPI {
    upload(album, success, error, progress) {
        let formData = new FormData();

        formData.append('file', album.file);

        axios.post('/media/album/upload?guid=' + album.guid, formData, {
            headers: {
                'Content-type': 'multipart/form-data'
            },
            onUploadProgress: progress,
        }).then(success).catch(error)
    }
}
