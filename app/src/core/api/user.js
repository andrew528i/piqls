import axios from '../../axios';
import APIClient from './client';
import AsyncStorage from '@react-native-community/async-storage';
import {store} from '../../store/store';
import {info} from '../../store/user/actions';
import {setLoadingState} from '../../store/loading/actions';
import {call} from 'react-native-reanimated';
import {requestNotifications} from 'react-native-permissions';
import messaging from '@react-native-firebase/messaging';

export default  class UserAPI {

    signUpPhone(user, success, error) {
        const signUp = avatar => {
            let data = {
                phone: user.phoneNumber,
                code: user.code,
                group: user.group,
                username: user.username,
                password: user.password,
            }

            if (avatar) {
                data.avatar = avatar
            }

            if (user.firstName && user.firstName.length > 0) {
                data.first_name = user.firstName
            }

            if (user.lastName && user.lastName.length > 0) {
                data.last_name = user.lastName
            }

            store.dispatch(setLoadingState(true))
            axios.post(
                '/user/sign_up/phone',
                data,
            ).then(success)
                .catch(error)
                .finally(() => store.dispatch(setLoadingState(false)))
        }

        if (user.avatar) {
            let formData = new FormData();

            formData.append('file', user.avatar);

            store.dispatch(setLoadingState(true))
            axios({
                method: 'post',
                url: '/media/upload?type=USER_PHOTO', // TODO: add sms code
                data: formData,
                headers: {'Content-Type': 'multipart/form-data'}
            }).then(resp => {
                signUp(resp.data.guid)
            }).catch(error).finally(() => store.dispatch(setLoadingState(false)))
        } else {
            signUp(null)
        }
    }

    signIn(user, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post(
        '/user/sign_in', {
            username: user.username,
            password: user.password,
            session_id: user.session_id,
        }).then(resp => {
            store.dispatch(info(resp.data.user))
            success(resp)
        }).catch(error).finally(() => store.dispatch(setLoadingState(false)))
    }

    signOut(callback) {
        store.dispatch(setLoadingState(true))
        axios.get('/user/sign_out').finally(() => {
            if (APIClient.socket) {
                APIClient.socket.close()
            }

            callback && callback();

            store.dispatch(setLoadingState(false))
        })
    }

    requestSmsCode(phoneNumber, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/user/phone/request_code', {
            phone: phoneNumber,
            action: 'sign_in',
        }).then(success).catch(error).finally(() => store.dispatch(setLoadingState(false)))
    }

    signInPhone(phoneNumber, smsCode, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post(
        '/user/sign_in/phone', {
            phone: phoneNumber,
            code: smsCode,
        }).then(resp => {
            store.dispatch(info(resp.data.user))
            success(resp)
        }).catch(error).finally(() => store.dispatch(setLoadingState(false)))
    }

    detail(success, error) {
        store.dispatch(setLoadingState(true))
        axios.get('/user/detail').then(resp => {
            store.dispatch(info(resp.data))
            success(resp);
        }).catch(error).finally(() => store.dispatch(setLoadingState(false)))
    }

    buildRoute(params, success, error) {
        store.dispatch(setLoadingState(true))
        axios.post('/user/route/build', params)
            .then(success)
            .catch(error)
            .finally(() => store.dispatch(setLoadingState(false)))
    }

    updateLocation(location) {
        APIClient.socket && APIClient.socket.send(JSON.stringify({
            type: 'update_location',
            location,
        }))
    }

    edit(user, success, error) {
        const edit = avatar => {
            let data = {}

            user.first_name && (data.first_name = user.first_name);
            user.last_name && (data.last_name = user.last_name);
            user.email && (data.email = user.email);
            user.fcm_token && (data.fcm_token = user.fcm_token);
            avatar && (data.avatar = avatar);

            store.dispatch(setLoadingState(true))
            axios.post('/user/edit', data).then(resp => {
                store.dispatch(info(resp.data));
                success && success(resp)
            }).catch(error).finally(() => store.dispatch(setLoadingState(false)))
        }

        if (user.avatar) {
            let formData = new FormData();

            formData.append('file', user.avatar);

            store.dispatch(setLoadingState(true))
            axios({
                method: 'post',
                url: '/media/upload?type=USER_PHOTO', // TODO: add sms code
                data: formData,
                headers: {'Content-Type': 'multipart/form-data'}
            }).then(resp => {
                edit(resp.data.guid)
            }).catch(error).finally(() => store.dispatch(setLoadingState(false)))
        } else {
            edit(null)
        }
    }
}
