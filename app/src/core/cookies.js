import {Platform} from 'react-native';
import CookieManager from 'react-native-cookies';
import config from './config';


const setCookie = (cookies, callback) => {
    let promises = [];
    let host = config.api_url.split(':')[0];

    for (let [key, value] of Object.entries(cookies)) {
        let p;

        if (Platform.OS === 'ios') {
            p = CookieManager.set({
                name: key,
                value: value,
                domain: host,
                origin: host,
                path: '/',
                version: '1',
                expiration: '2029-05-30T12:30:00.00-05:00'
            });
        } else {
            p = CookieManager.setFromResponse(
                'http://' + config.api_url, // TODO: https/http -> config
                key + '=' + value,
            );
        }

        promises.push(p);
    }

    Promise.all(promises).then(callback);
};


const clearAndSetCookie = (cookies, callback) => {
    CookieManager.clearAll().then(() => {
        setCookie(cookies, callback)
    })
};

export {setCookie, clearAndSetCookie}
