import {Platform} from 'react-native'

const loadConfig = () => {
    let defaultConfig = require('../config/default');
    let debugConfig = require('../config/debug');
    let releaseConfig = require('../config/release');

    let config;

    if (__DEV__) {
        config = {...defaultConfig, ...debugConfig};

        if (Platform.OS === 'ios') {
            config.api_url = '192.168.0.72:8000'
        }
    } else {
        config = {...defaultConfig, ...releaseConfig}
    }

    return config
};

export default loadConfig()
