import {check, PERMISSIONS, request, RESULTS} from 'react-native-permissions';

const requestPermission = (permission, success, error) => {
    check(permission).then(result => {
        switch (result) {
            case RESULTS.UNAVAILABLE:
                if (error) {
                    error(RESULTS.UNAVAILABLE)
                }
                break;

            case RESULTS.DENIED:
                request(permission).then(_ => {
                    requestPermission(permission, success, error)
                });
                break;
            case RESULTS.GRANTED:
                if (success) {
                    success()
                }
                break;
            case RESULTS.BLOCKED:
                if (error) {
                    error(RESULTS.BLOCKED)
                }
                break;
        }
    })
};

export default requestPermission
