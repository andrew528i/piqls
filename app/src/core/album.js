import AsyncStorage from '@react-native-community/async-storage';

class Album {
    static getInstance() {
        if (typeof Album.instance === 'object') {
            return Album.instance
        }

        return new Album()
    }

    constructor() {
        if (typeof Album.instance === 'object') {
            return Album.instance
        }

        Album.instance = this;
        this.photos = [];
        this.selected = [];
        this.uploaded = [];
    }

    save(callback) {
        AsyncStorage
            .setItem('album', JSON.stringify({
                photos: this.photos,
                selected: this.selected,
                uploaded: this.uploaded,
            }))
            .then(callback);
    }

    load(callback) {
        AsyncStorage.getItem('album')
            .then(album => {
                let photos = [];
                let selected = [];
                let uploaded = [];

                if (album !== undefined && album !== null) {
                    album = JSON.parse(album);
                    photos = album.photos || [];
                    selected = album.selected || [];
                    uploaded = album.uploaded
                }

                this.photos = photos;
                this.selected = selected;
                this.uploaded = uploaded;

                callback(album)
            })
    }

    clear(callback) {
        this.photos = [];
        this.selected = [];
        this.uploaded = [];
        // AsyncStorage.removeItem('album').then(callback);
        this.save();
        callback && callback();
    }

    addPhoto(url) {
        if (!this.photos.includes(url)) {
            this.photos.push(url);
            this.save()
        }
    }

    addUploadedPhoto(url) {
        if (!this.uploaded.includes(url)) {
            this.uploaded.push(url);
            this.save()
        }
    }

    addUploadedPhotos(urls) {
        urls.forEach(url => this.addUploadedPhoto(url))
    }

    addPhotos(urls) {
        urls.forEach(url => this.addPhoto(url))
    }

    removePhoto(url) {
        if (this.photos.includes(url)) {
            this.photos = this.photos.filter(p => p !== url);
            this.save()
        }
    }

    selectPhoto(url) {
        if (!this.selected.includes(url)) {
            this.selected.push(url);
            this.save()
        }
    }

    selectPhotos(urls) {
        urls.forEach(url => this.selectPhoto(url))
    }

    unselectPhoto(url) {
        if (this.selected.includes(url)) {
            this.selected = this.selected.filter(p => p !== url)
            this.save()
        }
    }
}

export default Album.getInstance()
