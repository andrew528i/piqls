import {Platform} from 'react-native';
import stripe from 'tipsi-stripe';


const requestPayment = ({amount, currency, description}, success, error) => {
    amount = amount.toFixed(2).toString();

    if (Platform.OS === 'ios') {
        stripe.paymentRequestWithNativePay({},
        [{
            label: description,
            amount,
        }]).then(token => {
            stripe.completeApplePayRequest();
            success && success(token);
        })
    } else {
        stripe.paymentRequestWithNativePay({
            total_price: amount,
            currency_code: currency,
            line_items: [{
                currency_code: currency,
                description,
                total_price: amount,
                unit_price: amount,
                quantity: '1',
            }],
        }).then(success)
    }
};

export { requestPayment }
