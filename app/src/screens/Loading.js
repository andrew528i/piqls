import {Dimensions, Text, View} from 'react-native';
import React, {Component} from 'react';
import LottieView from 'lottie-react-native';

const { width, height } = Dimensions.get('screen');


class LoadingScreen extends Component {
    render() {
        return <>
            <View style={{
              position: 'absolute',
              flex: 1,
              elevation: 1000,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: "rgba(57, 55, 208, 0.25)",
              width, height
            }}>

            <LottieView
              source={require('./loading.json')}
              autoPlay
              loop
              style={{
                width: 256,
                height: 256
              }}
            />
            </View>
          </>
    }
}

export default LoadingScreen
