import React, {Component} from 'react';
import {View, Image,StatusBar} from 'react-native';
import {default as appTheme} from '../themes/light.json';

import chick from '../asset/img/splash/chick.png';
import garden from '../asset/img/splash/garden.png';
import logo from '../asset/img/splash/logo.png';
import pickle from '../asset/img/splash/pickle.png';
import pickleSlised from '../asset/img/splash/pickleSlised.png';
import shoe from '../asset/img/splash/shoe.png';

class SplashScreen extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: appTheme['color-warning-500'],
        }}>
        <StatusBar backgroundColor={appTheme['color-warning-500']} barStyle={'dark-content'}/>
        <Image
          source={chick}
          style={{position: 'absolute', left: 0, top: 12}}
        />
        <Image
          source={pickleSlised}
          style={{position: 'absolute', right: 73, top: 181}}
        />
        <Image source={logo} />
        <Image
          source={garden}
          style={{position: 'absolute', left: 0, bottom: 28}}
        />
        <Image
          source={pickle}
          style={{position: 'absolute', left: 36, bottom: 0}}
        />
        <Image
          source={shoe}
          style={{position: 'absolute', right: 0, bottom: 0}}
        />
      </View>
    );
  }
}

export default SplashScreen;
