import React, {Component} from 'react';
import {
  Platform,
  View,
  Text,
  Animated,
  PanResponder,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {default as appTheme} from '../../themes/light';
import backgroundPhotoshoot from '../../asset/img/photoshoot.png';
import backgroundMeet from '../../asset/img/meet.png';
import backgroundEnjoy from '../../asset/img/enjoy.png';
import backgroundHead from '../../asset/img/head.png';
import backgroundSun from '../../asset/img/sun.png';
import backgroundAid from '../../asset/img/aid.png';

// TODO: add topNavigation with SKIP button

const {width, height} = Dimensions.get('window');
const SCREEN_COUNT = 3; // TODO: remove hardcode

class WelcomeScreen extends Component {
  current = 0;
  translateX = new Animated.Value(0);
  relativeX = new Animated.Value(0);

  state = {
    current: 0,
  };

  panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: () => true,
    onPanResponderMove: (e, gesture) => {
      let offsetX = this.current + this.offsetX(gesture.dx);

      if (offsetX <= 0 && offsetX >= -2) {
        this.translateX.setValue(offsetX);
        this.relativeX.setValue(Math.abs(this.offsetX(gesture.dx)));
      }
    },
    onPanResponderRelease: (e, gesture) => {
      let offsetX = this.offsetX(gesture.dx);
      let value = 0;

      if (offsetX >= 0.33) {
        value = 1;
      }

      if (offsetX <= -0.33) {
        value = -1;
      }

      this.move(value);
    },
  });

  offsetX(x) {
    return x / width;
  }

  move(value) {
    let newCurrent = this.current + value;

    if (newCurrent >= 0) {
      newCurrent = 0;
    }

    if (newCurrent <= -(SCREEN_COUNT - 1)) {
      newCurrent = -(SCREEN_COUNT - 1);
    }

    this.current = newCurrent;
    this.setState({current: newCurrent});

    Animated.timing(this.translateX, {
      toValue: this.current,
      duration: 250,
      // useNativeDriver: true,
    }).start();

    Animated.timing(this.relativeX, {
      toValue: 0,
      duration: 250,
      // useNativeDriver: true,
    }).start();
  }

  renderDots() {
    let dots = [];

    for (let i = 0; i < SCREEN_COUNT; i += 1) {
      let dotStyles = [styles.paginationDot];

      if (this.state.current === -2) {
        dotStyles.push({
          backgroundColor: 'rgba(0, 0, 0, .2)',
        });
      }
      if (this.state.current === -i) {
        dotStyles.push(styles.paginationDotActive);
        if (this.state.current === -2) {
          dotStyles.push({backgroundColor: '#000'});
        }
      }

      dots.push(<View key={'dot_' + i} style={dotStyles} />);
    }
    return <View style={styles.paginationDots}>{dots}</View>;
  }

  renderBottom() {
    const backgroundColor = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: [
        'rgba(250, 255, 0, 1)',
        'rgba(57, 55, 208, 1)',
        'rgba(255, 61, 200, 1)',
      ],
    });

    const buttonColor = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: [
        'rgba(255, 61, 200, 1)',
        'rgba(134, 250, 79, 1)',
        'rgba(57, 55, 208, 1)',
      ],
    });

    const buttonTextColor = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: [
        'rgba(252, 253, 84, 1)',
        'rgba(235, 83, 195, 1)',
        'rgba(77, 255, 15, 1)',
      ],
    });

    const titleRectangeTop = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: [90, 90, 60],
    });

    const titleRectangeWidth = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: [192, 164, 216],
    });

    const titleRectangeX = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: [-10, 46, 10],
    });

    const titleRectangeColor = this.translateX.interpolate({
      inputRange: [-2, -1, 0],
      outputRange: ['#3937D0', '#FF3DC8', '#FAFF00'],
    });

    return (
      <Animated.View style={[styles.paginationContainer, {backgroundColor}]}>
        <Animated.View
          style={[
            styles.titleRectangle,
            {
              top: titleRectangeTop,
              width: titleRectangeWidth,
              backgroundColor: titleRectangeColor,
              transform: [
                {
                  translateX: titleRectangeX,
                },
              ],
            },
          ]}
        />

        {this.renderDots()}

        {this.state.current === 0 && (
          <Text style={styles.title}>
            Choose
            {'\n'}
            <Text style={[styles.title, styles.titleHighlight]}>
              the location
            </Text>
            {'\n'}of your photoshoot
          </Text>
        )}

        {this.state.current === -1 && (
          <Text style={styles.title}>
            Meet your
            {'\n'}
            photographer
            {'\n'}
            there in 2 hours
          </Text>
        )}

        {this.state.current === -2 && (
          <Text style={styles.title}>
            <Text style={[styles.title, styles.titleHighlight]}>
              Sit back, enjoy,
              {'\n'}
              and get your photos
            </Text>
            {'\n'}
            instantly
          </Text>
        )}

        <TouchableOpacity
          onPress={() => {
            if (this.state.current === -2) {
              this.props.navigation.navigate('SignIn');
            } else {
              this.move(-1);
            }
          }}
          activeOpacity={1}>
          <Animated.View
            style={[styles.bottomButton, {backgroundColor: buttonColor}]}
            activeOpacity={1}>
            <Animated.Text
              style={{
                color: buttonTextColor,
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              {this.state.current === -2 ? 'Create account' : 'Next'}
            </Animated.Text>
          </Animated.View>
        </TouchableOpacity>
      </Animated.View>
    );
  }

  render() {
    const translateX = this.translateX.interpolate({
      inputRange: [0, SCREEN_COUNT],
      outputRange: [0, width * SCREEN_COUNT],
    });

    return (
      <Animated.View style={styles.container}>
        <Animated.View
          style={{
            flexGrow: 1,
            transform: [{translateX: translateX}],
            flexDirection: 'row',
          }}
          {...this.panResponder.panHandlers}>
          <View
            style={{
              flex: 1,
              width,
            }}>
            <ImageBackground
              source={backgroundPhotoshoot}
              style={styles.bgImage}
            />
            <Animated.Image
              source={backgroundHead}
              style={{
                position: 'absolute',
                top: (height-230) / width >= 1.55 ? 128+(height-230-582)*0.2 : 20+(height-230-width)/2,
                transform: [
                  {
                    translateX: this.translateX.interpolate({
                      inputRange: [-1, 0],
                      outputRange: [width, width / 2 - 50],
                    }),
                  },
                  {
                    scale: this.translateX.interpolate({
                      inputRange: [-1, 0],
                      outputRange: [1.5, 1],
                    }),
                  },
                ],
              }}
            />
          </View>

          <View>
            <ImageBackground source={backgroundMeet} style={styles.bgImage} />

            <Animated.Image
              source={backgroundSun}
              style={{
                position: 'absolute',
                width,
                top: -200,
                transform: [
                  {
                    translateY: this.translateX.interpolate({
                      inputRange: [-2, -1],
                      outputRange: [200, 0],
                    }),
                  },
                ],
              }}
            />
          </View>

          <View>
            <ImageBackground source={backgroundEnjoy} style={styles.bgImage} />

            <Animated.Image
              source={backgroundAid}
              style={{
                position: 'absolute',
                resizeMode: 'contain',
                bottom: height / 16,
                transform: [
                  {
                    translateX: this.translateX.interpolate({
                      inputRange: [-2, -1],
                      outputRange: [0, 258],
                    }),
                  },
                  {
                    scale: this.translateX.interpolate({
                      inputRange: [-2, -1],
                      outputRange: [1, 5],
                    }),
                  },
                ],
              }}
            />
          </View>
        </Animated.View>

        {this.renderBottom()}
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: width * SCREEN_COUNT,
    flex: 1,
  },
  paginationDot: {
    backgroundColor: 'rgba(255, 255, 255, .2)',
    width: 6,
    height: 6,
    borderRadius: 3,
    marginHorizontal: 4.5,
  },
  paginationDotActive: {
    backgroundColor: '#FFF',
  },
  paginationDots: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 24,
  },
  paginationContainer: {
    flexGrow: 0,
    width,
    paddingHorizontal: 16,
    paddingBottom: 34,
    paddingTop: 8,
    alignItems: 'stretch',
  },
  title: {
    color: '#FFF',
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    fontSize: 24,
    lineHeight: 29,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  titleHighlight: {
    color: appTheme['color-primary-500'],
  },
  titleRectangle: {
    position: 'absolute',
    alignSelf: 'center',
    height: 30,
  },
  bottomButton: {
    marginTop: 24,
    marginLeft: 24,
    marginRight: 24,
    height: 48,
    borderRadius: 0,
    backgroundColor: appTheme['color-info-500'],
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0,
  },
  bgImage: {
    flex: 1,
    width,
    resizeMode: 'cover',
  },
});

export default WelcomeScreen;
