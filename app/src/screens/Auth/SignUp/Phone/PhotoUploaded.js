import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';

import Icon from '../../../../asset/img/avatar.png'
import IconPiqls from '../../../../asset/img/piqls.png'
import APIClient from '../../../../core/api';
import StoreContext from '../../../../store/store';
import AsyncStorage from '@react-native-community/async-storage';

class PhotoUploadedScreen extends Component {

    signUp() {
        let user = this.props.route.params.user;

        // this.context.set('loading', true);

        APIClient.user.signUpPhone(user, result => this.signInSuccess(result))

        //     , e => {
        //     this.context.set('loading', false)
        // })
    }

    signInSuccess(result) { // TODO: -> APIClient.user
        let user = this.props.route.params.user;
        let data = result.data;
        let signedInUser = {...user, ...data.user};

        AsyncStorage.setItem('user', JSON.stringify(signedInUser)).then(() => {
            APIClient.user.signIn(signedInUser, result => {
                // this.context.set('loading', false);

                this.props.navigation.navigate('Main', {user: signedInUser})
            })
        })
    }

  // TODO: add animation and cleanup markup
    render() {
        return <View style={styles.container}>
            <Text style={styles.title}>Lovely!</Text>
            <View style={styles.wrapperImg}>
                <Image style={styles.img} source={this.props.route.params.user.avatar}/>
                <Image style={styles.imgPiqls} source={IconPiqls}/>
                <Text style={styles.iconOne}>🎉</Text>
                <Text style={styles.iconTwo}>🍕</Text>
                <Text style={styles.iconThree}>🤗</Text>
                {/*<Text style={styles.title}>🎉</Text>*/}
                {/*<Text style={styles.title}>🎉</Text>*/}
            </View>
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                    this.signUp()
                }}
                style={styles.btnWrapper}
            >
                <Text style={styles.btn}>Next</Text>
            </TouchableOpacity>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
    },
    title: {
        fontWeight: 'bold',
        fontSize: 24,
        lineHeight: 32,
        marginBottom: 24,
    },
    btn: {
        textAlignVertical: 'center',
        backgroundColor: '#FF3DC8',
        color: '#FAFF00',
        fontWeight: 'bold',
        fontSize: 16,
    },
    btnWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: 48,
        width: '70%',
        backgroundColor: '#FF3DC8',
    },
    img: {
        borderRadius: 100,
        width: 120,
        height: 120
    },
    wrapperImg: {
        width: 120,
        height: 120,
        borderRadius: 100,
        backgroundColor: '#F6F5FA',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 24,
    },
    imgPiqls: {
        position: 'absolute',
        top: -150,
        left: -20,
        width: 40,
        height: 40
    },
    iconOne: {
        position: 'absolute',
        fontSize: 50,
        top: -120,
        left: -100
    },
    iconTwo: {
        position: 'absolute',
        fontSize: 50,
        top: -150,
        left: 100
    },
    iconThree: {
        position: 'absolute',
        fontSize: 50,
        top: -120,
        left: 180
    },
});

export default PhotoUploadedScreen;
