import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import WelcomeScreen from '../../../Welcome';
import Index from '../../SignIn';
import CustomerDrawerNavigator from '../../../CustomerMain';
import NumberScreen from './Number';
import ActivationCodeScreen from './ActivationCode';
import UsernameScreen from './Username';
import FirstName from './FirstName';
import UploadPhotoScreen from './UploadPhoto';
import PhotoUploadedScreen from './PhotoUploaded';

const Stack = createStackNavigator()

class SignUpPhoneScreen extends Component {
    render() {
        return <Stack.Navigator>
            <Stack.Screen
                name="Number"
                component={NumberScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="ActivationCode"
                component={ActivationCodeScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Username"
                component={UsernameScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="FirstName"
                component={FirstName}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="UploadPhoto"
                component={UploadPhotoScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="PhotoUploaded"
                component={PhotoUploadedScreen}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    }
}

export default SignUpPhoneScreen
