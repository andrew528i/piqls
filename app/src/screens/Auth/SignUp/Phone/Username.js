import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  SafeAreaView,
  Keyboard,
} from 'react-native';
import axios from '../../../../axios';
import StoreContext from '../../../../store/store';
import AsyncStorage from '@react-native-community/async-storage';

import {default as appTheme} from '../../../../themes/light.json';

function generatePassword() {
  var length = 8,
    charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    retVal = '';
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
}

class UsernameScreen extends Component {
  state = {
    username: '',
  };

  signUp() {
    let user = this.props.route.params.user;

    user.group = 'customer';
    user.username = this.state.username;
    user.password = generatePassword();

    this.props.navigation.navigate('FirstName', {user});

    // axios.post('/user/sign_up/phone', {
    //     phone: user.phoneNumber,
    //     code: user.code,
    //     group: user.group,
    //     username: user.username,
    //     password: user.password,
    // }).then(result => {
    //     let data = result.data

    //
    //     AsyncStorage.setItem('user', JSON.stringify(signedInUser)).then(() => {
    //         AsyncStorage.setItem('session_id', data.session_id).then(() => {
    //             AsyncStorage.setItem('csrf', data.csrf).then(() => {
    //                 // this.props.navigation.navigate('Main', {user: signedInUser})
    //             })
    //         })
    //     })
    //
    //     // axios.post('/user/sign_in', {  // TODO: move this to auth func
    //     //     username: user.username,
    //     //     password: user.password,
    //     // }).then(result => {
    //     //     this.props.navigation.navigate('Main', {user})
    //     // }).catch(e => {
    //     //     console.warn('AUTH ERROR', e.response)
    //     //     this.props.navigation.navigate('SignIn')
    //     // }).finally(() => this.context.set('loading', false))
    //
    // }).catch(e => {
    //     console.warn('error', e.response)
    // }).finally(() => this.context.set('loading', false))
    //
    // this.context.set('loading', true)
  }

  // TODO: add topBar
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#ffffff',
          flex: 1,
        }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <View
              style={{
                flex: 1,
                paddingHorizontal: 24,
                justifyContent: 'center',
              }}>
              <View>
                <Text
                  style={{
                    fontSize: 24,
                    lineHeight: 32,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginBottom: 24,
                  }}>
                  Username to login
                </Text>
                <TextInput
                  placeholder={'veronica_carol'}
                  autoFocus={true}
                  multiline={false}
                  maxLength={20}
                  value={this.state.username}
                  onChangeText={username => {
                    this.setState({username});
                  }}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: appTheme['color-danger-500'],
                    fontSize: 20,
                    color: '#000',
                    marginBottom: 24,
                    paddingHorizontal: 8,
                    paddingVertical: 8,
                  }}
                />

                <TouchableOpacity
                  onPress={() => {
                    this.signUp();
                  }}
                  style={{
                    height: 48,
                    borderRadius: 0,
                    backgroundColor: appTheme['color-info-500'],
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0,
                  }}
                  activeOpacity={1}>
                  <Text
                    style={{
                      color: appTheme['color-success-500'],
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}>
                    Next
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

export default UsernameScreen;
