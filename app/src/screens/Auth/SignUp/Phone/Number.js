import React, {Component} from 'react';
import {Input} from '@ui-kitten/components';
import {
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  Text,
  SafeAreaView,
  View,
} from 'react-native';
import CountryPicker from 'react-native-country-picker-modal';
import Flag from 'react-native-flags';
import StoreContext from '../../../../store/store';
import axios from '../../../../axios';
import pickleStroke from '../../../../asset/img/pickleStroke.png';
import {default as appTheme} from '../../../../themes/light.json';

const {height} = Dimensions.get('window');

class NumberScreen extends Component {
  state = {
    user: {
      phoneNumber: '',
    },
    country: 'USA',
    isoCode: 'US',
    modalVisible: false,
  };

  requestCode() {
    // this.context.set('loading', true)

    axios
      .post('/user/phone/request_code', {
        phone: this.state.user.phoneNumber,
      })
      .then(result => {
        this.props.navigation.navigate('ActivationCode', {
          user: this.state.user,
        });
      }); //.catch(e => {
    // }).finally(() => this.context.set('loading', false))
  }

  // TODO: add topBar
  // TODO: избавиться от флага и сделать нормальный формат номера
  render() {
    const FlagIcon = () => (
      <TouchableOpacity
        onPress={() => {
          this.setState({modalVisible: true});
        }}>
        <Flag
          code={this.state.isoCode}
          size={24}
          type={'flat'}
          style={{alignSelf: 'center'}}
        />
      </TouchableOpacity>
    );
    //TODO: <TopNavigation goBack={this.props.navigation.goBack}/>
    //TODO: попробовать прозрачный фон в инпуте
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#ffffff',
          flex: 1,
        }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <Image
              source={pickleStroke}
              style={{
                position: 'absolute',
                top: height / 2,
                right: 54,
                height: 220,
                resizeMode: 'contain',
              }}
            />
            <View
              style={{
                flex: 1,
                paddingHorizontal: 24,
                justifyContent: 'center',
              }}>
              <View>
                <View
                  style={{
                    marginBottom: 8,
                  }}>
                  <Text
                    style={{
                      fontSize: 24,
                      lineHeight: 32,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      marginBottom: 24,
                    }}>
                    Enter your phone number
                  </Text>

                  <Text
                    style={{
                      fontSize: 20,
                      lineHeight: 28,
                      textAlign: 'right',
                      paddingRight: 8,
                    }}>
                    {this.state.country}
                  </Text>
                </View>

                <Input
                  size={"large"}
                  placeholder={"+1(999)123321"}
                  multiline={false}
                  autoFocus={true}
                  value={this.state.phoneNumber}
                  keyboardType={'phone-pad'}
                  maxLength={15}
                  icon={FlagIcon}
                  textStyle={{
                    fontSize: 20,
                    color: '#000',
                  }}
                  onChangeText={phoneNumber =>
                    this.setState({user: {phoneNumber}})
                  }
                  style={{
                    borderRadius: 0,
                    // borderColor: '#fff',
                    // borderBottomColor: appTheme['color-danger-500']
                    borderColor: '#E2E1E8',
                    backgroundColor: '#ffffff',
                    marginBottom: 24,
                  }}
                />

                <TouchableOpacity
                  onPress={() => {
                    this.requestCode();
                  }}
                  style={{
                    height: 48,
                    borderRadius: 0,
                    backgroundColor: appTheme['color-info-500'],
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderWidth: 0,
                  }}
                  activeOpacity={1}>
                  <Text
                    style={{
                      color: appTheme['color-success-500'],
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}>
                    Confirm
                  </Text>
                </TouchableOpacity>

                <CountryPicker
                  visible={this.state.modalVisible}
                  withCallingCode
                  withFilter
                  onSelect={country => {
                    this.setState({
                      isoCode: country.cca2,
                      country: country.name,
                      modalVisible: false,
                    });
                  }}
                  placeholder={''}
                  translation="eng"
                />
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

export default NumberScreen;
