import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
  Keyboard,
  StyleSheet,
  Image,
} from 'react-native';

import ImagePicker from 'react-native-image-picker';
import APIClient from '../../../../core/api';
import AsyncStorage from '@react-native-community/async-storage';
import StoreContext from '../../../../store/store';

import {default as appTheme} from '../../../../themes/light.json';

const options = {
  title: 'Select Avatar',
  quality: 0.35,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class UploadPhotoScreen extends Component {
  withPhoto() {
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let user = this.props.route.params.user;

        // this.setState({source: { uri: response.uri }});
        user.avatar = {
          uri: response.uri,
          name: 'avatar.jpg', //,response.fileName,
          type: 'image/jpeg',
          // data: response.data,
        };

        this.props.navigation.navigate('PhotoUploaded', {user});
      }
    });
  }

  signInSuccess(result) {
    // TODO: -> APIClient.user
    let user = this.props.route.params.user;
    let data = result.data;
    let signedInUser = {...user, ...data.user};

    AsyncStorage.setItem('user', JSON.stringify(signedInUser)).then(() => {
      APIClient.user.signIn(signedInUser, result => {
        // this.context.set('loading', false);

        this.props.navigation.navigate('Main', {user: signedInUser});
      });
    });
  }

  withoutPhoto() {
    let user = this.props.route.params.user;

    // this.context.set('loading', true);

    APIClient.user.signUpPhone(user, result => this.signInSuccess(result));
    // ,
    // () => this.context.set('loading', false))
  }

  // TODO: add topBar
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#ffffff',
          flex: 1,
        }}>
        <View style={styles.container}>
          <Text style={styles.title}>Add photo</Text>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.wrapperImg}
            onPress={() => this.withPhoto()}>
            <Image
              style={styles.img}
              source={require('../../../../asset/img/dug.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.withPhoto()}
            style={styles.btnWrapper}>
            <Text style={styles.btn}>Upload photo</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.withoutPhoto()}
            style={styles.bottomBtnWrapper}>
            <Text style={styles.bottomBtn}>Continue without photo</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 24,
  },
  btn: {
    color: appTheme['color-success-500'],
    fontSize: 16,
    fontWeight: 'bold',
  },
  btnWrapper: {
    height: 48,
    borderRadius: 0,
    backgroundColor: appTheme['color-info-500'],
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0,
    marginBottom: 24,
  },
  bottomBtnWrapper: {
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomBtn: {
    color: appTheme['color-info-500'],
    fontSize: 16,
    fontWeight: 'bold',
  },
  img: {
    width: 45,
    height: 45,
  },
  wrapperImg: {
    width: 120,
    height: 120,
    borderRadius: 60,
    backgroundColor: '#F6F5FA',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 24,
  },
});

export default UploadPhotoScreen;
