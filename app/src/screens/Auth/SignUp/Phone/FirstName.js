import React, {Component, useState} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  SafeAreaView,
  Keyboard,
  StyleSheet,
} from 'react-native';

import {default as appTheme} from '../../../../themes/light.json';

class FirstName extends Component {
  state = {
    firstName: '',
    lastName: '',
  };

  // TODO: add topBar
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#ffffff',
          flex: 1,
        }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <View style={styles.container}>
              <View>
                <Text style={styles.title}>What’s your name?</Text>
                <TextInput
                  autoFocus={true}
                  multiline={false}
                  maxLength={50}
                  placeholder={'First name'}
                  value={this.state.firstName}
                  style={styles.input}
                  onChangeText={firstName => this.setState({firstName})}
                />
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => {
                    let user = this.props.route.params.user;

                    let firstName = this.state.firstName;
                    let lastName = this.state.lastName;

                    if (firstName.includes(' ')) {
                      let splitted = firstName.split(' ');

                      firstName = splitted[0];
                      lastName = splitted[1];
                    }

                    user.firstName = firstName;
                    user.lastName = lastName;

                    this.props.navigation.navigate('UploadPhoto', {user});
                  }}
                  style={styles.btnWrapper}>
                  <Text style={styles.btn}>Next</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: appTheme['color-danger-500'],
    fontSize: 20,
    color: '#000',
    marginBottom: 24,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  title: {
    fontSize: 24,
    lineHeight: 32,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 24,
  },
  btn: {
    color: appTheme['color-success-500'],
    fontSize: 16,
    fontWeight: 'bold',
  },
  btnWrapper: {
    height: 48,
    borderRadius: 0,
    backgroundColor: appTheme['color-info-500'],
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0,
  },
});

export default FirstName;
