import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableWithoutFeedback,
  SafeAreaView,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
} from 'react-native';
import axios from '../../../../axios';
import StoreContext from '../../../../store/store';
import {AlertHelper} from '../../../AlertHelper';

import {default as appTheme} from '../../../../themes/light.json';

class ActivationCodeScreen extends Component {
  state = {
    code: '',
  };

  checkCode(code) {
    let user = this.props.route.params.user;

    user.code = code;

    axios
      .post('/user/phone/check_code', {
        phone: user.phoneNumber,
        code,
      })
      .then(result => {
        // console.warn(result)
        this.props.navigation.navigate('Username', {user});
      })
      .catch(e => {
        // console.warn('error', e.response)
        AlertHelper.show('error', 'Error', e.response.data.detail);
      }); //.finally(() => this.context.set('loading', false))

    // this.context.set('loading', true)
  }

  // TODO: add topBar
  // TODO: resendCode
  // TODO: сделать счётчик
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#ffffff',
          flex: 1,
        }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <View
              style={{
                flex: 1,
                paddingHorizontal: 24,
                justifyContent: 'center',
              }}>
              <View>
                <Text
                  style={{
                    fontSize: 24,
                    lineHeight: 32,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginBottom: 24,
                  }}>
                  Activation code
                </Text>

                <TextInput
                  placeholder={'123-456'}
                  autoFocus={true}
                  multiline={false}
                  value={this.state.code}
                  keyboardType={'number-pad'}
                  maxLength={6}
                  onChangeText={code => {
                    if (code.length <= 6) {
                      this.setState({code});
                    }

                    if (code.length === 6) {
                      this.checkCode(code);
                    }
                  }}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: appTheme['color-danger-500'],
                    fontSize: 20,
                    color: '#000',
                    marginBottom: 16,
                    paddingHorizontal: 8,
                    paddingVertical: 8,
                  }}
                />

                <Text
                  style={{
                    fontSize: 18,
                    lineHeight: 32,
                    color: '#9D9CA1',
                    textAlign: 'center',
                    marginBottom: 24,
                  }}>
                  The activation code has been sent to:{' '}
                  <Text
                    style={{
                      color: '#000',
                    }}>
                    {this.props.route.params.user.phoneNumber}
                  </Text>
                </Text>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => {
                    alert('Not today');
                  }}
                  style={{
                    height: 48,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      color: appTheme['color-info-500'],
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}>
                    Resend code
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

export default ActivationCodeScreen;
