import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableWithoutFeedback,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableOpacity,
  Platform,
  Keyboard,
} from 'react-native';
import axios from '../../../../axios';
import StoreContext from '../../../../store/store';
import {AlertHelper} from '../../../AlertHelper';
import APIClient from '../../../../core/api';
import AsyncStorage from '@react-native-community/async-storage';
import connectWebsocket from '../../../../core/api/socket';

import {default as appTheme} from '../../../../themes/light.json';

class ActivationCodeScreen extends Component {
  state = {code: ''};

  // checkCode(code) {
  //     axios.post('/user/phone/check_code', {
  //         phone: this.props.route.params.phone,
  //         code,
  //     }).then(result => {
  //         // console.warn(result)
  //         this.props.navigation.navigate('Username', {code, phone: })
  //     }).catch(e => {
  //         // console.warn('error', e.response)
  //         AlertHelper.show('error', 'Error', e.response.data.detail)
  //     }).finally(() => this.context.set('loading', false))
  //
  //     this.context.set('loading', true)
  // }

  signInByPhone(code) {
    let phoneNumber = this.props.route.params.phone;

    //this.context.set('loading', true);

    APIClient.user.signInPhone(phoneNumber, code, resp => {
      let data = resp.data;
      let user = data.user;

      AsyncStorage.setItem('user', JSON.stringify(user)).then(() => {
        AsyncStorage.setItem('session_id', data.session_id).then(() => {
          AsyncStorage.setItem('csrf', data.csrf).then(() => {
            APIClient.me = user;

            if (user.group === 'customer') {
              this.props.navigation.navigate('Main', {user});
            } else if (user.group === 'employee') {
              this.props.navigation.navigate('EmployeeMain', {user});
            } else {
              this.props.navigation.navigate('SignIn');
            }
          });
        });
      });

      //this.context.set('loading', false)
    });
    // , e => {
    //     this.context.set('loading', false)
    // })
  }

  // TODO: add topBar
  // TODO: resendCode
  //TODO: сделать счётчик
  render() {
    return (
      <SafeAreaView
        style={{
          backgroundColor: '#ffffff',
          flex: 1,
        }}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS == 'ios' ? 'padding' : null}>
            <View
              style={{
                flex: 1,
                paddingHorizontal: 24,
                justifyContent: 'center',
              }}>
              <View>
                <Text
                  style={{
                    fontSize: 24,
                    lineHeight: 32,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginBottom: 24,
                  }}>
                  Activation code
                </Text>
                <TextInput
                  placeholder={'123-456'}
                  autoFocus={true}
                  multiline={false}
                  value={this.state.code}
                  keyboardType={'number-pad'}
                  maxLength={6}
                  onChangeText={code => {
                    if (code.length <= 6) {
                      this.setState({code});
                    }

                    if (code.length === 6) {
                      this.signInByPhone(code);
                    }
                  }}
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: appTheme['color-danger-500'],
                    fontSize: 20,
                    color: '#000',
                    marginBottom: 16,
                    paddingHorizontal: 8,
                    paddingVertical: 8,
                  }}
                />
                <Text
                  style={{
                    fontSize: 18,
                    lineHeight: 32,
                    color: '#9D9CA1',
                    textAlign: 'center',
                    marginBottom: 24,
                  }}>
                  The activation code has been sent to:{' '}
                  <Text
                    style={{
                      color: '#000',
                    }}>
                    {this.props.route.params.phone}
                  </Text>
                </Text>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => {
                    alert('Not today');
                  }}
                  style={{
                    height: 48,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      color: appTheme['color-info-500'],
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}>
                    Resend code
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    );
  }
}

export default ActivationCodeScreen;
