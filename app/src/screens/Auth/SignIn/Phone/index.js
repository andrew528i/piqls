import React, {Component} from 'react'
import NumberScreen from '../../SignIn/Phone/Number';
import ActivationCodeScreen from '../../SignIn/Phone/ActivationCode';
import UsernameScreen from '../../SignUp/Phone/Username';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator()

class SignInPhone extends Component {
    render() {
        return <Stack.Navigator>
            <Stack.Screen
                name="Number"
                component={NumberScreen}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="ActivationCode"
                component={ActivationCodeScreen}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    }
}

export default SignInPhone
