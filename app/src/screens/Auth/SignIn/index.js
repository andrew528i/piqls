import React, {Component} from 'react';
import {
  Platform,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  SafeAreaView,
  BackHandler,
  Text
} from 'react-native';

import {default as appTheme} from '../../../themes/light.json';

import iconChat from '../../../asset/icon/chat.png';
import iconGoogle from '../../../asset/icon/google.png';
import iconFacebook from '../../../asset/icon/facebook.png';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appTheme['color-warning-500'],
  },
  screen: {
    flex: 1,
  },
  wrapper: {
    flexGrow: 1,
    justifyContent: 'center',
    marginHorizontal: 24,
  },
  wrapperText: {
    marginBottom: 32,
  },
  title: {
    color: appTheme['color-danger-500'],
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    fontSize: 30,
    lineHeight: 36,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 8,
  },
  title80: {
    color: appTheme['color-danger-transparent-600'],
  },
  title60: {
    color: appTheme['color-danger-transparent-500'],
  },
  title40: {
    color: appTheme['color-danger-transparent-400'],
    marginBottom: 0,
  },
  button: {
    paddingLeft: 24,
    paddingRight: 56,
    paddingVertical: 12,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: appTheme['color-info-500'],
    marginBottom: 16,
  },
  buttonGoogle: {
    backgroundColor: appTheme['color-danger-500'],
  },
  buttonPhone: {
    backgroundColor: '#fff',
    paddingRight: 24,
    marginBottom: 0,
  },
  buttonText: {
    color: '#fff',
    flexGrow: 1,
    fontSize: 20,
    lineHeight: 32,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  buttonTextPhone: {
    color: appTheme['color-info-500'],
  },
  bottomLink: {
    paddingVertical: 24,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  bottomLinkDesc: {
    color: appTheme['color-primary-500'],
    fontSize: 20,
    lineHeight: 32,
  },
  bottomLinkSign: {
    color: appTheme['color-danger-500'],
    fontWeight: 'bold',
  },
  Icon32: {
    height: 32,
    width: 32,
  },
  topNavigation: {
    position: 'absolute',
    height: 44,
    alignItems: 'center',
    paddingHorizontal: 6,
    marginTop: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'flex-end',
    flexGrow: 1,
    flexShrink: 1,
  },
  Icon24: {
    height: 24,
    width: 24,
    margin: 8,
  },
  topNavigationRight: {
    width: 40,
    height: 40,
    margin: 2,
    backgroundColor: '#FFF',
    borderRadius: 20,
    //android shadow
    elevation: 4,
    //ios shadow
    shadowColor: appTheme['color-primary-500'],
    shadowOpacity: 0.6,
    shadowRadius: 14,
    shadowOffset: {width: 0, height: 2},
  },
});

class Index extends Component {
  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.screen}>
          <View style={styles.wrapper}>
            <View style={styles.wrapperText}>
              <Text style={styles.title}>Join PIQLS</Text>
              <Text style={[styles.title, styles.title80]}>Join</Text>
              <Text style={[styles.title, styles.title60]}>Join ...</Text>
              <Text style={[styles.title, styles.title40]}>...</Text>
            </View>

            <TouchableOpacity style={styles.button}>
              <Image source={iconFacebook} style={styles.Icon32} />
              <Text style={styles.buttonText}>Facebook</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button, styles.buttonGoogle]}>
              <Image source={iconGoogle} style={styles.Icon32} />
              <Text style={styles.buttonText}>Google</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.buttonPhone]}
              onPress={() => {
                this.props.navigation.navigate('SignUpPhone');
              }}>
              <Text style={[styles.buttonText, styles.buttonTextPhone]}>
                Phone number
              </Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            style={styles.bottomLink}
            onPress={() => {
              this.props.navigation.navigate('SignInPhone');
            }}>
            <Text style={styles.bottomLinkDesc}>Have an account?</Text>
            <Text style={[styles.bottomLinkDesc, styles.bottomLinkSign]}>
              {' '}
              Sign in
            </Text>
          </TouchableOpacity>

          {/* <View style={styles.topNavigation}>
							<View style={styles.topNavigationRight}>
								<Image source={iconChat} style={styles.Icon24}/>
							</View>
						</View> */}
        </View>
      </SafeAreaView>
    );
  }
}

export default Index;
