import React, {Component} from 'react'
import {View, ScrollView, RefreshControl, TouchableOpacity, Text} from 'react-native'
import APIClient from '../../../core/api';
import StoreContext from '../../../store/store';


class OrderListScreen extends Component {

    state = {
        orders: [],
        refreshing: false,
    };

    loadOrders() {
        this.setState({refreshing: true})

        APIClient.order.list({mine: 1}, resp => {
            this.setState({orders: resp.data.results, refreshing: false})
        })
    }

    componentDidMount() {
        this.loadOrders()
    }

    render() {
        return <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity
                onPress={() => {
                    APIClient.user.signOut(() => {
                        this.props.navigation.navigate('SignIn')
                    })
                }}
            >
                <Text>Hello world</Text>
            </TouchableOpacity>

            <ScrollView
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => {
                    this.loadOrders()
                }} />}
            >
                {this.state.orders.map((order, idx) => {
                    return <View key={'order_item_' + idx} style={{marginBottom: 50}}>
                        <Text>Customer: {order.customer.username}</Text>
                        <Text>Time: {order.started_at}</Text>
                    </View>
                })}
            </ScrollView>
        </View>
    }
}

export default OrderListScreen
