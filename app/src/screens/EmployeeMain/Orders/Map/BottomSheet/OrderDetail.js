import React, {Component} from 'react'
import {View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, Alert, Platform, AppState} from 'react-native';
import {formatAMPM} from '../../../../../core/time';
import APIClient from '../../../../../core/api';
import {AlertHelper} from '../../../../AlertHelper';
import EventBus from '../../../../../core/event_bus';
import {RNCamera} from 'react-native-camera';
import ProgressCircle from 'react-native-progress-circle';
import BottomSheet from 'reanimated-bottom-sheet';
import Album from '../../../../../core/album';

const { width, height } = Dimensions.get('screen');
const expandedSnapPoints = [height, height];

class OrderDetailBottomSheet extends Component {

    bs = React.createRef();
    barcodeRead = false;
    onClose = null;

    state = {
        order: {
            address: '',
        },
        renderSuccess: false,
        percent: 0,
        heightContent: height,
        showCancel: false,
        snapPoints: expandedSnapPoints,
    };

    updateOrder() {
        if (!this.state.order || !this.state.order.guid) {
            return
        }

        APIClient.order.getState({guid: this.state.order.guid}, resp => {
            this.setState({order: resp.data})
        })
    }

    componentDidMount() {
        AppState.addEventListener('change', this.appStateListener = nextAppState => {
            if (
                this.appState &&
                this.appState.match(/inactive|background/) &&
                nextAppState === 'active'
            ) {
                this.updateOrder()
            }

            this.appState = nextAppState
        })

        EventBus.addListener('order_state', this.orderStateListener = data => {
            let order = {...this.state.order, ...data.order};

            if (data.order.guid !== this.state.order.guid) {
                return
            }

            if (order.state === 'CANCELLED' || order.state === 'DONE') {
                this.closePanel()
                return
            }

            order.meta = data.order.meta;
            order.location = {
                latitude: order.location.coordinates[0],
                longitude: order.location.coordinates[1],
            };

            if (this.state.order.state !== order.state) {
                // this.context.set('loading', false);
                this.setState({percent: 0})
            }

            this.setState({order})
        });

        this.interval = setInterval(() => {
            if (this.state.order.meta) {
                let delta = this.state.order.meta.delta;

                if (this.state.percent >= 100) {
                    // this.context.set('loading', true);
                    return
                }

                let percent = Math.max(this.state.order.meta.percent, this.state.percent + (delta / 4));

                if (percent >= 0) {
                    this.setState({percent})
                }
            }
        }, 250);
    }

    componentWillUnmount() {
        this.appStateListener && AppState.removeEventListener('change', this.appStateListener);

        EventBus.removeListener(this.orderStateListener);
        this.interval && clearInterval(this.interval);
    }

    takeOrder() {
        let orderGuid = this.state.order.guid;

        APIClient.order.state({
            guid: orderGuid,
            state: 'ASSIGNED',
        }, resp => {
            this.props.onTake && this.props.onTake(orderGuid)
            this.setState({renderSuccess: true})

            Album.clear(() => {
                setTimeout(() => {
                    this.setState({renderSuccess: false})
                }, 2000)
            })
        }, e => {
            console.warn(e)
            AlertHelper.show('error', 'Failed to take order', 'Please try again later');
        })
    }

    readyOrder() {
        // this.context.set('loading', true);

        APIClient.order.state({
            guid: this.state.order.guid,
            state: 'READY',
        }, resp => {
            this.props.loadOrders({mine: 1})
            // this.context.set('loading', false);
        }) //, () => this.context.set('loading', false));
    }

    doneOrder() {
        // this.context.set('loading', true);

        APIClient.order.state({
            guid: this.state.order.guid,
            state: 'DONE',
        }, resp => {
            // this.context.set('loading', false);
            this.props.onDone && this.props.onDone();
            this.props.loadOrders && this.props.loadOrders();
        }) //, () => this.context.set('loading', false));
    }

    completeOrder() {
        APIClient.order.state({
            guid: this.state.order.guid,
            state: 'COMPLETED',
        }, resp => {
        }, () => {
            AlertHelper.show('error', 'Failed to complete order', 'Please try again later');
        })
    }

    renderCancel() {
        let order = this.state.order;

        return <View
            style={{
                position: 'absolute',
                // top: 0,
                backgroundColor: '#3E3D3B',
                width: '100%',
                height: height + 22, // sorry for that
                paddingTop: '10%',
                // elevation: 1000,
            }}
        >
            <TouchableOpacity
                onPress={() => {
                    this.setState({showCancel: false})
                }}
                style={{alignItems: 'flex-end', paddingRight: 15, paddingTop: 20}}
            >
                <Text style={{fontSize: 40, fontWeight: 'normal', color: 'white'}}>&times;</Text>
            </TouchableOpacity>
            <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', padding: 10}}>
                <Text style={{fontFamily: Platform.OS === 'ios' ? "SF Mono" : "monospace", paddingTop: '10%', fontSize: 25, textAlign: 'center', fontWeight: 'bold', color: 'white'}}>Are you sure that you want to cancel the order?</Text>
                <Text style={{paddingTop: '5%', fontSize: 32, textAlign: 'center', fontWeight: 'normal', color: 'white'}}>{order.address.split(',')[0]}</Text>
                <Text style={{fontSize: 30, textAlign: 'center', fontWeight: 'bold', color: 'white'}}>at {order.started_at ? formatAMPM(new Date(order.started_at)) : null}</Text>

            </View>
            <View style={{paddingHorizontal: 20, marginTop: '10%'}}>
                <TouchableOpacity
                    onPress={() => {
                        APIClient.order.cancel({
                            guid: this.state.order.guid,
                        }, resp => {
                            this.setState({showCancel: false});
                            this.props.loadOrders();

                            if (['WARM_UP', 'PROCESSING'].includes(order.state)) {
                                this.completeOrder();
                            } else {
                                this.closePanel();
                                this.props.onCancel && this.props.onCancel();
                            }
                        })
                    }}
                    style={{backgroundColor: '#27AE60', height: 56, justifyContent: 'center', alignItems: 'center', marginBottom: 20}}
                >
                    <Text style={{color: 'white', fontWeight: '500', fontSize: 20}}>Cancel order</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.setState({showCancel: false});
                    }}
                    style={{backgroundColor: '#EB5757', height: 56, justifyContent: 'center', alignItems: 'center'}}
                >
                    <Text style={{color: 'white', fontWeight: '500', fontSize: 20}}>No, we're still on</Text>
                </TouchableOpacity>
            </View>
        </View>
    }

    render() {
        if (this.state.showCancel) {
            return this.renderCancel()
        }

        if (!this.state.order.guid) {
            return null
        }

        return <BottomSheet
            ref={this.bs}
            snapPoints={this.state.snapPoints}
            renderContent={this.renderContent.bind(this)}
            onCloseEnd={() => {
                this.barcodeRead = false;
                // EventBus.fireEvent('bs_close');

                if (this.onClose) {
                    this.onClose()
                    this.onClose = null
                }
            }}
            enabledGestureInteraction={true}
            style={{
                // position: 'absolute',
                // bottom: 0,
                // width,
            }}
        />
    }

    renderTakeOrder() {
        let order = this.state.order;
        let addressShort = order.address;
        let addressFull = '';
        let splitted = order.address.split(',');
        // let startedAt = new Date(order.started_at);
        // let minutesToStart = (startedAt.getTime() - (new Date()).getTime()) / 60000;

        if (splitted.length > 0) {
            addressShort = splitted[0];
            splitted.splice(0, 1);
            addressFull = splitted.join(',')
        }

        return (
            <View
                onLayout={e => {
                    let height = e.nativeEvent.layout.height;
                    let snapPoints = [0, height];

                    if (this.getSnapPoints() !== snapPoints) {
                        this.setSnapPoints(snapPoints);
                    }
                }}
                style={styles.container}
            >
                <Text style={styles.meetClient}>Meet client</Text>
                <View style={styles.address}>
                    <Text style={styles.addressTextTop}>{addressShort}</Text>
                    <Text>{addressFull}</Text>
                </View>

                <Text style={styles.time}>{formatAMPM(new Date(order.started_at))}</Text>

                <Text>Photoshoot duration {order.duration} min</Text>

                <Text style={styles.price}>you earn {order.amount} {order.currency}</Text>

                <TouchableOpacity
                    onPress={() => this.takeOrder()}
                    style={styles.btn}
                >
                    <Text style={styles.btnText}>Accept order</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderSuccess() {
        return (
            <View
                onLayout={e => {
                    let height = e.nativeEvent.layout.height;
                    let snapPoints = [0, height];

                    if (this.getSnapPoints() !== snapPoints) {
                        this.setSnapPoints(snapPoints, true);
                    }
                }}
                style={styles.container}
            >

                <View style={styles.address}>
                    <Text style={styles.addressTextTop}>Success</Text>
                </View>

                <Image
                    source={require('../../../../../asset/img/piqls.png')}
                />

                <Text style={styles.assignedText}>You've been assigned</Text>

            </View>
        )
    }

    renderAssigned() {
        let order = this.state.order;
        let addressShort = order.address;
        let addressFull = '';
        let splitted = order.address.split(',');
        let startedAt = new Date(order.started_at);
        let minutesToStart = ((startedAt.getTime() - (new Date()).getTime()) / 60000).toFixed(0);
        let minutesLeft = minutesToStart % 60;
        let hoursLeft = ((minutesToStart - minutesLeft) / 60);
        let formattedLeft = '';

        if (hoursLeft > 0) {
            formattedLeft += hoursLeft + 'h ';
        }

        if (minutesLeft > 0) {
            formattedLeft += minutesLeft + 'm left';
        }

        if (splitted.length > 0) {
            addressShort = splitted[0];
            splitted.splice(0, 1);
            addressFull = splitted.join(',');
        }

        let isFocused = this.props.navigation.isFocused() && this.state.order.state === 'READY';

        return (
            <View
                onLayout={e => {
                    let height = e.nativeEvent.layout.height;
                    let snapPoints = [0, height];

                    if (this.getSnapPoints() !== snapPoints) {
                        this.setSnapPoints(snapPoints, true);
                    }
                }}
                style={styles.container}
            >
                <Text style={styles.meetClient}>Meet client</Text>
                <View style={styles.address}>
                    <Text style={styles.addressTextTop}>{addressShort}</Text>
                    <Text>{addressFull}</Text>
                </View>
                <Text style={styles.time}>{formatAMPM(startedAt)}</Text>
                <Text>Photoshoot durating {order.duration} min</Text>
                <Text style={styles.price}>you earn {order.amount} {order.currency}</Text>

                {this.state.order.state === 'READY' ?
                    <View style={{alignItems: 'center', width: '100%'}}>
                        <View style={{width: 250, height: 250, overflow: 'hidden'}}>

                            {isFocused && <RNCamera
                                ref={ref => {
                                    this.camera = ref;
                                }}
                                style={{
                                    height: '100%',
                                    width: '100%',
                                }}
                                onBarCodeRead={code => {
                                    // this.context.set('loading', true);
                                    if (!this.barcodeRead) {
                                        let guid = code.data;

                                        if (guid !== this.state.order.guid) {
                                            // this.context.set('loading', false);
                                            Alert.alert(
                                                'Wrong QR code',
                                                'Please try again',
                                                [
                                                    {text: 'OK', onPress: () => {
                                                            this.barcodeRead = false;
                                                            // this.context.set('loading', false);
                                                        }},
                                                ]);

                                            this.barcodeRead = true;
                                            return
                                        }

                                        // this.context.set('loading', true);
                                        APIClient.order.state({
                                            guid,
                                            state: 'WARM_UP',
                                        }) //, resp => this.context.set('loading', false),
                                        // this.context.set('loading', false));
                                    }
                                    //
                                    this.barcodeRead = true;
                                }}
                            />}
                        </View>

                    </View> :
                    <View style={{alignItems: 'center', width: '100%'}}>
                        <Text style={styles.timeLeft}>
                            {formattedLeft}
                        </Text>

                        <View style={styles.buttonsWrapper}>

                            <TouchableOpacity
                                onPress={() => {
                                    if (!this.state.order || !this.state.order.location) {
                                        return
                                    }

                                    let orderLocation = this.state.order.location

                                    if (orderLocation.coordinates) {
                                        orderLocation = {
                                            latitude: this.state.order.location.coordinates[0],
                                            longitude: this.state.order.location.coordinates[1],
                                        };
                                    }

                                    APIClient.user.buildRoute({
                                        from_location: this.props.location,
                                        to_location: orderLocation,
                                    }, resp => {
                                        let polyline = resp.data.polyline;

                                        if (polyline && polyline.length > 0) {
                                            this.setState({route: polyline});
                                        }

                                        if (polyline && polyline.length > 0) {
                                            this.props.onBuildRoute(polyline);
                                        }
                                    });
                                }}
                                style={{alignItems: 'center'}}
                            >
                                <View style={{width: 48, height: 48, marginBottom: 5}}>

                                    <Image
                                        source={require('./../../../../../asset/img/route_line.png')}
                                        style={{
                                            width: 48,
                                            height: 48,
                                        }}
                                    />

                                </View>
                                <Text>Show directions</Text>
                            </TouchableOpacity>

                            <View style={{alignItems: 'center'}}>

                                <Image
                                    source={require('./../../../../../asset/img/phone.png')}
                                    style={{
                                        width: 48,
                                        height: 48,
                                    }}
                                />

                                <Text>Call</Text>
                                <Text>client</Text>
                            </View>

                            <View style={{alignItems: 'center'}}>

                                <Image
                                    source={require('./../../../../../asset/img/smile.png')}
                                    style={{
                                        width: 48,
                                        height: 48,
                                    }}
                                />

                                <Text>Chat with</Text>
                                <Text>us</Text>
                            </View>

                        </View>

                        {this.state.order.state === 'ASSIGNED' && <TouchableOpacity
                            onPress={() => this.readyOrder()}
                            style={styles.btn}
                        >
                            <Text style={styles.btnText}>I'm here</Text>
                        </TouchableOpacity>}
                    </View>
                }


                <TouchableOpacity
                    onPress={() => {
                        this.setState({showCancel: true})
                    }}
                    style={{...styles.btn, backgroundColor: 'white'}}
                >
                    <Text style={{...styles.btnText, color: '#FF0000'}}>Cancel order</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderWarmUp() {
        // TODO: move to helper func in core
        let order = this.state.order;
        let addressShort = order.address;
        let addressFull = '';
        let splitted = order.address.split(',');

        if (splitted.length > 0) {
            addressShort = splitted[0];
            splitted.splice(0, 1);
            addressFull = splitted.join(',');
        }

        return (
            <View
                onLayout={e => {
                    let height = e.nativeEvent.layout.height;
                    let snapPoints = [0, height];

                    if (this.getSnapPoints() !== snapPoints) {
                        this.setSnapPoints(snapPoints, true);
                    }
                }}
                style={styles.container}
            >
                <Text style={styles.meetClient}>Meet client</Text>
                <View style={styles.address}>
                    <Text style={styles.addressTextTop}>{addressShort}</Text>
                    <Text>{addressFull}</Text>
                </View>

                <View style={styles.timer}>
                    <ProgressCircle
                        percent={this.state.percent || 0}
                        radius={70}
                        borderWidth={10}
                        shadowColor="#FAFF00"
                        color="#fff"
                        bgColor="#fff"
                    >
                        <Text style={styles.timerText}>{(order.meta.remain / 60).toFixed(0)}</Text>
                        <Text style={styles.timerTextMinutes}>min</Text>
                    </ProgressCircle>
                </View>

                <Text style={styles.title}>WARMING UP</Text>
                <Text style={{
                    fontSize: 18,
                    width: '85%',
                    marginTop: '5%',
                    lineHeight: 17.28,
                }}>
                    Let your client dress up, do hair dress and put lipstick to be a hot chick
                </Text>

                <View style={{flex: 1}}/>

                <View style={styles.buttonsWrapper}>
                    <View style={{alignItems: 'center'}}>
                        <Image
                            source={require('./../../../../../asset/img/smile.png')}
                            style={{
                                width: 48,
                                height: 48,
                            }}
                        />
                        <Text>Chat with</Text>
                        <Text>us</Text>
                    </View>
                </View>

                <TouchableOpacity
                    onPress={() => {
                        this.setState({showCancel: true})
                    }}
                    style={{...styles.btn, backgroundColor: 'white'}}
                >
                    <Text style={{...styles.btnText, color: '#FF0000'}}>Finish the shoot</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderProcessing() {
        // TODO: move to helper func in core
        let order = this.state.order;
        let addressShort = order.address;
        let addressFull = '';
        let splitted = order.address.split(',');

        if (splitted.length > 0) {
            addressShort = splitted[0];
            splitted.splice(0, 1);
            addressFull = splitted.join(',');
        }

        return (
            <View
                onLayout={e => {
                    let height = e.nativeEvent.layout.height;
                    let snapPoints = [0, height];

                    if (this.getSnapPoints() !== snapPoints) {
                        this.setSnapPoints(snapPoints, true);
                    }
                }}
                style={styles.container}
            >
                <Text style={styles.meetClient}>Meet client</Text>
                <View style={styles.address}>
                    <Text style={styles.addressTextTop}>{addressShort}</Text>
                    <Text>{addressFull}</Text>
                </View>

                <View style={styles.timer}>
                    <ProgressCircle
                        percent={this.state.percent || 0}
                        radius={70}
                        borderWidth={10}
                        shadowColor="#FAFF00"
                        color="#fff"
                        bgColor="#fff"
                    >
                        <Text style={styles.timerText}>{(order.meta.remain / 60).toFixed(0)}</Text>
                        <Text style={styles.timerTextMinutes}>min</Text>
                    </ProgressCircle>
                </View>

                <Text style={styles.title}>Shoot is ongoing</Text>
                <Text style={{
                    fontSize: 18,
                    width: '85%',
                    marginTop: '5%',
                    lineHeight: 17.28,
                }}>
                    Let your client dress up, do hair dress and put lipstick to be a hot chick
                </Text>

                <View style={{flex: 1}}/>

                <View style={styles.buttonsWrapper}>
                    <View style={{alignItems: 'center'}}>
                        <Image
                            source={require('./../../../../../asset/img/smile.png')}
                            style={{
                                width: 48,
                                height: 48,
                            }}
                        />
                        <Text>Chat with</Text>
                        <Text>us</Text>
                    </View>
                </View>

                <TouchableOpacity
                    onPress={() => {
                        this.setState({showCancel: true})
                    }}
                    style={{...styles.btn, backgroundColor: 'white'}}
                >
                    <Text style={{...styles.btnText, color: '#FF0000'}}>Finish the shoot</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderCompleted() {
        // TODO: move to helper func in core
        let order = this.state.order;
        let addressShort = order.address;
        let addressFull = '';
        let splitted = order.address.split(',');

        if (splitted.length > 0) {
            addressShort = splitted[0];
            splitted.splice(0, 1);
            addressFull = splitted.join(',');
        }

        return (
            <View
                onLayout={e => {
                    let height = e.nativeEvent.layout.height;
                    let snapPoints = [0, height];

                    if (this.getSnapPoints() !== snapPoints) {
                        this.setSnapPoints(snapPoints, true);
                    }
                }}
                style={styles.container}
            >
                <Text style={styles.meetClient}>Meet client</Text>
                <View style={styles.address}>
                    <Text style={styles.addressTextTop}>{addressShort}</Text>
                    <Text>{addressFull}</Text>
                </View>

                <Text style={styles.title}>Time for shooting is over</Text>
                <Text style={{
                    fontSize: 18,
                    width: '85%',
                    marginTop: '5%',
                    lineHeight: 17.28,
                }}>
                    If client wants continue the shooting the he shouls push the button in his phone and you can continue
                    shooting
                </Text>

                <View style={styles.buttonsWrapper}>
                    <View style={{alignItems: 'center'}}>
                        <Image
                            source={require('./../../../../../asset/img/smile.png')}
                            style={{
                                width: 48,
                                height: 48,
                            }}
                        />
                        <Text>Chat with</Text>
                        <Text>us</Text>
                    </View>
                </View>

                <TouchableOpacity
                    onPress={() => {
                        Album.load(album => {
                            if (album.photos.length) {
                                Alert.alert('Attention', 'Please upload your photos!', [{text: 'OK'}])
                                this.props.navigation.navigate('Album')
                            } else {
                                Album.clear(() => {
                                    this.doneOrder()
                                })
                            }
                        })
                    }}
                    style={{...styles.btn,}}
                >
                    <Text style={styles.btnText}>Stop for now</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderContent() {
        if (this.state.renderSuccess) {
            return this.renderSuccess()
        }

        switch (this.state.order.state) {
            case 'PAID': {
                return this.renderTakeOrder()
            }
            case 'ASSIGNED':
            case 'READY': {
                return this.renderAssigned()
            }
            case 'WARM_UP': {
                return this.renderWarmUp()
            }
            case 'PROCESSING': {
                return this.renderProcessing()
            }
            case 'COMPLETED': {
                return this.renderCompleted()
            }
            default: {
                return null
            }
        }
    }

    // showViewContainer = () => {
    //     return (
    //         <View style={{width: '100%', position: 'absolute', bottom: 0}}>
    //             {this.renderContent()}
    //         </View>)
    // }

    openPanel(order) {
        order && this.setState({order});
        this.barcodeRead = false;
        // this.updateOrder()
        this.open();
    }

    open() {
        if (!this.bs.current) {
            return
        }

        let idx = this.state.snapPoints.length - 1;
        this.bs.current.snapTo(idx);
        this.bs.current.snapTo(idx);
    }

    closePanel() {
        if (!this.bs.current) {
            return
        }

        this.bs.current.snapTo(0);
        this.bs.current.snapTo(0);
    }

    snapTo(idx) {
        if (!this.bs.current) {
            return
        }

        this.bs.current.snapTo(idx);
        this.bs.current.snapTo(idx);
    }

    setSnapPoints(snapPoints) {
        this.setState({snapPoints})
    }

    getSnapPoints() {
        return this.state.snapPoints;
    }
}

const styles = StyleSheet.create({
    leftText: {
        fontSize: 20
    },
    smallContainer: {
        marginTop: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: '100%',
    },
    tips: {
        backgroundColor: '#FF005C',
        width: 48,
        height: 48,
        borderTopLeftRadius: 100,
        borderTopRightRadius: 100,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 15,
        right: 20
    },
    btnText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    btn: {
        height: 56,
        backgroundColor: '#FF3DC8',
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
    },
    buttonsWrapper: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: '5%'
    },
    timeLeft: {
        fontWeight: 'bold',
        fontSize: 40,
        color: '#6330F2'
    },
    assignedText: {
        fontSize: 25,
        marginVertical: '5%'
    },
    price: {
        fontSize: 25,
        fontWeight: 'bold',
        marginVertical: '5%'
    },
    time: {
        fontSize: 24,
        fontWeight: '500',
        marginBottom: 5
    },
    addressTextTop: {
        fontSize: 34,
        fontWeight: 'bold',
        marginBottom: 5
    },
    address: {
        paddingVertical: '5%',
        alignItems: 'center'
    },
    meetClient: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 18,
        marginTop: 20,
    },
    container: {
        backgroundColor: 'white',
        alignItems: 'center',
        width: '100%',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        paddingTop: 10,
        paddingBottom: 30,
    },
    timerText: {
        color: 'black',
        fontSize: 35,
        fontWeight: 'bold'
    },
    timerTextMinutes: {
        color: 'black',
        fontSize: 31,
        fontWeight: 'bold'
    },
    timer: {
        alignItems: 'center',
        marginTop: '5%'
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'black',
        marginTop: '5%'
    },
});


export default OrderDetailBottomSheet
