import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import React, {Component} from 'react';

import {default as appTheme} from '../../../../themes/light.json';

// TODO: move to core
const capitalize = text => text.charAt(0).toUpperCase() + text.slice(1);

class TabBar extends Component {
  state = {
    active: 'catalogue',
    menu: [
      {
        label: 'My orders',
        name: 'myOrders',
      },
      {
        label: 'Catalogue',
        name: 'catalogue',
      },
    ],
  };

  render() {
    return (
      <View style={{flexDirection: 'row'}}>
        {this.state.menu.map((e, idx) => (
          <TouchableOpacity
            key={'tab_bar_' + idx}
            onPress={() => {
              let callback = this.props['on' + capitalize(e.name)];

              callback && callback();

              this.setState({active: e.name});
            }}
            style={styles.tabBar}>
            <View
              style={[
                styles.tabBarView,
                this.state.active === e.name ? styles.tabBarViewActive : null,
              ]}>
              <Text
                style={[
                  styles.tabBarText,
                  this.state.active === e.name ? styles.tabBarTextActive : null,
                ]}>
                {e.label}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    );
  }

  setActive(name) {
    this.setState({active: name});
  }
}

const styles = StyleSheet.create({
  tabBar: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "white"
  },
  tabBarText: {
    fontSize: 18,
    paddingVertical: 16,
    color: '#000c',
  },
  tabBarTextActive: {
    fontSize: 18,
    fontWeight: 'bold',
    paddingVertical: 16,
    color: appTheme['color-danger-500']
  },
  tabBarView: {
    borderBottomWidth: 0,
  },
  tabBarViewActive: {
    borderBottomColor: appTheme['color-danger-500'],
    borderBottomWidth: 3,
  },
});

export default TabBar;
