import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {formatAMPM} from '../../../../core/time';


class MapMarker extends Component {
    render() {
        let formattedAMPM = formatAMPM(new Date(this.props.order.started_at));

        let time = formattedAMPM.split(' ')[0];
        let ampm = formattedAMPM.split(' ')[1];

        return <View style={styles.container}>
            <View style={[styles.circle, {backgroundColor: this.props.color}]}/>
            <View style={[styles.square, {backgroundColor: this.props.color}]}/>
            <View style={styles.text_wrapper}>
                <Text style={[styles.text, {fontSize: 14, color: this.props.colorText}]}>{time}{'\n'}{ampm}</Text>
            </View>
        </View>
    }
}

class LocationMarker extends Component {
    render() {
        return <View style={styles.locationContainer}>
            <View style={styles.point}/>
        </View>
    }
}

const styles = StyleSheet.create({
    text_wrapper: {
        position: 'absolute',
        bottom: 22,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: 'white',
        fontWeight: 'bold',
        lineHeight: 15,
        textAlign: 'center'
    },
    container: {
        width: 56,
        height: 56,
        alignItems: 'center'
    },
    locationContainer: {
        width: 56,
        height: 56,
        backgroundColor: 'rgba(55, 55, 208, .16)',
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    point: {
        width: 12,
        height: 12,
        backgroundColor: '#FF3DC8',
        borderRadius: 100
    },
    circle: {
        position: 'absolute',
        bottom: 12,
        width: 56,
        height: 56,
        backgroundColor: 'rgb(55, 55, 208)',
        borderRadius: 100,
    },
    square: {
        position: 'absolute',
        bottom: 6,
        width: 27,
        height: 27,
        backgroundColor: 'rgb(55, 55, 208)',
        transform: [{rotate: '45deg'}],
    }
});

export {MapMarker, LocationMarker}
