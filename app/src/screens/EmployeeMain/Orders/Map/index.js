import React, {Component} from 'react';
import {
  Dimensions,
  Platform,
  SafeAreaView,
  StatusBar,
  View,
  AppState,
  Alert,
} from 'react-native';
import MapView, {Marker, Polyline, PROVIDER_GOOGLE} from 'react-native-maps';

import APIClient from '../../../../core/api';
import OrderDetailBottomSheet from './BottomSheet/OrderDetail';
import TabBar from './TabBar';
import {LocationMarker, MapMarker} from './Marker';
import {PERMISSIONS} from 'react-native-permissions';
import requestPermission from '../../../../core/permissions';
import Geolocation from 'react-native-geolocation-service';
import {OrderDateFilter, getOrderColor} from './OrderDateFilter';
import ActiveOrder from './ActiveOrder';
import EventBus from '../../../../core/event_bus';
import RNFS from 'react-native-fs';
import Album from '../../../../core/album';

const {width, height} = Dimensions.get('window');

function distance(lat1, lon1, lat2, lon2) {
  if (lat1 === lat2 && lon1 === lon2) {
    return 0;
  } else {
    let radlat1 = (Math.PI * lat1) / 180;
    let radlat2 = (Math.PI * lat2) / 180;
    let theta = lon1 - lon2;
    let radtheta = (Math.PI * theta) / 180;
    let dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

    if (dist > 1) {
      dist = 1;
    }

    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;

    return dist;
  }
}

class EmployeeMapScreen extends Component {
  appState = null;
  state = {
    location: {latitude: 0, longitude: 0},
    initialRegion: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    },
    orders: [],
    filteredOrders: [],
    activeOrder: null,
    route: [],
  };

  orderExtendConfirmShowed = false;
  onlyMine = false;
  tabBar = null;
  region = {latitude: 0, longitude: 0};

  showOrderExtendConfirm(activeOrder) {
    if (this.orderExtendConfirmShowed) {
      return;
    }

    this.orderExtendConfirmShowed = true;

    this.showOnlyMyOrders();
    this.OrderDetailBottomSheet.openPanel(activeOrder);

    Alert.alert(
      'Photoshoot extending',
      'User wants to continue. Are you agree?',
      [
        {
          text: 'No',
          onPress: () => {
            setTimeout(() => (this.orderExtendConfirmShowed = false), 10000);

            APIClient.order.extendConfirm({
              guid: activeOrder.guid,
              state: 'no',
            });
          },
        },
        {
          text: 'Yes',
          onPress: () => {
            setTimeout(() => (this.orderExtendConfirmShowed = false), 10000);
            APIClient.order.extendConfirm({
              guid: activeOrder.guid,
              state: 'yes',
            });
          },
        },
      ],
    );
  }

  componentDidMount() {
    AppState.addEventListener(
      'change',
      (this.appStateListener = nextAppState => {
        if (
          this.appState &&
          this.appState.match(/inactive|background/) &&
          nextAppState === 'active'
        ) {
          this.loadOrders();
        }

        this.appState = nextAppState;
      }),
    );

    EventBus.addListener(
      'new_order',
      (this.newOrderListener = data => {
        let orders = this.state.orders;
        let filteredOrders = this.state.filteredOrders;
        let exists = false;

        orders.forEach(o => {
          if (o.guid === data.order.guid) {
            exists = true;
          }
        });

        if (exists) {
          return;
        }

        orders.push(data.order);

        if (!this.onlyMine) {
          filteredOrders.push(data.order);
        }

        this.setState({orders, filteredOrders});
      }),
    );

    EventBus.addListener(
      'cancel_order',
      (this.cancelOrderListener = data => {
        let orders = this.state.orders;
        let filteredOrders = this.state.filteredOrders;
        let activeOrder = this.state.activeOrder;

        orders = orders.filter(o => o.guid !== data.order_guid);
        filteredOrders = filteredOrders.filter(o => o.guid !== data.order_guid);

        if (activeOrder && data.order_guid === activeOrder.guid) {
          activeOrder = null;
        }

        this.OrderDetailBottomSheet.closePanel();

        this.setState({orders, filteredOrders, activeOrder});
      }),
    );

    EventBus.addListener(
      'order_state',
      (this.orderStateListener = data => {
        const updateOrders = orders => {
          return orders
            .map(o => {
              let order = {...o, ...data.order};

              if (data.order.guid !== o.guid) {
                return o;
              }

              return order;
            })
            .filter(o => {
              if (o.state === 'CANCELLED') {
                return false;
              }

              return true;
            });
        };

        let activeOrder = this.state.activeOrder;

        if (activeOrder && data.order.guid === activeOrder.guid) {
          activeOrder = {...activeOrder, ...data.order};

          if (
            activeOrder.meta &&
            activeOrder.meta.extend_confirmed === 'requested'
          ) {
            this.showOrderExtendConfirm(activeOrder);
          }
        }

        if (activeOrder && activeOrder.state === 'CANCELLED') {
          activeOrder = null;
        }

        this.setState({
          orders: updateOrders(this.state.orders),
          filteredOrders: updateOrders(this.state.filteredOrders),
          activeOrder,
        });
      }),
    );

    let permission = null;

    if (Platform.OS === 'ios') {
      permission = PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
    } else {
      permission = PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION;
    }

    requestPermission(
      permission,
      () => {
        Geolocation.getCurrentPosition(position => {
          this.setState({location: position.coords});
          this.map.animateCamera(
            {
              center: position.coords,
              zoom: 15,
            },
            3000,
          );

          APIClient.user.updateLocation(position.coords);
        }).then();

        Geolocation.watchPosition(position => {
          this.setState({location: position.coords});
          APIClient.user.updateLocation(position.coords);
        });
      },
      () => {
        console.warn('NO WAY');
      },
    );

    this.loadOrders();
  }

  componentWillUnmount() {
    this.appStateListener &&
      AppState.removeEventListener('change', this.appStateListener);

    EventBus.removeListener(this.newOrderListener);
    EventBus.removeListener(this.cancelOrderListener);
    EventBus.removeListener(this.orderStateListener);
  }

  loadOrders(params) {
    // this.context.set('loading', true);

    let p = {};

    APIClient.order.list({active: 1}, resp => {
      let results = resp.data.results;
      let activeOrder = results[0];

      if (
        activeOrder &&
        activeOrder.meta &&
        activeOrder.meta.extend_confirmed === 'requested'
      ) {
        this.showOrderExtendConfirm(activeOrder);
      }

      EventBus.fireEvent('active_order', activeOrder);

      this.setState({activeOrder});
    });

    if (this.onlyMine) {
      p['mine'] = 1;
    }

    APIClient.order.list(params || p, resp => {
      let results = resp.data.results;

      // this.context.set('loading', false);
      this.setState({orders: results, filteredOrders: results, route: []});
    }); //, () => this.context.set('loading', false));
  }

  showOnlyMyOrders() {
    this.tabBar && this.tabBar.setActive('myOrders');
    this.onlyMine = true;
    this.loadOrders({mine: 1});
  }

  render() {
    // let params = this.props.route.params;

    // params && params.show_active &&
    // this.tabBar && !this.onlyMine && this.showOnlyMyOrders();

    return (
      <SafeAreaView
        style={{
          flex: 1,
          width,
          height,
          backgroundColor: "white"
        }}>
            
          <StatusBar backgroundColor={'white'} barStyle={'dark-content'} />

          <TabBar
            onCatalogue={() => {
              this.OrderDetailBottomSheet.closePanel();
              this.onlyMine = false;
              this.loadOrders({});
            }}
            onMyOrders={() => {
              this.OrderDetailBottomSheet.closePanel();
              this.onlyMine = true;
              this.loadOrders({mine: 1});
            }}
            ref={tabBar => {
              this.tabBar = tabBar;
            }}
          />

          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <MapView
              style={{flex: 1, width, height}}
              customMapStyle={require('./style.json')}
              provider={PROVIDER_GOOGLE}
              ref={map => {
                this.map = map;
              }}
              initialRegion={this.state.initialRegion}
              onRegionChangeComplete={e => (this.region = e)}
              showsCompass={false}
              moveOnMarkerPress={false}>
              {this.state.filteredOrders.map(order => {
                if (order.state === 'DONE') {
                  return;
                }

                let coordinate = {
                  latitude: order.location.coordinates[0],
                  longitude: order.location.coordinates[1],
                };
                const [color, colorText] = getOrderColor(order);

                return (
                  <Marker
                    key={'order_marker_' + order.guid}
                    coordinate={coordinate}
                    onPress={e => {
                      let center = e.nativeEvent.coordinate;
                      center.latitude -= 0.0045;

                      let d = distance(
                        center.latitude,
                        center.longitude,
                        this.region.latitude,
                        this.region.longitude,
                      );

                      this.map.animateCamera(
                        {center, zoom: 15},
                        {duration: 750},
                      );

                      if (d < 0.5) {
                        this.OrderDetailBottomSheet.openPanel(order);
                      } else {
                        setTimeout(() => {
                          this.OrderDetailBottomSheet.openPanel(order);
                        }, 750);
                      }

                      this.setState({route: []});
                    }}
                    anchor={{x: 0.5, y: 1}}
                    style={{
                      height: 69,
                      justifyContent: 'flex-end',
                    }}>
                    <MapMarker
                      order={order}
                      color={color}
                      colorText={colorText}
                    />
                  </Marker>
                );
              })}

              {this.state.location ? (
                <Marker
                  coordinate={this.state.location}
                  anchor={{x: 0.5, y: 0.5}}>
                  <LocationMarker />
                </Marker>
              ) : null}

              {this.state.route.length > 0 ? (
                <Polyline
                  coordinates={this.state.route}
                  strokeWidth={4}
                  strokeColor={'#4c8bf5'}
                />
              ) : null}
            </MapView>

            <OrderDateFilter
              navigation={this.props.navigation}
              orders={this.state.orders}
              onFilter={filteredOrders =>
                this.setState({filteredOrders, route: []})
              }
            />

            {this.state.activeOrder && this.onlyMine && (
              <ActiveOrder
                map={this.map}
                order={this.state.activeOrder}
                onPress={() =>
                  this.OrderDetailBottomSheet.openPanel(this.state.activeOrder)
                }
              />
            )}
          </View>
        

        <OrderDetailBottomSheet
          ref={ref => {
            this.OrderDetailBottomSheet = ref;
          }}
          loadOrders={() => this.loadOrders()}
          onBuildRoute={route => {
            this.setState({route});
            this.OrderDetailBottomSheet.closePanel();
          }}
          onTake={guid => {
            this.showOnlyMyOrders();
          }}
          onDone={() => {
            this.setState({activeOrder: null});
            this.OrderDetailBottomSheet.closePanel();
          }}
          location={this.state.location}
          {...this.props}
          style={{
            position: 'absolute',
            bottom: 0,
            width,
          }}
        />
      </SafeAreaView>
    );
  }
}

export default EmployeeMapScreen;
