import React, {Component} from 'react'
import {View, TouchableOpacity, Text} from 'react-native'


class DateBadge extends Component {
    render() {
        return <TouchableOpacity
            onPress={() => this.props.onPress()}
            style={{
                paddingVertical: 13,
                paddingHorizontal: 15,
                backgroundColor: this.props.color || '#FF3DC8',
                borderRadius: 30,
                marginLeft: 10,
            }}
        >
            <Text
                style={{
                    fontSize: 16,
                    lineHeight: 17,
                    color: this.props.colorText || '#000'
                }}
            >
                {this.props.text}
            </Text>
        </TouchableOpacity>
    }
}

export default DateBadge
