import React, {Component} from 'react'
import {ScrollView, View} from 'react-native';
import DateBadge from './DateBadge';
import APIClient from '../../../../core/api'


const filters = [
    {
        name: 'ASAP',
        color: '#FF3DC8',
        colorText: '#FFFFFF',
        check: delta => (delta >= 0 && delta < 2 * 3600 * 1000),
    }, {
        name: 'Today',
        color: '#3937D0',
        colorText: '#FFFFFF',
        check: delta => (delta >= 2 * 3600 * 1000 && delta < 24 * 3600 * 1000),
    }, {
        name: 'Tomorrow',
        color: '#FAFF00',  //'#4B8EE3',
        colorText: '#000000',
        check: delta => (delta >= 24 * 3600 * 1000 && delta < 2 * 24 * 3600 * 1000),
    }, {
        name: 'Later',
        color: '#4DFF0F',
        colorText: '#000000',
        check: delta => (delta >= 2 * 24 * 3600 * 1000),
    },
];

const getOrderColor = order => {
    let now = new Date();
    let startedAt = new Date(order.started_at);
    let delta = startedAt.getTime() - now.getTime();

    for (let i = 0; i < filters.length; i += 1) {
        let filter = filters[i];

        if (filter.check(delta)) {
            return [filter.color, filter.colorText]
        }
    }

    return 'lightsteelblue' // '#65A38A'
};

class OrderDateFilter extends Component {
    render() {
        return <View style={{
            position: 'absolute',
            width: '100%',
            top: 10,
        }}>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                {filters.map((f, idx) => <DateBadge
                    key={'date_filter_' + idx}
                    text={f.name}
                    color={f.color}
                    colorText={f.colorText}
                    onPress={() => {
                        this.props.onFilter(
                            this.props.orders.filter(o => {
                                let now = new Date();
                                let startedAt = new Date(o.started_at);
                                let delta = startedAt.getTime() - now.getTime();

                                return f.check(delta)
                            }))
                    }}
                />)}
                <DateBadge
                    onPress={() => this.props.onFilter(this.props.orders)}
                    text={'All orders'}
                    color={'#FFFFFF'}
                    colorText={'#000000'}
                />

                {/*<DateBadge*/}
                {/*    onPress={() => {*/}
                {/*        APIClient.user.signOut(() => {*/}
                {/*            this.props.navigation.navigate('SignIn')*/}
                {/*        })*/}
                {/*    }}*/}
                {/*    text={'Logout (remove me from here)'}*/}
                {/*    color={'#000000'}*/}
                {/*/>*/}

            </ScrollView>
        </View>
    }
}

export {getOrderColor, OrderDateFilter}
