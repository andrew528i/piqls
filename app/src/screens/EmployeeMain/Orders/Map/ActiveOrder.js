import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {formatAMPM} from '../../../../core/time';
import EventBus from '../../../../core/event_bus';

class ActiveOrder extends Component {
  state = {
    order: this.props.order,
  };

  centerMap() {
    let center = {
      latitude: this.state.order.location.coordinates[0],
      longitude: this.state.order.location.coordinates[1],
    };

    this.props.map.animateCamera({center, zoom: 14}, {duration: 750});
  }

  openOrder() {
    this.centerMap();

    this.props.map.getCamera().then(camera => {
      if (
        camera.center.latitude.toFixed(4) ===
          this.state.order.location.coordinates[0].toFixed(4) &&
        camera.center.longitude.toFixed(4) ===
          this.state.order.location.coordinates[1].toFixed(4)
      ) {
        this.props.onPress && this.props.onPress();
      } else {
        this.centerMap();
      }
    });

    setTimeout(() => {
      this.props.onPress && this.props.onPress();
    }, 750);
  }

  componentDidMount() {
    EventBus.addListener(
      'order_state',
      (this.orderStateListener = data => {
        let order = data.order;

        if (order.guid !== this.state.order.guid) {
          return;
        }

        this.setState({order});
      }),
    );
  }

  componentWillUnmount() {
    EventBus.removeListener(this.orderStateListener);
  }

  render() {
    let order = this.state.order;
    let splitted = order.address.split(','); // TODO: address split -> core func
    let addressShort = order.address;
    let addressFull = '';
    let startedAt = new Date(order.started_at);

    if (splitted.length > 0) {
      addressShort = splitted[0];
      splitted.splice(0, 1);
      addressFull = splitted.join(',');
    }

    let minutesToStart = (
      (startedAt.getTime() - new Date().getTime()) /
      60000
    ).toFixed(0);
    let minutesLeft = minutesToStart % 60;
    let hoursLeft = (minutesToStart - minutesLeft) / 60;
    let formattedLeft = '';

    if (hoursLeft > 0) {
      formattedLeft += hoursLeft + 'h ';
    }

    if (minutesLeft > 0) {
      formattedLeft += minutesLeft + 'm';
    }

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => this.openOrder()}
          style={styles.smallContainer}>
          {(order.state === 'ASSIGNED' || order.state === 'READY') && (
            <View style={styles.smallContainer}>
              <View>
                <Text style={{...styles.leftText, fontWeight: 'bold'}}>
                  Upcomming order
                </Text>
                <Text style={{...styles.leftText, marginVertical: 5}}>
                  {addressShort}
                </Text>
                <Text style={styles.leftText}>at {formatAMPM(startedAt)}</Text>
              </View>
              <View>
                <Text style={{...styles.timeLeft, fontSize: 30}}>
                  {formattedLeft}
                </Text>
                <Text
                  style={{
                    ...styles.timeLeft,
                    fontSize: 20,
                    textAlign: 'center',
                  }}>
                  left
                </Text>
              </View>
            </View>
          )}

          {order.state === 'WARM_UP' && (
            <View style={styles.smallContainer}>
              <View>
                <Text
                  style={{
                    ...styles.leftText,
                    fontWeight: 'bold',
                    fontSize: 26,
                  }}>
                  Warming up
                </Text>
              </View>
              <View>
                <Text style={{...styles.timeLeft, fontSize: 30}}>
                  {(this.state.order.meta.remain / 60).toFixed(0)} min
                </Text>
                <Text
                  style={{
                    ...styles.timeLeft,
                    fontSize: 20,
                    textAlign: 'center',
                  }}>
                  left
                </Text>
              </View>
            </View>
          )}

          {order.state === 'PROCESSING' && (
            <View style={styles.smallContainer}>
              <View>
                <Text
                  style={{
                    ...styles.leftText,
                    fontWeight: 'bold',
                    fontSize: 26,
                  }}>
                  Your shoot is ongoing
                </Text>
              </View>
              <View>
                <Text style={{...styles.timeLeft, fontSize: 30}}>
                  {(this.state.order.meta.remain / 60).toFixed(0)} min
                </Text>
                <Text
                  style={{
                    ...styles.timeLeft,
                    fontSize: 20,
                    textAlign: 'center',
                  }}>
                  left
                </Text>
              </View>
            </View>
          )}

          {order.state === 'COMPLETED' && (
            <View style={styles.smallContainer}>
              <View>
                <Text
                  style={{
                    ...styles.leftText,
                    fontWeight: 'bold',
                    fontSize: 22,
                  }}>
                  Your shoot is completed
                </Text>
              </View>
              <View>
                <Text style={{...styles.timeLeft, fontSize: 30}}>DONE</Text>
                <Text
                  style={{
                    ...styles.timeLeft,
                    fontSize: 20,
                    textAlign: 'center',
                  }}
                />
              </View>
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  leftText: {
    fontSize: 20,
  },
  smallContainer: {
    marginTop: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    height: '100%',
  },
  tips: {
    backgroundColor: '#FF005C',
    width: 48,
    height: 48,
    borderRadius: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 15,
    right: 20,
  },
  btnText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  btn: {
    height: 56,
    backgroundColor: '#FF3DC8',
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    marginTop: '5%',
  },
  timeLeft: {
    fontWeight: 'bold',
    fontSize: 40,
    color: '#6330F2',
  },
  assignedText: {
    fontSize: 25,
    marginVertical: '5%',
  },
  container: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'white',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-around',
    width: '100%',
    paddingHorizontal: 10,
  },
});

export default ActiveOrder;
