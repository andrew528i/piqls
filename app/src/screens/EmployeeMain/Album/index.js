import React, {Component} from 'react'
import {
    View,
    FlatList,
    StyleSheet,
    ScrollView,
    Dimensions,
    Text,
    TouchableOpacity,
    Alert,
    SafeAreaView
} from 'react-native';
import FastImage from 'react-native-fast-image'
import Icon from 'react-native-vector-icons/FontAwesome';
import Collapsible from 'react-native-collapsible';
import Album from '../../../core/album'
import RNFS from 'react-native-fs'
import EventBus from '../../../core/event_bus';
import {zip} from 'react-native-zip-archive'
import APIClient from '../../../core/api'
import * as Progress from 'react-native-progress'


const {width, height} = Dimensions.get('window');
const cellSize = (width / 2);


class AlbumScreen extends Component {

    state = {
        photos: [],
        selected: [],
        uploaded: [],
        activeOrder: {},
        progress: 0,
    };

    uploadInProgress = false;

    componentDidMount() {
        this.load();

        this.interval = setInterval(() => {
            Album.load(photos => {
                this.uploadPhotos()
            })
        }, 20000)

        EventBus.addListener('capture_photo', this.capturePhotoListener = data => {
            let photos = this.state.photos;

            photos.push(data.uri);

            this.setState({photos});
            this.save()
        });

        EventBus.addListener('active_order', this.activeOrderListenerListener = activeOrder => {
            this.setState({activeOrder: activeOrder || {}})
        });
    }

    componentWillUnmount() {
        this.save();

        this.interval && clearInterval(this.interval);

        EventBus.removeListener(this.capturePhotoListener);
        EventBus.removeListener(this.activeOrderListenerListener);
    }

    load() {
        Album.load(album => {
            this.setState({
                selected: album.selected,
                photos: album.photos,
                uploaded: album.uploaded,
            });
        });
    }

    save() {
        Album.clear(() => {
            Album.addPhotos(this.state.photos);
            Album.selectPhotos(this.state.selected);
            Album.addUploadedPhotos(this.state.uploaded);
            Album.save()
        });
    }

    uploadPhotos(success, error) {
        let cameraFolder = RNFS.CachesDirectoryPath + '/Camera';
        let zipFile = RNFS.CachesDirectoryPath + '/order-' + this.state.activeOrder.guid + '.zip';
        let photos = this.state.photos.filter(p => !this.state.uploaded.includes(p));

        if (this.uploadInProgress) {
            console.warn('Upload in progress')
            return
        }

        if (!this.state.activeOrder.guid) {
            console.warn('No active order')
            return
        }

        this.uploadInProgress = true;

        if (photos.length === 0) {
            success && success(zipFile, photos)
            this.uploadInProgress = false;
            return
        }

        RNFS.exists(zipFile).then(exists => {
            if (exists) {
                RNFS.unlink(zipFile).then();
            }

            zip(cameraFolder, zipFile).then(() => {
                let album = {
                    file: {
                        uri: 'file://' + zipFile,
                        name: 'order-' + this.state.activeOrder.guid + '.zip',
                        type: 'application/zip',
                    },
                    guid: this.state.activeOrder.guid,
                };

                APIClient.album.upload(album, resp => {
                    this.setState({progress: 0, uploaded: [...this.state.uploaded, ...photos]});
                    this.uploadInProgress = false;
                    success && success(zipFile, photos);
                }, () => {
                    this.setState({progress: 0});
                    this.uploadInProgress = false;
                    error && error();
                }, event => {
                    let progress = (event.loaded / event.total);

                    this.setState({progress})
                })
            });
        });
    }

    renderProgress() {
        return <View
            style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                backgroundColor: 'black',
                opacity: 0.7,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Progress.Bar progress={this.state.progress} width={width * 0.75} />
        </View>
    }

    render() {
        if (!this.state.activeOrder.guid) {
            return <View style={styles.container}>
                <Text>No active order</Text>
            </View>
        }

        return <SafeAreaView style={{flex: 1}}>
                <View style={styles.container}>
                    <Collapsible collapsed={this.state.selected.length === 0} style={{width: '100%'}}>
                        <View
                            style={{
                                width: '100%',
                                height: 75,
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}
                        >
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({selected: []});
                                    this.save()
                                }}
                            >
                                <Text
                                    style={{color: '#6A36FF', fontSize: 19, fontWeight: 'bold', paddingHorizontal: 20}}>Unselect
                                    all</Text>
                            </TouchableOpacity>

                    <View style={{flex: 1}} />

                            <TouchableOpacity
                                onPress={() => {
                                    Alert.alert(
                                        'Are you sure?',
                                        'Do you want to delete selected photos?', [{
                                            text: 'No',
                                            onPress: () => {
                                            }
                                        }, {
                                            text: 'Yes',
                                            onPress: () => {
                                                let selected = this.state.selected;
                                                let photos = this.state.photos.filter(url => !selected.includes(url));

                                                selected.forEach(item => {
                                                    RNFS.unlink(item).then()
                                                });

                                                Album.clear(() => {
                                                    Album.addPhotos(photos);
                                                    Album.save();
                                                    this.setState({photos, selected: []})
                                                })
                                            }
                                        }],
                                    )
                                }}
                            >
                                <Text style={{
                                    color: 'tomato',
                                    fontSize: 19,
                                    fontWeight: 'bold',
                                    paddingHorizontal: 20
                                }}>Delete</Text>
                            </TouchableOpacity>
                        </View>
                    </Collapsible>

                    <FlatList
                        onLayout={this._onLayout}
                        style={{width: '100%', height: '100%'}}
                        data={this.state.photos}
                        renderItem={item => this.renderItem(item)}
                        numColumns={2}
                        keyExtractor={item => {
                            return 'album_' + item
                        }}
                        contentContainerStyle={{paddingBottom: 100}}
                    />

                    <TouchableOpacity
                        onPress={() => {
                            // console.warn(this.state.photos);

                            if (!this.state.activeOrder.guid) {
                                Alert.alert('no active orders', 'No active order');
                                return
                            }

                            this.uploadPhotos((zipFile, photos) => {
                                Album.clear(() => {
                                    Promise.all(photos.map(photo => {
                                        return RNFS.unlink(photo)
                                    })).then();

                                    RNFS.unlink(zipFile).then();
                                    this.load();

                                    Alert.alert('Success!', 'Photos has been uploaded');

                                    this.props.navigation.navigate('Orders', {show_active: true})
                                });
                            }, () => {
                                Alert.alert('Error!', 'Please try again');
                            })

                            // Promise.all(this.state.photos.map(photo => {
                            //     console.warn(photo.replace('file://', ''));
                            //     return zip(photo.replace('file://', ''), zipFile);
                            // })).then(() => {
                            //     console.warn('OK')
                            // });

                            // zip(cameraFolder, zipFile).then(() => {
                            //     console.warn('OKOK')
                            // });

                            // console.warn(zipFile)
                        }}
                        style={styles.btn}
                    >
                        <Text style={styles.btnText}>Confirm photos</Text>
                    </TouchableOpacity>

                    {this.state.progress > 0 && this.renderProgress()}
                </View>
            </SafeAreaView>
    }

    renderItem(item) {
        return <SafeAreaView style={{flex: 1}}>
                <TouchableOpacity
                    onPress={() => {
                        let selected = this.state.selected;

                        item = item.item;

                        if (!selected.includes(item)) {
                            selected.push(item)
                        } else {
                            selected = selected.filter(i => i !== item)
                        }

                        this.setState({selected});
                        this.save();
                    }}
                    style={styles.cell}
                >
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                    >
                        <FastImage
                            source={{uri: item.item}}
                            style={{
                                width: '100%',
                                height: '100%',
                            }}
                        />

                        {this.state.selected.includes(item.item) && <View
                            style={{
                                width: 32,
                                height: 32,
                                borderRadius: 32,
                                backgroundColor: 'darkturquoise',
                                borderWidth: 2,
                                borderColor: 'white',
                                position: 'absolute',
                                bottom: 10,
                                right: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                        {/*//TODO: put native PNG icon */}
                            <Icon name={'check'} color={'white'} size={18}/>
                        </View>}
                    </View>
                </TouchableOpacity>
            </SafeAreaView>

    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    cell: {
        width: cellSize,
        height: cellSize,
        padding: 1,
    },
    btnText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    btn: {
        height: 56,
        backgroundColor: '#FF3DC8',
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
        position: 'absolute',
        bottom: 25,
    },
});

export default AlbumScreen
