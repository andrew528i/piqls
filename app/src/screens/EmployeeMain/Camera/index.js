import React, {Component} from 'react'
import {View, Text, TouchableOpacity, StatusBar, StyleSheet, Platform} from 'react-native';
import { RNCamera } from 'react-native-camera';
import StoreContext from '../../../store/store';
import EventBus from '../../../core/event_bus';
import {PERMISSIONS} from 'react-native-permissions';
import requestPermission from '../../../core/permissions';
import LottieView from "lottie-react-native";


class CameraScreen extends Component {

    state = {
        activeOrder: {},
    };

    componentDidMount() {
        EventBus.addListener('order_state', this.orderStateListener = data => {
            let order = data.order;

            if (order.state === 'WARM_UP' || order.state === 'PROCESSING') {
                EventBus.fireEvent('active_order', order);
            } else {
                EventBus.fireEvent('active_order', {});
            }

            return
        });

        EventBus.addListener('active_order', this.activeOrderListener = activeOrder => {
            this.setState({activeOrder: activeOrder || {}})
        });
    }

    componentWillUnmount() {
        EventBus.removeListener(this.activeOrderListener);
        EventBus.removeListener(this.orderStateListener);
    }

    render() {
        if (!this.state.activeOrder.guid) {
            return <View style={styles.container}>
                <Text>No active order</Text>
            </View>
        }

        let isFocused = this.props.navigation.isFocused() && ['WARM_UP', 'PROCESSING'].includes(this.state.activeOrder.state);

        if (isFocused) {
            let permission = null;

            if (Platform.OS === 'ios') {
                permission = PERMISSIONS.IOS.CAMERA
            } else {
                permission = PERMISSIONS.ANDROID.CAMERA
            }

            requestPermission(permission)
        }

        return <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <StatusBar hidden={false} />

            <View
                style={{
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'gray',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                {['WARM_UP', 'PROCESSING'].includes(this.state.activeOrder.state) ? <LottieView
                    source={require('../../loading.json')}
                    autoPlay
                    loop
                    style={{
                        width: 512,
                        height: 512,
                    }}
                /> : <Text>Order state: {this.state.activeOrder.state}</Text>}
            </View>

            {isFocused && <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                type={RNCamera.Constants.Type.back}
                autoFocus={RNCamera.Constants.AutoFocus.on}
                orientation={'portrait'}
                style={{
                    position: 'absolute',
                    height: '100%',
                    width: '100%',
                    alignItems: 'center',
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        this.camera && this.camera.takePictureAsync({quality: 0.45}).then(result => {
                            // console.warn(result.uri, this.state.activeOrder.guid);

                            EventBus.fireEvent('capture_photo', {uri: result.uri})
                        })
                    }}
                    style={{
                        position: 'absolute',
                        bottom: 25,
                        width: 65,
                        height: 65,
                        backgroundColor: 'tomato',
                        borderRadius: 50,
                        borderWidth: 2,
                        borderColor: 'white',
                    }}
                />
            </RNCamera>}

            {/*<View style={{position: 'absolute', top: '50%'}}>*/}
            {/*    <Text style={{color: 'white'}}>{this.state.activeOrder && this.state.activeOrder.guid}</Text>*/}
            {/*</View>*/}

            {/*<TouchableOpacity*/}
            {/*    onPress={() => {*/}
            {/*        this.camera && this.camera.takePictureAsync({quality: 0.45}).then(result => {*/}
            {/*            // console.warn(result.uri, this.state.activeOrder.guid);*/}

            {/*            EventBus.fireEvent('capture_photo', {uri: result.uri})*/}
            {/*        })*/}
            {/*    }}*/}
            {/*    style={{*/}
            {/*        position: 'absolute',*/}
            {/*        bottom: 25,*/}
            {/*        width: 65,*/}
            {/*        height: 65,*/}
            {/*        backgroundColor: 'tomato',*/}
            {/*        borderRadius: 50,*/}
            {/*        borderWidth: 2,*/}
            {/*        borderColor: 'white',*/}
            {/*    }}*/}
            {/*/>*/}
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});


export default CameraScreen
