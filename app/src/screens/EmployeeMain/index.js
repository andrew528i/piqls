import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Component} from 'react';
import EmployeeMapScreen from './Orders/Map';
import Icon from 'react-native-vector-icons/FontAwesome';
import CameraScreen from './Camera';
import AlbumScreen from './Album';
import DeviceInfo from 'react-native-device-info';
import AccountSettingsScreen from './AccountSettings';
import ChatScreen from './Chat';
import {requestNotifications} from 'react-native-permissions';
import messaging from '@react-native-firebase/messaging';
import APIClient from '../../core/api';

import {default as appTheme} from '../../themes/light.json';

const Tab = createBottomTabNavigator();

class EmployeeMainNavigator extends Component {
  async requestUserPermission() {
    const authorizationStatus = await messaging().requestPermission();

    if (authorizationStatus) {
      console.log('Permission status:', authorizationStatus);
    }
  }

  componentDidMount() {
    // console.warn(DeviceInfo.getModel());

    this.requestUserPermission().then(() => {
      requestNotifications(['alert', 'sound']).then(({status, settings}) => {
        messaging()
          .getToken()
          .then(fcmToken => {
            if (fcmToken && fcmToken.length > 0) {
              APIClient.user.edit({fcm_token: fcmToken});
            }
          });
      });
    });
  }

  render() {
    return (
        <Tab.Navigator
          tabBarOptions={{
            activeBackgroundColor: '#fff',
            inactiveBackgroundColor: '#fff',
            activeTintColor: appTheme['color-danger-500'],
            inactiveTintColor: '#0009',
            labelStyle: {
              fontSize: 11,
              lineHeight: 13,
            },
          }}
          lazy={false}>
          <Tab.Screen
            name="Orders"
            component={EmployeeMapScreen}
            options={{
              tabBarIcon: props => <Icon name="map" {...props} />,
            }}
          />

          <Tab.Screen
            name="Album"
            component={AlbumScreen}
            options={{
              tabBarIcon: props => <Icon name="image" {...props} />,
            }}
          />

          <Tab.Screen
            name="Camera"
            component={CameraScreen}
            options={{
              tabBarIcon: props => <Icon name="camera" {...props} />,
            }}
          />

          <Tab.Screen
            name="Chats"
            component={ChatScreen}
            options={{
              tabBarIcon: props => <Icon name="envelope" {...props} />,
            }}
          />

          <Tab.Screen
            name="Profiles"
            component={AccountSettingsScreen}
            options={{
              tabBarIcon: props => <Icon name="user" {...props} />,
            }}
          />
        </Tab.Navigator>
    );
  }
}

export default EmployeeMainNavigator;
