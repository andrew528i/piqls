import React, {Component} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import APIClient from '../../../core/api';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import Dialog from 'react-native-dialog';
import FastImage from 'react-native-fast-image';
import TopNavigation from '../../../componets/top-navigation/top-navigation';
import {connect} from 'react-redux';
import {default as appTheme} from '../../../themes/light.json';

const {width, height} = Dimensions.get('window');

const options = {
  title: 'Select Avatar',
  quality: 0.35,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class AccountSettingsScreen extends Component {
  state = {
    userEdit: {},
    dialogVisible: false,
  };

  saveUser(user) {
    let newUser = user || {...this.props.user, ...this.state.userEdit};

    if (newUser.avatar && !newUser.avatar.uri) {
      delete newUser.avatar;
    }

    APIClient.user.edit(newUser);
  }

  renderDialog() {
    let user = this.props.user;
    let userEdit = this.state.userEdit;

    return (
      <View>
        <Dialog.Container visible={this.state.dialogVisible}>
          <Dialog.Title>{this.state.fieldName}</Dialog.Title>
          <Dialog.Input
            value={userEdit[this.state.field] || user[this.state.field]}
            onChange={e => {
              userEdit[this.state.field] = e.nativeEvent.text;

              this.setState({userEdit});
            }}
          />
          <Dialog.Button
            label="Cancel"
            onPress={() => {
              delete userEdit[this.state.field];
              this.setState({dialogVisible: false, userEdit});
            }}
          />
          <Dialog.Button
            label="Save"
            onPress={() => {
              this.setState({dialogVisible: false});
              this.saveUser();
            }}
          />
        </Dialog.Container>
      </View>
    );
  }

  render() {
    let user = this.props.user;

    return (
      <SafeAreaView style={styles.screen}>
        <TopNavigation goBack={this.props.navigation.goBack} />

        {this.renderDialog()}

        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => {
              ImagePicker.showImagePicker(options, response => {
                if (response.didCancel) {
                  console.log('User cancelled image picker');
                } else if (response.error) {
                  console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                  console.log(
                    'User tapped custom button: ',
                    response.customButton,
                  );
                } else {
                  user.avatar = {
                    uri: response.uri,
                    name: 'avatar.jpeg',
                    type: 'image/jpeg',
                  };

                  this.saveUser(user);
                }
              });
            }}
            style={styles.ImageWrapper}>
            <FastImage
              source={{
                uri:
                  typeof user.avatar === 'string'
                    ? user.avatar
                    : 'https://www.wmpg.org/wp-content/uploads/2019/03/default-avatar.jpg',
              }}
              style={styles.avatar_img}
            />
            {/*//TODO: put native PNG icon */}
            <Icon name={'pencil'} size={28} style={styles.edit_img} />
          </TouchableOpacity>
          <Text style={styles.title}>First name</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                dialogVisible: true,
                fieldName: 'First name',
                field: 'first_name',
              });
            }}
            style={styles.wrapper_text}>
            <Text style={styles.text_bold}>{user.first_name}</Text>
            <Text style={styles.text_bold}> 〉</Text>
          </TouchableOpacity>
          <Text style={styles.title}>Last name</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                dialogVisible: true,
                fieldName: 'Last name',
                field: 'last_name',
              });
            }}
            style={styles.wrapper_text}>
            <Text style={styles.text_bold}>{user.last_name}</Text>
            <Text style={styles.text_bold}> 〉</Text>
          </TouchableOpacity>
          <Text style={styles.title}>Phone number</Text>
          <View style={styles.wrapper_text}>
            <Text style={styles.text_bold}>{user.phone}</Text>
            <Text style={styles.text_bold}> 〉</Text>
          </View>
          <Text style={styles.title}>Email</Text>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                dialogVisible: true,
                fieldName: 'Email name',
                field: 'email',
              });
            }}
            style={styles.wrapper_text}>
            <Text style={styles.text_bold}>{user.email}</Text>
            <Text style={styles.text_bold}> 〉</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => {
            APIClient.user.signOut(() => {
              this.props.navigation.navigate('SignIn');
            });
          }}
          style={styles.signOut}>
          <Text style={styles.signOutText}>Sign out</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  signOut: {
    paddingTop: 8,
    paddingBottom: 24,
    paddingHorizontal: 24,
  },
  signOutText: {
    fontSize: 22,
    fontWeight: 'bold',
    color: appTheme['color-danger-500'],
  },
  title: {
    marginBottom: 2,
  },
  text_bold: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  wrapper_text: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  edit_img: {
    position: 'absolute',
    transform: [{rotate: '90deg'}],
    bottom: 0,
    right: 0,
  },
  avatar_img: {
    width: 128,
    height: 128,
    borderRadius: 64,
  },
  ImageWrapper: {
    width: 128,
    height: 128,
    marginTop: 24,
    marginBottom: 32,
  },
  screen: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 34,
  },
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },
});

const stateToProps = state => ({
  user: state.user.info,
});

const dispatchToProps = dispatch => ({});

export default connect(
  stateToProps,
  dispatchToProps,
)(AccountSettingsScreen);
