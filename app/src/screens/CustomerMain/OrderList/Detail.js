import React, {Component} from 'react';
import {
  Platform,
  View,
  Image,
  StyleSheet,
  Text,
  SafeAreaView,
  ScrollView,
  Dimensions,ImageBackground
} from 'react-native';
import config from '../../../core/config';
import EventBus from '../../../core/event_bus';
import TopNavigation from '../../../componets/top-navigation/top-navigation';

import {default as appTheme} from '../../../themes/light.json';

const {width, height} = Dimensions.get('window');

class OrderDetailScreen extends Component {
  state = {
    order: {},
  };

  componentDidMount() {
    EventBus.addListener(
      'order_state',
      (this.listener = data => {
        if (this.state.guid === data.order.guid) {
          this.setState({order: data.order});
        }
      }),
    );
  }

  componentWillUnmount() {
    EventBus.removeListener(this.listener);
  }

  render() {
    let order = this.props.route.params.order;
    let employee = order.employee;

    let lat = order.location.coordinates[0];
    let lng = order.location.coordinates[1];

    let mapUrl =
      'https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=516x532&markers=color:red%7Clabel:O%7C' +
      lat +
      ',' +
      lng +
      '&key=' +
      config.google_api_key;

    return (
      <SafeAreaView style={styles.screen}>
        <TopNavigation goBack={this.props.navigation.goBack} />
        <ScrollView alwaysBounceVertical="false" style={styles.container}>
          <View style={styles.map_container}>
            <Image
              source={{uri: mapUrl}}
              style={{height: ((width - 40) / 16) * 9}}
            />
            <View style={styles.map_order_description}>
              <View style={styles.address_container}>
                <Text style={styles.text_bold}>
                  {order.address.split(',')[0]}
                </Text>
                <Text style={styles.text_bold}>
                  {order.amount} {order.currency}
                </Text>
              </View>
              <Text style={styles.text_main}>January, 25</Text>
              <View
                style={[
                  styles.address_container,
                  {justifyContent: 'flex-start'},
                ]}>
                <Text style={styles.text_main}>11:31 am</Text>
                <Text style={styles.text_main}>38 min</Text>
              </View>
              <View style={[styles.address_container]}>
                <Text style={styles.text_main}>
                  Photographer: {employee ? employee.username : 'not assigned'}
                </Text>
                <View>
                  <Image
                    style={styles.img}
                    source={{
                      uri: employee
                        ? employee.avatar
                        : 'https://www.drupal.org/files/issues/default-avatar.png',
                    }}
                  />
                  <ImageBackground
                    style={styles.starImage}
                    source={require('../../../asset/img/star.png')}>
                    <Text style={styles.raitingText}>4.9</Text>
                  </ImageBackground>
                </View>
              </View>
            </View>
          </View>
          <Text style={styles.title}>Get your photos{'\u00A0'}here:</Text>
          <View style={styles.code_wrapper}>
            <Text style={[styles.text_main, {paddingVertical: 8}]}>
              piqls.com/p/q34k3k2
            </Text>
            <Image
              source={require('../../../asset/img/copy-icon.png')}
              style={{width: 24, height: 24}}
            />
          </View>
          <Text style={styles.bottom_text}>
            Photos are successfully uploaded
          </Text>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: appTheme['color-warning-500'],
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: 16,
  },
  map_container: {
    borderWidth: 4,
    borderColor: appTheme['color-info-500'],
    backgroundColor: 'white',
    marginBottom: 32,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 16,
  },
  code_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  bottom_text: {
    fontSize: 22,
    fontWeight: 'bold',
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    textAlign: 'center',
    marginBottom: 64,
  },
  map_order_description: {
    paddingHorizontal: 12,
    paddingVertical: 16,
  },
  address_container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  text_bold: {
    fontSize: 20,
    lineHeight: 32,
    fontWeight: 'bold',
  },
  text_main: {
    fontSize: 20,
    lineHeight: 32,
    marginRight: 16,
  },
  img: {
    width: 56,
    height: 56,
    borderRadius: 28,
  },
  starImage: {
    width: 32,
    height: 32,
    position: 'absolute',
    left: -12,
    top: -12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  raitingText: {
    color: appTheme['color-danger-500'],
    fontSize: 10,
    fontWeight: 'bold',
  },
});

export default OrderDetailScreen;
