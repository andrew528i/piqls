import React, {Component} from 'react';

import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  StatusBar,
  Text,
  RefreshControl,
  SafeAreaView,
} from 'react-native';

import Collapsible from 'react-native-collapsible';
import EventBus from '../../../core/event_bus';
import APIClient from '../../../core/api';
import StoreContext from '../../../store/store';
import TopNavigation from '.././../../componets/top-navigation/top-navigation';

import {default as appTheme} from '../../../themes/light.json';

// const OrderHeader = (props) => {
//     let url = 'https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=600x300&markers=color:red%7Clabel:O%7C' + props.coords[0] + ',' + props.coords[1] + '&key=AIzaSyDDXaz_SHzP9Fo2DPpZhghjKt-2i2u0Q8k'
//
//     return <CardHeader {...props}>
//         <Image
//             style={styles.headerImage}
//             source={{ uri: url }}
//         />
//     </CardHeader>
// };
//
// class OrderCard extends Component {
//
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             collapsed: true,
//             ...props.order,
//         }
//     }
//
//     componentDidMount() {
//         EventBus.addListener('order_state', this.listener = data => {
//             if (this.state.guid === data.order.guid) {
//                 this.setState({...data.order})
//             }
//         })
//     }
//
//     componentWillUnmount() {
//         EventBus.removeListener(this.listener);
//     }
//
//     render() {
//         return <Card
//             style={styles.card}
//             status={this.state.state === 'PAID' ? 'success' : 'warning'}
//             onPressOut={() => {
//                 this.setState({collapsed: !this.state.collapsed})
//             }}
//             header={
//                 (props) => <OrderHeader {...props} coords={this.state.location.coordinates}/>}
//         >
//             <Text>
//                 Status: {this.state.state}
//             </Text>
//
//             <Collapsible collapsed={this.state.collapsed}>
//                 <View style={{flexDirection: 'row'}}>
//                     <Text>Created at:</Text>
//                     <Text>{this.state.created_at}</Text>
//                 </View>
//
//                 <View style={{flexDirection: 'row'}}>
//                     <Text>State:</Text>
//                     <Text>{this.state.state}</Text>
//                 </View>
//
//                 <View style={{flexDirection: 'row'}}>
//                     <Text>Location:</Text>
//                     <Text>{this.state.location.coordinates}</Text>
//                 </View>
//
//                 <View style={{flexDirection: 'row'}}>
//                     <Text>Employee:</Text>
//                     <Text>{this.state.employee ? this.props.order.employee.username : 'not found :('}</Text>
//                 </View>
//             </Collapsible>
//         </Card>
//     }
// }

class OrderListScreen extends Component {
  state = {
    orders: [],
    count: 0,
    refreshing: false,
  };

  // willFocus = this.props.navigation.addListener(
  //     'willFocus',
  //     payload => {
  //         this.forceUpdate()
  //     }
  // );

  loadOrders() {
    this.setState({refreshing: true});
    // this.context.set('loading', true);

    APIClient.order.list(
      {},
      result => {
        let count = result.data.count;
        let orders = result.data.results;

        // this.context.set('loading', false);

        this.setState({count, orders, refreshing: false});
      },
      () => {
        // this.context.set('loading', false)
        this.setState({refreshing: false});
      },
    );
  }

  componentDidMount() {
    EventBus.addListener(
      'load_orders',
      (this.listener = () => {
        this.loadOrders();
      }),
    );

    EventBus.fireEvent('load_orders', {});
  }

  componentWillUnmount() {
    EventBus.removeListener(this.listener);
  }

  render() {
    // const icon = (style) => <Icon {...style} name='arrow-back' onPress={() => this.props.navigation.goBack()} />
    //
    // const BackAction = () => (
    //     <TopNavigationAction icon={icon}/>
    // );
    //
    // return <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
    //     <StatusBar hidden={false}/>
    //     <TouchableOpacity
    //         onPress={() => {
    //             this.props.navigation.goBack()
    //         }}
    //         style={{width: '100%'}}
    //     >
    //         <TopNavigation
    //             leftControl={BackAction()}
    //             title='Order list'
    //         />
    //     </TouchableOpacity>
    //
    //     <View style={{width: '100%', padding: 20, justifySelf: 'flex-start'}}>
    //         <Text category={'h6'}>Order count: {this.state.count}</Text>
    //     </View>
    //
    //     <ScrollView style={{flex: 1, width: '100%'}}>
    //         {this.state.orders.map((o, idx) => {
    //             return <View
    //                 style={{flexDirection: 'column', paddingBottom: 40}}
    //                 key={'order_' + idx}
    //             >
    //                 <OrderCard order={o} />
    //             </View>
    //         })}
    //     </ScrollView>
    // </View>
    return (
      <View style={styles.screen}>
        <SafeAreaView style={styles.top}>
          <TopNavigation goBack={this.props.navigation.goBack} />
        </SafeAreaView>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                this.loadOrders();
              }}
            />
          }
          style={styles.container}>
          {this.state.orders.map((o, idx) => (
            <TouchableOpacity
              key={'order_list_item_' + idx}
              onPress={() => {
                this.props.navigation.navigate('OrderDetail', {order: o});
              }}
              style={styles.item_order_container}>
              <View style={{flexGrow: 1, marginRight: 16}}>
                <Text style={styles.text_address}>
                  {o.address.split(',')[0]} • 38 min
                </Text>
                <Text style={styles.text_date}>Jan, 25 11:31 am</Text>
              </View>
              <View style={{justifyContent: 'center'}}>
                {/*<View style={styles.upload_message}>*/}
                {/*    <Text style={{fontSize: 10, color: 'white', textAlign: 'center'}}>Photos uploaded</Text>*/}
                {/*</View>*/}
                <Text
                  style={{color: '#FF3DC8', fontWeight: 'bold', fontSize: 25}}>
                  〉
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: appTheme['color-warning-500'],
    flex: 1,
  },
  top: {
    flexGrow: 0,
  },
  container: {
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: 16,
  },
  item_order_container: {
    flexDirection: 'row',
    marginBottom: 32,
    borderWidth: 4,
    borderColor: appTheme['color-info-500'],
    backgroundColor: 'white',
    padding: 16,
  },
  text_address: {
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 32,
  },
  text_date: {
    fontSize: 20,
    lineHeight: 28,
  },
  //   upload_message: {
  //     position: 'absolute',
  //     top: -40,
  //     right: -30,
  //     width: 73,
  //     height: 37,
  //     justifyContent: 'center',
  //     alignItems: 'center',
  //     backgroundColor: '#EB5757',
  //     borderRadius: 30,
  //   },
});

export default OrderListScreen;
