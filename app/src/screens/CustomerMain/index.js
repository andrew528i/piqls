import React, {Component} from 'react';
import {BackHandler} from 'react-native'
import {createDrawerNavigator} from '@react-navigation/drawer';
import MapScreen from './Map';
import DrawerContent from './Drawer';
import OrderListScreen from './OrderList';
import OrderDetailScreen from './OrderList/Detail';
import AccountSettingsScreen from './AccountSettings';
import messaging from '@react-native-firebase/messaging';
import {requestNotifications} from 'react-native-permissions';

const Drawer = createDrawerNavigator();

class CustomerDrawerNavigator extends Component {
	componentDidMount() {
		// requestNotifications(['alert', 'sound']).then(({ status, settings }) => {
		// 	// console.warn(status);
		// 	// console.warn(settings);
		//
		// 	messaging().getToken()
		// 	.then(fcmToken => {
		// 		// console.warn(fcmToken)
		// 	});
		// });

		// messaging().getToken()
		// .then(fcmToken => {
		// 	console.warn(fcmToken)
		// });


		// messaging().onTokenRefresh(fcmToken => {
		// 	console.warn(fcmToken)
		// });


		// messaging().hasPermission()
		// 	.then(enabled => {
		// 	console.warn(enabled)
		// });

		let disableBack = () => {
			if (this.props.route.state === undefined) {
				return true
			}

			return this.props.route.state.index === 0;
		};

		this.backHandler = BackHandler.addEventListener('hardwareBackPress', function() {
			return disableBack();
		});
	}

	componentWillUnmount() {
		if (this.backHandler) {
			this.backHandler.remove()
		}
	}

	render() {
		return <Drawer.Navigator
			initialRouteName='Map'
			drawerContent={props => <DrawerContent {...props} />}
			drawerStyle={{
				width: 240,
			}}
		>
			<Drawer.Screen name='Map' component={MapScreen} />
			<Drawer.Screen name='OrderList' component={OrderListScreen} />
			<Drawer.Screen name='OrderDetail' component={OrderDetailScreen} />
			<Drawer.Screen name='AccountSettings' component={AccountSettingsScreen} />
		</Drawer.Navigator>
	}
}

export default CustomerDrawerNavigator
