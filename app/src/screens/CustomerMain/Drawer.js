import React, {Component} from 'react';
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  View,
  Image,
  Text,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import AsyncStorage from '@react-native-community/async-storage';
import StoreContext from '../../store/store';
import {default as appTheme} from '../../themes/light.json';
import EventBus from '../../core/event_bus';
import {connect} from 'react-redux';

const {width, height} = Dimensions.get('window');

class DrawerContent extends Component {
  render() {
    let name = `${this.props.user.first_name} ${this.props.user.last_name}`;

    return (
      <View style={styles.drawer}>
        <Image
          source={require('../../asset/img/Drawer.png')}
          style={{position: 'absolute', bottom: 34, right: 0}}
        />

        <TouchableOpacity
          style={{
            width: 80,
            height: 80,
            borderRadius: 40,
            marginBottom: 16,
          }}>
          <FastImage
            source={{
              uri:
                this.props.user.avatar ||
                'https://www.wmpg.org/wp-content/uploads/2019/03/default-avatar.jpg',
            }}
            style={{width: 80, height: 80, borderRadius: 40}}
          />
        </TouchableOpacity>

        <View
          style={{
            marginBottom: 32,
          }}>
          <Text
            numberOfLines={2}
            style={{
              color: appTheme['color-primary-500'],
              fontSize: 24,
              lineHeight: 32,
              fontWeight: 'bold',
            }}>
            {name}
          </Text>
          <Text
            numberOfLines={1}
            style={{
              color: appTheme['color-info-500'],
              fontSize: 18,
              lineHeight: 21,
              fontWeight: 'bold',
            }}>
            @{this.props.user.username || 'username'}
          </Text>
        </View>

        <TouchableOpacity
          style={{marginBottom: 2}}
          onPress={() => {
            alert('Sorry, you have not friends');
          }}>
          <Text
            style={{
              color: appTheme['color-danger-500'],
              fontSize: 18,
              lineHeight: 44,
              fontWeight: 'bold',
            }}>
            ❤️ Invite friends
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{marginBottom: 2}}
          onPress={() => {
            this.props.navigation.navigate('OrderList');
            EventBus.fireEvent('load_orders', {});
          }}>
          <Text style={styles.row}>🥒 Orders</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{marginBottom: 2}}
          onPress={() => {
            this.props.navigation.navigate('AccountSettings');
          }}>
          <Text style={styles.row}>🤖 Account settings</Text>
        </TouchableOpacity>

        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, 0.1)',
            height: 1,
            marginVertical: 12,
          }}
        />

        <TouchableOpacity
          style={{marginBottom: 2}}
          onPress={() => {
            alert('feedback');
          }}>
          <Text style={styles.row}>Leave feedback</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{marginBottom: 2}}
          onPress={() => {
            alert('FAQ');
          }}>
          <Text style={styles.row}>FAQ</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{marginBottom: 2}}
          onPress={() => {
            alert('terms');
          }}>
          <Text style={styles.row}>Terms of use</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  drawer: {
    height,
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: 48,
    backgroundColor: appTheme['color-warning-500'],
  },
  row: {
    color: appTheme['color-primary-500'],
    backgroundColor: '#FAFF0088',
    fontSize: 18,
    lineHeight: 44,
    fontWeight: 'bold',
  },
});

const stateToProps = state => ({
  user: state.user.info,
});

const dispatchToProps = dispatch => ({});

export default connect(
  stateToProps,
  dispatchToProps,
)(DrawerContent);
