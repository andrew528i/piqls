import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'

//TODO: доверстать
class MapMarker extends Component {
    render() {
        return <View style={styles.container}>
            <View style={styles.point}/>
            <View style={styles.circle}/>
            <View style={styles.square}/>
            <View style={styles.text_wrapper}>
                <Text style={{...styles.text, fontSize: 20}}>30</Text>
                <Text style={{...styles.text, fontSize: 12}}>min</Text>
            </View>
        </View>
    }
}

const styles = StyleSheet.create({
    text_wrapper: {
        position: 'absolute',
        top: -35
    },
    text: {
        color: 'white',
        fontWeight: 'bold'
    },
    container: {
        width: 56,
        height: 56,
        backgroundColor: 'rgba(55, 55, 208, .16)',
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    point: {
        width: 12,
        height: 12,
        backgroundColor: '#FF3DC8',
        borderRadius: 100
    },
    circle: {
        position: 'absolute',
        top: -40,
        width: 56,
        height: 56,
        backgroundColor: 'rgb(55, 55, 208)',
        borderRadius: 100,
    },
    square: {
        position: 'absolute',
        top: -5,
        width: 27,
        height: 27,
        backgroundColor: 'rgb(55, 55, 208)',
        transform: [{rotate: '45deg'}]
    }
});


export default MapMarker
