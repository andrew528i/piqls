import React, {Component} from 'react';
import {Dimensions, StatusBar, TouchableOpacity, View, Platform, AppState, AsyncStorage} from 'react-native';
import MapView, {Marker, Polygon, Polyline, PROVIDER_GOOGLE} from 'react-native-maps';
import {Text} from '@ui-kitten/components';
import Geocoder from 'react-native-geocoding';
import TopBar from './TopBar';
import APIClient from '../../../core/api';

import {PERMISSIONS, requestNotifications} from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import {default as appTheme} from '../../../themes/light.json';
import Icon from 'react-native-vector-icons/FontAwesome';
import EventBus from '../../../core/event_bus';
import requestPermission from '../../../core/permissions';
import {isLocationEnabled} from 'react-native-device-info';
import BottomSheetView from './BottomSheet';
import {connect} from 'react-redux';
import {store} from '../../../store/store';
import {setLoadingState} from '../../../store/loading/actions';
import messaging from '@react-native-firebase/messaging';
import ChangeOrder from './BottomSheet/ChangeOrder';


const { width, height } = Dimensions.get('screen');

class MapScreen extends Component {

    appState = null;
    state = {
        location: {latitude: 0, longitude: 0},
        initialRegion: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        selectLocation: false,
        regionOpacity: 0.195,
        polygons: [],
        order: {
            location: {
                latitude: 0,
                longitude: 0
            },
            address: '',
        },
        route: [],
        usersOnMap: [],
    };

    loadAreas() {
        APIClient.order.areaList(resp => {
            let results = resp.results;
            let polygons = [];

            results.forEach(r => {
                let p = r.polygon.coordinates[0].map(coord => {
                    return {longitude: coord[1], latitude: coord[0]}
                });

                polygons.push(p)
            });

            this.setState({polygons})
        })
    }

    updateActiveOrder() {
        APIClient.order.list({active: 1}, resp => {
            let results = resp.data.results;

            if (results.length > 0) {
                let order = results[0];

                order.location = {
                    latitude: order.location.coordinates[0],
                    longitude: order.location.coordinates[1],
                };

                if (order.employee && order.employee.location && order.employee.location.coordinates) {
                    order.employee.location = {
                        latitude: order.employee.location.coordinates[0],
                        longitude: order.employee.location.coordinates[1],
                    };
                }

                this.setState({order})
            } else {
                this.setState({usersOnMap: []})
            }
        })
    }

    async requestUserPermission() {
        const authorizationStatus = await messaging().requestPermission();

        if (authorizationStatus) {
            console.log('Permission status:', authorizationStatus);
        }
    }

    componentDidMount() {
        AppState.addEventListener('change', this.appStateListener = nextAppState => {
            if (
                this.appState &&
                this.appState.match(/inactive|background/) &&
                nextAppState === 'active'
            ) {
                if (this.state.order.state) {
                    this.updateActiveOrder();
                }
            }

            this.appState = nextAppState
        });

        this.requestUserPermission().then(() => {
            requestNotifications(['alert', 'sound']).then(({ status, settings }) => {
                messaging().getToken()
                    .then(fcmToken => {
                        if (fcmToken && fcmToken.length > 0) {
                            APIClient.user.edit({fcm_token: fcmToken})
                        }
                });
            });
        })

        this.updateActiveOrder();

        EventBus.addListener('update_location', this.locationListener = data => {
            this.setState({
                usersOnMap: data.users.map(user => {
                    if (!user || !user.location || !user.location.coordinates) {
                        return null;
                    }

                    user.location = {
                        latitude: user.location.coordinates[0],
                        longitude: user.location.coordinates[1],
                    };

                    return user
                }).filter(u => u !== null)
            })
        });

        EventBus.addListener('order_state', this.orderStateListener = data => {
            Geolocation.getCurrentPosition(
                position => APIClient.user.updateLocation(position.coords)
            ).then();

            let order = {...this.state.order, ...data.order};

            if (order.state === 'CANCELLED' || order.state === 'DONE') {
                this.setState({
                    order: {
                        location: {
                            latitude: 0,
                            longitude: 0
                        },
                        address: '',
                    },
                    route: [],
                    usersOnMap: [],
                });
                return
            }

            if (order.employee && order.employee.location && order.employee.location.coordinates) {
                order.employee.location = {
                    latitude: order.employee.location.coordinates[0],
                    longitude: order.employee.location.coordinates[1],
                };
            }

            order.location = {
                latitude: order.location.coordinates[0],
                longitude: order.location.coordinates[1],
            };

            this.setState({order})
        });


        let permission = null;

        if (Platform.OS === 'ios') {
            permission = PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
        } else {
            permission = PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
        }

        requestPermission(
            permission,
            () => {
                Geolocation.getCurrentPosition(
                    position => {
                        this.setState({location: position.coords});
                        this.map.animateCamera({
                            center: position.coords,
                            zoom: 15,
                        }, 3000);

                        APIClient.user.updateLocation(position.coords)
                    }
                ).then();

                Geolocation.watchPosition(
                    position => {
                        this.setState({location: position.coords});
                        APIClient.user.updateLocation(position.coords);
                    }
                )
            },
            () => {
                console.warn('NO WAY')
            }
        );

        EventBus.addListener('select_location', this.locationListener = data => {
            AsyncStorage.setItem('selectedLocation', JSON.stringify(data)).then(() => {
                this.setState({selectLocation: false})
            })
        })

        this.loadAreas()
    }

    componentWillUnmount() {
        this.appStateListener && AppState.removeEventListener('change', this.appStateListener)

        EventBus.removeListener(this.locationListener);
        EventBus.removeListener(this.orderStateListener);
    }

    render() {
        let regionColor = 'rgba(255, 61, 200, ' + this.state.regionOpacity + ')';

        return (
            <View style={{flex: 1}}>
                {/*<StatusBar hidden={false} />*/}
                <StatusBar translucent={true} backgroundColor={'transparent'} />
                <MapView
                    style={{position: 'absolute', width, height}}
                    customMapStyle={require('./style.json')}
                    provider={PROVIDER_GOOGLE}
                    ref={map => {
                        this.map = map
                    }}
                    initialRegion={this.state.initialRegion}
                    showsCompass={false}
                    // onRegionChange={region => {
                    //     console.warn(region)
                    // }}
                    onPress={e => {
                        if (this.props.user.has_active_orders && this.state.order.address !== '' && !this.state.selectLocation) {
                            console.warn('has active orders')
                            return
                        }

                        let coord = e.nativeEvent.coordinate;

                        this.map.animateCamera({center: coord});

                        store.dispatch(setLoadingState(true))

                        let callback = null;

                        if (!this.state.selectLocation) {
                            callback = location => {
                                this.setState({
                                    order: {
                                        ...this.state.order,
                                        ...location,
                                    },
                                }, () => this.bs.open())
                            }
                        } else {
                            callback = location => {
                                // this.setState({selectLocation: false}, () => {
                                //     EventBus.fireEvent('select_location', location)
                                // })
                                EventBus.fireEvent('select_location', location)
                            }
                        }

                        Geocoder.from(coord.latitude, coord.longitude)
                            .then(json => {
                                let orderAddress = json.results[0]['formatted_address'];

                                callback && callback({
                                    location: coord,
                                    address: orderAddress,
                                })
                            })
                            .finally(() => {
                                store.dispatch(setLoadingState(false))
                            })
                    }}
                    onRegionChangeComplete={region => {
                        // let zoom = Math.round(Math.log(360 / region.longitudeDelta) / Math.LN2)
                        let regionOpacity = [0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]

                        if (region.longitudeDelta <= 1) {
                            regionOpacity = regionOpacity[parseInt((region.longitudeDelta / 1.5) * regionOpacity.length)]

                            this.setState({regionOpacity})
                        }
                    }}
                >
                    <Marker
                        coordinate={this.state.location}
                    >
                        <Icon name={'circle'} size={22} color={'#f69124'} />
                        {/*<MapMarker />*/}
                    </Marker>

                    {this.state.order.location.latitude !== 0.0 ? <Marker
                        coordinate={this.state.order.location}
                        onPress={() => {
                            if (this.createOrderBottomSheet) {
                                this.createOrderBottomSheet.showOrderView(this.state.order.address);
                            }
                        }}
                    >
                        <Icon name={'circle'} size={22} color={'#FF3DC8'} />
                    </Marker>: null}

                    {this.state.polygons.map((polygon, idx) => <Polygon
                        key={'polygon_' + idx}
                        coordinates={polygon}
                        strokeColor={'rgba(255, 61, 200, 0.7)'}
                        fillColor={regionColor}
                    />)}

                    {this.state.route.length > 0 ? <Polyline
                        coordinates={this.state.route}
                        strokeWidth={4}
                        strokeColor={'#4c8bf5'}
                    /> : null}

                    {this.state.usersOnMap.map((user, idx) => <Marker
                        key={'user_on_map_' + idx}
                        coordinate={user.location}
                    >
                        <Icon name={'circle'} size={22} color={'#3cd86d'} />
                    </Marker>)}
                </MapView>


                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        right: 10,
                        bottom: 240,
                        padding: 14,
                        backgroundColor: '#FFF',
                        borderRadius: 60,
                        flex: 1,
                        //android shadow
                        elevation: 0,
                        //ios shadow
                        shadowColor: appTheme['color-primary-500'],
                        shadowOpacity: 0.16,
                        shadowRadius: 14,
                        shadowOffset: {width:0,height:2}
                    }}
                    onPress={() => {
                        isLocationEnabled().then(result => {
                            Geolocation.getCurrentPosition(
                                position => {
                                    this.map.animateCamera({
                                        center: position.coords,
                                        zoom: 15,
                                    }, 3000 );
                                    APIClient.user.updateLocation(position.coords)
                                }
                            ).then()
                        })
                    }}
                >
                    <Icon name={'location-arrow'} size={21} />
                </TouchableOpacity>


                <View
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        marginTop: 44,
                        flex: 1,
                        flexDirection: 'column',
                    }}
                >
                    <TopBar {...this.props} />
                    <View
                        style={{
                            flex: 1,
                            marginTop: 24,
                            paddingHorizontal: 20,
                            alignItems: 'center',
                        }}
                    >
                        <Text style={{fontSize:16, lineHeight:20}}>You are here</Text>
                        <Text style={{fontSize:16, lineHeight:20, marginBottom:8}}>Choose your photoshoot spot:</Text>
                        <Text style={{fontSize:32, lineHeight:38}}>{this.state.order.address.split(',')[0]}</Text>
                    </View>
                </View>

                <BottomSheetView
                    ref={bs => this.bs = bs}
                    order={this.state.order}
                    selectLocation={this.state.selectLocation}
                    onSuggestionSelected={result => {
                        let location = result.geometry.location;
                        let orderLocation = {
                            latitude: location.lat,
                            longitude: location.lng,
                        };

                        this.map.animateCamera({center: orderLocation});

                        if (this.state.selectLocation) {
                            EventBus.fireEvent('select_location', {
                                location: orderLocation,
                                address: result.formatted_address,
                            })

                            return
                        }

                        this.setState({
                            // selectLocation: false,
                            order: {
                                ...this.state.order,
                                location: orderLocation,
                                address: result.formatted_address,
                            },
                        }, () => {
                            this.bs.open()
                        });
                    }}
                    onChange={() => {
                        this.updateActiveOrder()
                    }}
                    onChangeLocation={() => {
                        this.setState({selectLocation: true})
                    }}
                    onCreateOrderClose={() => {
                        this.setState({
                            order: {
                                location: {
                                    latitude: 0,
                                    longitude: 0
                                },
                                address: '',
                            },
                        })
                    }}
                    onDone={() => {
                        // this.context.set('loading', true);

                        APIClient.order.state({
                            guid: this.state.order.guid,
                            state: 'DONE',
                        }, resp => {
                            this.bs.setSnapPoints([height, height], true)
                            this.setState({
                                usersOnMap: [],
                                order: {
                                    location: {
                                        latitude: 0,
                                        longitude: 0
                                    },
                                    address: '',
                                },
                                user: {
                                    ...this.state.user,
                                    has_active_orders: false,
                                }
                            });

                            this.updateActiveOrder()
                        })
                    }}
                    onSuccess={(order) => {
                        // this.updateUserDetails();
                        this.setState({order: {
                            ...order,
                            ...this.state.order,
                        }})
                    }}
                    onBuildRoute={() => {
                        APIClient.user.buildRoute({
                            from_location: this.state.location,
                            to_location: this.state.order.location,
                        }, resp => {
                            let polyline = resp.data.polyline;

                            if (polyline && polyline.length > 0) {
                                this.setState({route: polyline});
                            }
                        });
                    }}
                    onCancel={() => {
                        this.setState({
                            order: {
                                location: {
                                    latitude: 0,
                                    longitude: 0
                                },
                                address: '',
                            },
                            user: {
                                ...this.state.user,
                                has_active_orders: false,
                            }
                        })
                    }}
                    showOnMap={() => {
                        this.state.usersOnMap.map(user => {
                            if (this.state.order.employee && user.id === this.state.order.employee.id) {
                                this.map.animateCamera({
                                    center: user.location,
                                    zoom: 15,
                                }, 3000);
                            }
                        });
                    }}
                    {...this.props}
                />

                {/*{*/}
                {/*    this.state.order.employee || this.state.user.has_active_orders ? <EmployeePanel*/}
                {/*        order={this.state.order}*/}
                {/*        onDone={() => {*/}
                {/*            this.context.set('loading', true);*/}

                {/*            APIClient.order.state({*/}
                {/*                guid: this.state.order.guid,*/}
                {/*                state: 'DONE',*/}
                {/*            }, resp => {*/}
                {/*                this.context.set('loading', false);*/}
                {/*                this.setState({*/}
                {/*                    usersOnMap: [],*/}
                {/*                    order: {*/}
                {/*                        location: {*/}
                {/*                            latitude: 0,*/}
                {/*                            longitude: 0*/}
                {/*                        },*/}
                {/*                        address: '',*/}
                {/*                    }*/}
                {/*                });*/}

                {/*                this.updateUserDetails()*/}
                {/*                // this.props.navigation.navigate('OrderList')*/}
                {/*            }, this.context.set('loading', true))*/}
                {/*        }}*/}
                {/*        onCancel={() => {*/}
                {/*            this.setState({*/}
                {/*                order: {*/}
                {/*                    location: {*/}
                {/*                        latitude: 0,*/}
                {/*                        longitude: 0*/}
                {/*                    },*/}
                {/*                    address: '',*/}
                {/*                },*/}
                {/*                user: {*/}
                {/*                    ...this.state.user,*/}
                {/*                    has_active_orders: false,*/}
                {/*                }*/}
                {/*            })*/}
                {/*        }}*/}
                {/*        onBuildRoute={() => {*/}
                {/*            // TODO: -> core*/}
                {/*            this.context.set('loading', true);*/}

                {/*            APIClient.user.buildRoute({*/}
                {/*                from_location: this.state.location,*/}
                {/*                to_location: this.state.order.location,*/}
                {/*            }, resp => {*/}
                {/*                this.context.set('loading', false);*/}
                {/*                let polyline = resp.data.polyline;*/}

                {/*                if (polyline && polyline.length > 0) {*/}
                {/*                    this.setState({route: polyline});*/}
                {/*                }*/}

                {/*            }, () => this.context.set('loading', false));*/}
                {/*        }}*/}
                {/*        showOnMap={() => {*/}
                {/*            this.state.usersOnMap.map(user => {*/}
                {/*                if (this.state.order.employee && user.id === this.state.order.employee.id) {*/}
                {/*                    this.map.animateCamera({*/}
                {/*                        center: user.location,*/}
                {/*                        zoom: 15,*/}
                {/*                    }, 3000);*/}
                {/*                }*/}
                {/*            });*/}
                {/*        }}*/}
                {/*    /> : <CreateOrder*/}
                {/*        ref={ref => {*/}
                {/*            this.createOrderBottomSheet = ref;*/}
                {/*        }}*/}
                {/*        onCancel={() => {*/}
                {/*            this.setState({*/}
                {/*                order: {*/}
                {/*                    ...this.state.order,*/}
                {/*                    location: {latitude: 0, longitude: 0},*/}
                {/*                    address: '',*/}
                {/*                },*/}
                {/*            });*/}
                {/*            this.updateUserDetails();*/}
                {/*        }}*/}
                {/*        onSuccess={() => {*/}
                {/*            this.updateUserDetails();*/}
                {/*        }}*/}
                {/*        showOrderView={this.state.order.address.length > 0}*/}
                {/*        order={this.state.order}*/}
                {/*        onSearchSuggestion={result => {*/}
                {/*            let location = result.geometry.location;*/}
                {/*            let orderLocation = {*/}
                {/*                latitude: location.lat,*/}
                {/*                longitude: location.lng,*/}
                {/*            };*/}

                {/*            this.setState({*/}
                {/*                order: {*/}
                {/*                    ...this.state.order,*/}
                {/*                    location: orderLocation,*/}
                {/*                    address: result.formatted_address,*/}
                {/*                },*/}
                {/*            });*/}
                {/*            this.map.animateCamera({center: orderLocation});*/}

                {/*            if (this.createOrderBottomSheet) {*/}
                {/*                this.createOrderBottomSheet.showOrderView(result.formatted_address);*/}
                {/*            }*/}
                {/*        }}*/}
                {/*        {...this.props}*/}
                {/*    />*/}
                {/*}*/}
            </View>
        );
    }
}

const stateToProps = state => ({
    user: state.user.info,
});

const dispatchToProps = dispatch => ({});

export default connect(stateToProps, dispatchToProps)(MapScreen)
