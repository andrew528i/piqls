import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  StyleSheet,
  KeyboardAvoidingView,
  SafeAreaView,
  Platform,
} from 'react-native';
import Geocoder from 'react-native-geocoding';

const {height} = Dimensions.get('window');

class SearchSuggestionView extends Component {
  state = {
    suggestions: [],
    paddingBottom: 0,
  };

  render() {
    const input = this.renderInput();
    const suggestions = this.renderSuggestions();

    return (
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : null}
          // onLayout={e => {
          //     let height = e.nativeEvent.layout.height;
          //     let snapPoints = [height, height];
          //
          //     if (this.props.getSnapPoints() !== snapPoints) {
          //         this.props.setSnapPoints(snapPoints, true);
          //         this.props.open();
          //     }
          // }}
          {...this.props}
          style={[styles.container, {paddingBottom: this.state.paddingBottom}]}>
          {input}
          {this.state.suggestions.length > 0 && suggestions}
        </KeyboardAvoidingView>
    );
  }

  renderInput() {
    return (
      <TextInput
        placeholder={'Address'}
        onChangeText={this.getSuggestions.bind(this)}
        style={styles.input}
      />
    );
  }

  renderSuggestions() {
    let items = this.state.suggestions.map(
      this.renderSuggestionItem.bind(this),
    );

    return (
      <View style={styles.suggestionContainer}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          onScroll={() => Keyboard.dismiss()}>
          {items}
        </ScrollView>
      </View>
    );
  }

  renderSuggestionItem(item, idx) {
    return (
      <TouchableOpacity
        style={styles.suggestionItem}
        key={'suggestion_item_' + idx}
        onPress={() => this.props.onSuggestionSelected(item)}>
        <Text style={styles.suggestionItemText}>{item.formatted_address}</Text>
      </TouchableOpacity>
    );
  }

  getSuggestions(query) {
    if (query.length === 0) {
      this.setState({suggestions: []});
      return;
    }

    Geocoder.from(query).then(json => {
      let suggestions = json.results;
      //     .filter(result => {
      //     let isValid = false;
      //
      //     result.address_components.forEach(ac => {
      //         if (ac.short_name === 'SF') { // San Francisco
      //             isValid = true;
      //         }
      //     });
      //
      //     return isValid;
      // });

      this.setState({suggestions});
    });
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: '#f7f5eee8',
    padding: 24,
    bottom: 0,
    left: 0,
    right: 0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  input: {
    fontSize: 18,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderRadius: 10,
    height: 50,
    borderColor: '#00000030',
    marginBottom: 24,
    color: '#000000',
  },
  suggestionContainer: {
    height: height / 3,
  },
  suggestionItem: {
    paddingVertical: 10,
  },
  suggestionItemText: {
    fontSize: 20,
  },
});

export default SearchSuggestionView;
