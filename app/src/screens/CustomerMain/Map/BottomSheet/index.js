import React, {Component} from 'react';
import {View, Text, Dimensions, StyleSheet} from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import SearchSuggestionView from './SearchSuggestion';
import CreateOrderView from './CreateOrder';
import EventBus from '../../../../core/event_bus';
import OrderView from './Order';


const {width, height} = Dimensions.get('screen');
const expandedSnapPoints = [height, height];

class BottomSheetView extends Component {

    state = {
        snapPoints: expandedSnapPoints,
        gestureInteraction: false,
    };

    bs = React.createRef();
    onClose = null;

    render() {
        let content = this.renderContent();
        let order = this.props.order;

        if (!content || this.props.selectLocation) {
            return <SearchSuggestionView {...this.props} />;
        }

        if (['READY', 'WARM_UP', 'PROCESSING', 'COMPLETED'].includes(order.state)) {
            return content
        }

        return <BottomSheet
            ref={this.bs}
            snapPoints={this.state.snapPoints}
            renderContent={() => content}
            enabledGestureInteraction={this.state.gestureInteraction}
            onCloseEnd={() => {
                EventBus.fireEvent('bs_close')

                if (this.onClose) {
                    this.onClose()
                    this.onClose = null
                }
            }}
            onOpenEnd={() => {
                // console.warn('!!!!!opened')
            }}
            style={styles.panel}
        />
    }

    open() {
        let idx = this.state.snapPoints.length - 1;
        this.bs.current.snapTo(idx);
        this.bs.current.snapTo(idx);
    }

    close() {
        this.bs.current.snapTo(0);
        this.bs.current.snapTo(0);
    }

    snapTo(idx) {
        this.bs.current.snapTo(idx);
        this.bs.current.snapTo(idx);
    }

    setSnapPoints(snapPoints, gestureInteraction) {
        this.setState({snapPoints, gestureInteraction})
    }

    getSnapPoints() {
        return this.state.snapPoints;
    }

    renderContent() {
        let order = this.props.order;

        const props = {
            setSnapPoints: this.setSnapPoints.bind(this),
            getSnapPoints: this.getSnapPoints.bind(this),
            snapTo: this.snapTo.bind(this),
            open: this.open.bind(this),
            close: this.close.bind(this),
            ...this.props,
        };

        let createOrder = <CreateOrderView {...props} />;
        let orderView = <OrderView {...props} />;

        if (!!(order.address && order.address.length > 0)) {
            if (order.state && order.state.length > 0) {
                return orderView;
            }

            return createOrder;
        }

        return null
    }
}

const styles = StyleSheet.create({
    panel: {
        position: 'absolute',
        bottom: 0,
        width,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    }
});

export default BottomSheetView
