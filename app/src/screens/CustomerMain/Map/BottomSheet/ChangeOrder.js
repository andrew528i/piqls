import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView, AsyncStorage,
} from 'react-native';
import DatePicker from 'react-native-date-picker';

import {default as appTheme} from '../../../../themes/light.json';
import SearchSuggestionView from './SearchSuggestion';
import EventBus from '../../../../core/event_bus';

function getMinimumDate() {
  let minimumDate = new Date();

  minimumDate.setTime(minimumDate.getTime()); // + (3600 * 1000));

  return minimumDate;
}

function getMaximumDate() {
  let maximumDate = new Date();

  maximumDate.setDate(maximumDate.getDate() + 14); // + 1);

  return maximumDate;
}

class ChangeOrder extends Component {

  state = {
    showTimePicker: false,
    orderDate: new Date(this.props.order.started_at),
    orderLocation: this.props.order.location,
    orderAddress: this.props.order.address,
    minimumDate: getMinimumDate(),
    maximumDate: getMaximumDate(),
  };

  componentDidMount() {
    // EventBus.addListener('select_location', this.locationListener = data => {
    //   this.setState({orderLocation: data.location, orderAddress: data.address})
    // })
    AsyncStorage.getItem('selectedLocation').then(res => {
      if (!res) {
        return
      }

      res = JSON.parse(res);
      this.setState({orderLocation: res.location, orderAddress: res.address})
    })
  }

  // componentWillUnmount() {
  //   EventBus.removeListener(this.locationListener)
  // }

  renderDatetimePicker() {
    return (
      <SafeAreaView
        onLayout={e => {
          let height = e.nativeEvent.layout.height;
          let snapPoints = [0, height];

          if (this.props.getSnapPoints() !== snapPoints) {
            this.props.setSnapPoints(snapPoints, true);
          }
        }}
        style={styles.screen}>
        <View style={styles.container}>
          <DatePicker
            date={this.state.orderDate}
            onDateChange={orderDate => this.setState({orderDate})}
            minimumDate={this.state.minimumDate}
            maximumDate={this.state.maximumDate}
            minuteInterval={5}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({showTimePicker: false});
            }}
            style={styles.btn}>
            <Text style={styles.btnText}>Confirm</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }

  render() {
    let order = this.props.order;

    if (this.state.showTimePicker) {
      return this.renderDatetimePicker();
    }

    return <View
        onLayout={e => {
          let height = e.nativeEvent.layout.height;
          let snapPoints = [height, height];

          if (this.props.getSnapPoints() !== snapPoints) {
            this.props.setSnapPoints(snapPoints, true);
          }
        }}
        style={styles.screen}>
        <View style={styles.container}>
          <Text style={styles.textTitle}>Change order details</Text>

          <TouchableOpacity
            onPress={() => {
              this.props.onChangeLocation && this.props.onChangeLocation()
            }}
          >
          <Text style={styles.textLabel}>Location</Text>
          <Text style={styles.textValue}>{this.orderAddress || this.state.orderAddress}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.setState({showTimePicker: true});
            }}>
            <Text style={styles.textLabel}>Date and time</Text>
            <Text style={styles.textValue}>
              {this.state.orderDate.toString()}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              AsyncStorage.removeItem('selectedLocation').then(() => {
                this.props.onChangeSave({
                  guid: order.guid,
                  started_at: this.state.orderDate.toISOString(),
                  location: this.state.orderLocation,
                })
              })
            }}
            style={[styles.btn, {backgroundColor: appTheme['color-info-500']}]}
            activeOpacity={1}>
            <Text style={[styles.btnText, {color: 'white'}]}>Save</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.props.onChangeCancel}
            style={styles.btn}
            activeOpacity={1}>
            <Text style={styles.btnText}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
  }
}

const styles = StyleSheet.create({
  screen: {
    backgroundColor: appTheme['color-warning-500'],
    borderTopLeftRadius: 20,
    borderTopEndRadius: 20,
  },
  container: {
    paddingHorizontal: 16,
    paddingVertical: 24,
    justifyContent: 'center',
  },
  textTitle: {
    fontSize: 20,
    marginBottom: 24,
    textAlign: 'center',
  },
  textLabel: {
    fontSize: 16,
  },
  textValue: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 24,
    color: appTheme['color-danger-500'],
  },
  time: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
  btn: {
    height: 48,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 8,
  },
  btnText: {
    color: appTheme['color-info-500'],
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default ChangeOrder;
