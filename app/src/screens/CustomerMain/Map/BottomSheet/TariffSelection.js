import React, {Component} from 'react'
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native'
import axios from '../../../../axios';
import StoreContext from '../../../../store/store';
import APIClient from '../../../../core/api'


class TariffSelection extends Component {

    state = {
        tariffs: [
            {
                "amount": 40,
                "currency": {
                    "code": "USD",
                    "rate": 1
                },
                "duration": 20,
                "guid": "5fc247b5-3363-40de-9e88-ee9356181eac",
                "name": "Express"
            },
            {
                "amount": 70,
                "currency": {
                    "code": "USD",
                    "rate": 1
                },
                "duration": 40,
                "guid": "3a8c929d-3159-48ca-bdc0-397b63d7ec33",
                "name": "Classic"
            },
            {
                "amount": 100,
                "currency": {
                    "code": "USD",
                    "rate": 1
                },
                "duration": 60,
                "guid": "84b2edf5-fa0c-4bfd-9b66-44a74ba00df5",
                "name": "Full size"
            },
            {
                "amount": 200,
                "currency": {
                    "code": "USD",
                    "rate": 1
                },
                "duration": 120,
                "guid": "824a7793-de99-497d-bbd6-3c2530d1c6ce",
                "name": "Economy"
            },
        ],
        selected: {},
    };

    componentDidMount() {
        let selected = this.state.selected;

        if (!selected.guid && this.state.tariffs.length > 0) {
            selected = this.state.tariffs[0];

            this.setState({selected})
        }

        APIClient.order.tariffList(resp => {
            let tariffs = resp.data.results;

            if (!selected.guid) {
                selected = tariffs[0];
            }

            this.setState({tariffs, selected})
        })
    }

    render() {
        return <View style={{justifyContent: 'center', alignItems: 'center', width: '100%'}}>
            {this.state.tariffs.map(tariff => {
                let perMin = (tariff.amount / tariff.duration).toFixed(2);
                let pricePerMin = `1 min - ${perMin} ${tariff.currency.code}`;
                let price = `${tariff.amount} ${tariff.currency.code}`;
                let duration = `${tariff.duration} min`;

                return <TouchableOpacity
                    activeOpacity={1}
                    key={'tariff_' + tariff.guid}
                    onPress={() => {
                        if (this.props.onSelect) {
                            this.props.onSelect(tariff)
                        }

                        this.setState({selected: tariff})
                    }}
                    style={[styles.container_order_item, styles.minutes_text_order, tariff.guid === this.state.selected.guid ? styles.container_order_item__active : null]}
                >
                    <View>
                        <View style={styles.wrapper_order_descr}>
                            <Text style={styles.top_text_order}>{tariff.name}</Text>
                            <Text style={styles.top_text_order}>{price}</Text>
                        </View>
                        <View style={styles.wrapper_order_descr}>
                            <Text style={styles.minutes_text_order}>{duration}</Text>
                            <Text style={styles.tariff_text_order}>{pricePerMin}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            })}
        </View>
    }
}

const styles = StyleSheet.create({
    tariff_text_order: {
        fontSize: 15,
        color: '#766A6A'
    },
    minutes_text_order: {
        fontSize: 20
    },
    top_text_order: {
        fontSize: 24,
        fontWeight: 'bold',
        paddingBottom: 5
    },
    wrapper_order_descr: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 20,
    },
    container_order_item__active: {
        backgroundColor: '#DCF4FF'
    },
    container_order_item: {
        width: '100%',
        paddingVertical: 5
    },
    time: {
        fontSize: 24,
        fontWeight: '500'
    },
    area_title: {
        fontSize: 34,
        fontWeight: 'bold',
        marginTop: 10,
        marginBottom: 5,
    },
});

export default TariffSelection
