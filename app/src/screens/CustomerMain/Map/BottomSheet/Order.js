import React, {Component} from 'react';
import {
  Platform,
  Image,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ImageBackground,
  ScrollView,
  SafeAreaView,
  StatusBar,
  AsyncStorage, TextInput, Alert,
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import ProgressCircle from 'react-native-progress-circle';
import APIClient from '../../../../core/api';
import Collapsible from 'react-native-collapsible';
import {requestPayment} from '../../../../core/payment';
import {formatAMPM} from '../../../../core/time';
import {AlertHelper} from '../../../AlertHelper';
import TopBar from '../TopBar';

import faceSad from '../../../../asset/img/faceSad.png';
import faceSad2 from '../../../../asset/img/faceSad2.png';
import faceSad3 from '../../../../asset/img/faceSad3.png';

import {default as appTheme} from '../../../../themes/light.json';
import ChangeOrder from './ChangeOrder';
import RatingView from './Rating';
const {width, height} = Dimensions.get('window');

class OrderView extends Component {
  state = {
    percent: 0,
    showCancel: false,
    showRating: false,
    changeOrderDetails: false,
    selectLocation: false,
  };

  extendOrder(duration, amount) {
    let order = this.props.order;

    requestPayment(
      {
        amount: amount,
        currency: 'USD',
        description: 'Extending photoshoot by ' + duration + ' mins',
      },
      token => {
        // this.context.set('loading', true);

        APIClient.order.extend(
          {
            token_id: token.tokenId,
            guid: order.guid,
            duration,
          },
          resp => {
            // this.setState({showExtendButtons: true})
          },
        );
      },
    );
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      if (this.props.order.meta) {
        let delta = this.props.order.meta.delta;

        if (this.state.percent >= 100) {
          // this.context.set('loading', true);
          return;
        }

        this.setState({
          percent: Math.max(
            this.props.order.meta.percent,
            this.state.percent + delta / 4,
          ),
        });
      }
    }, 250);

    AsyncStorage.getItem('selectedLocation').then(res => {
      res && this.setState({selectLocation: true})
    })
  }

  componentWillUnmount() {
    this.interval && clearInterval(this.interval);
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (
      // this.orderState === 'COMPLETED' &&
      nextProps.order.state !== this.orderState
    ) {
      // this.context.set('loading', false);
      this.setState({percent: 0});
    }

    if (
        this.props.order.meta &&
        nextProps.order.meta &&
        nextProps.order.meta.extend_confirmed !== this.props.order.meta.extend_confirmed
    ) {
      if (nextProps.order.meta.extend_confirmed === 'no') {
        Alert.alert('Photoshoot extending declined', 'Maybe next time.', [{text: 'OK'}])
      }
    }

    this.orderState = nextProps.order.state;

    return true;
  }

  paidAssignedPanelHeight = 0;
  panelHeight = 0;

  renderPaidAssigned() {
    let order = this.props.order;
    let employee = order.employee;
    let startedAt = new Date(order.started_at);
    let minutesToStart = (startedAt.getTime() - new Date().getTime()) / 60000;

    const onLayout = () => {
      let snapPoints = [this.panelHeight, this.paidAssignedPanelHeight];

      if (snapPoints.includes(0)) {
        return;
      }

      if (this.props.getSnapPoints() !== snapPoints) {
        this.props.setSnapPoints(snapPoints, true);
        this.props.open();
      }
    };

    return (
      <View
        onLayout={e => {
          // panelHeight = e.nativeEvent.layout.height;

          this.panelHeight = e.nativeEvent.layout.height;
          onLayout();
        }}
        style={styles.container}>
        <View
          onLayout={e => {
            this.paidAssignedPanelHeight = e.nativeEvent.layout.height;
            onLayout();
            // console.warn(height, panelHeight);

            // if (this.props.getSnapPoints() !== snapPoints) {
            //     this.props.setSnapPoints(snapPoints, true);
            // }
          }}
          style={{}}>
          <View style={{alignItems: 'center'}}>
            <View style={styles.headerBtn} />
          </View>

          <View style={styles.header}>
            <View style={styles.headerImgWrapper}>
              <Image
                source={
                  employee
                    ? {uri: employee.avatar}
                    : require('../../../../asset/img/avatar-default.png')
                }
                style={styles.headerImg}
              />
            </View>
            <View style={styles.headerTextContainer}>
              <Text style={styles.titleHeader}>
                Meet{' '}
                {employee ? (
                  <Text style={{color: '#FAFF00'}}>{employee.username}</Text>
                ) : (
                  'your photographer'
                )}
                {'\u00A0'}
                at:
              </Text>
              <Text style={styles.headerAdressTitle}>
                {this.props.order.address.split(',')[0]}{' '}
                <Text style={styles.headerTitleTime}>
                  in{'\u00A0'}
                  {Math.max(0, minutesToStart.toFixed(0))}
                  {'\u00A0'}min
                </Text>
              </Text>
              <Text style={styles.headerDescr}>
                We will send you information about your photographer soon
              </Text>
            </View>
          </View>
        </View>

        <View style={{alignItems: 'center', marginBottom: 24}}>
          <View style={employeeFoundStyles.imgContainer}>
            {employee ? (
              <TouchableOpacity
                onPress={() => {
                  this.props.showOnMap && this.props.showOnMap();
                  this.props.close();
                }}
                style={employeeFoundStyles.imgWrapper}>
                <Image
                  source={require('./../../../../asset/img/route_line.png')}
                  style={employeeFoundStyles.img}
                />
                <Text style={employeeFoundStyles.imgDescr}>
                  Show{'\n'}directions
                </Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  if (this.props.onBuildRoute) {
                    this.props.onBuildRoute();
                  }

                  this.props.snapTo(1);
                }}
                style={employeeFoundStyles.imgWrapper}>
                <Image
                  source={require('./../../../../asset/img/route_line.png')}
                  style={employeeFoundStyles.img}
                />
                <Text style={employeeFoundStyles.imgDescr}>
                  Build{'\n'}route
                </Text>
              </TouchableOpacity>
            )}

            {employee ? (
              <TouchableOpacity
                onPress={() => {
                  Linking.openURL('tel:' + employee.phone);
                }}
                style={employeeFoundStyles.imgWrapper}>
                <Image
                  source={require('./../../../../asset/img/phone.png')}
                  style={employeeFoundStyles.img}
                />
                <Text style={employeeFoundStyles.imgDescr}>
                  Call your{'\n'}photographer
                </Text>
              </TouchableOpacity>
            ) : null}

            <View style={employeeFoundStyles.imgWrapper}>
              <Image
                source={require('./../../../../asset/img/smile.png')}
                style={employeeFoundStyles.img}
              />
              <Text style={employeeFoundStyles.imgDescr}>Talk{'\n'}to us</Text>
            </View>
          </View>
        </View>

        {employee ? (
          <TouchableOpacity
            onPress={() => {
              this.setState({showCancel: true});

              // this.context.set('loading', true);
              // APIClient.order.cancel({guid: order.guid},
              // () => this.context.set('loading', false),
              // () => this.context.set('loading', false))
            }}
            style={{paddingVertical: 8, marginBottom: 16}}>
            <Text style={{color: '#9D9CA1', fontSize: 20, textAlign: 'center'}}>
              Cancel with {order.cancellation_fee}$
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            onPress={() => {
              this.setState({showCancel: true});
            }}
            style={{paddingVertical: 8, marginBottom: 16}}>
            <Text style={{color: '#9D9CA1', fontSize: 20, textAlign: 'center'}}>
              Cancel order
            </Text>
          </TouchableOpacity>
        )}

        {employee ? null : (
          <TouchableOpacity
            onPress={() => {
              this.setState({changeOrderDetails: true});
            }}
            style={{paddingVertical: 8, marginBottom: 24}}>
            <Text style={{color: '#9D9CA1', fontSize: 20, textAlign: 'center'}}>
              Change order details
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }

  renderReady() {
    let order = this.props.order;
    let employee = order.employee;

    return (
      <SafeAreaView style={employeeFoundStyles.screen}>
        <ScrollView
          centerContent={Platform.OS === 'ios' ? 'true' : 'false'}
          style={employeeFoundStyles.container}>
          <View
            style={{
              alignItems: 'center',
              marginTop: 48,
              marginBottom: 16,
            }}>
            <View>
              <Image
                style={employeeFoundStyles.avatarImg}
                source={{uri: employee.avatar}}
              />
              <ImageBackground
                style={employeeFoundStyles.imgStar}
                source={require('./../../../../asset/img/star.png')}>
                <Text style={employeeFoundStyles.rating}>4.9</Text>
              </ImageBackground>
            </View>
          </View>

          <View
            style={{
              marginBottom: 16,
              alignItems: 'center',
            }}>
            <Text style={employeeFoundStyles.mainText}>YOUR PHOTOGRAPHER</Text>
            <Text style={employeeFoundStyles.yelowText}>
              {employee.username}
            </Text>
            <Text style={employeeFoundStyles.mainText}>IS HERE</Text>
            <Text
              style={{
                backgroundColor: appTheme['color-info-500'],
                ...employeeFoundStyles.mainText,
              }}>
              LIFT UP YOUR PHONE
            </Text>
            <Text style={employeeFoundStyles.mainText}>TO BE FOUND</Text>
          </View>

          <View
            style={{
              marginBottom: 24,
              alignItems: 'center',
            }}>
            <View style={employeeFoundStyles.imgContainer}>
              <View style={employeeFoundStyles.imgWrapper}>
                <Image
                  source={require('./../../../../asset/img/route_line.png')}
                  style={employeeFoundStyles.img}
                />
                <Text style={employeeFoundStyles.imgDescrLight}>
                  Show{'\n'}directions
                </Text>
              </View>
              <TouchableOpacity
                onPress={() => Linking.openURL('tel:' + employee.phone)}
                style={employeeFoundStyles.imgWrapper}>
                <Image
                  source={require('./../../../../asset/img/phone.png')}
                  style={employeeFoundStyles.img}
                />
                <Text style={employeeFoundStyles.imgDescrLight}>
                  Call your{'\n'}photographer
                </Text>
              </TouchableOpacity>
              <View style={employeeFoundStyles.imgWrapper}>
                <Image
                  source={require('./../../../../asset/img/smile.png')}
                  style={employeeFoundStyles.img}
                />
                <Text style={employeeFoundStyles.imgDescrLight}>
                  Talk{'\n'}to us
                </Text>
              </View>
            </View>
          </View>

          <View style={{alignItems: 'center', marginBottom: 16}}>
            <Text style={employeeFoundStyles.qrDescr}>
              In order to start session show your{'\n'}photographer this QR code
            </Text>
          </View>

          <View style={{alignItems: 'center', marginBottom: 16}}>
            <QRCode
              value={order.guid}
              size={192}
              // backgroundColor='transparent'
            />
          </View>

          <TouchableOpacity
            onPress={() => {
              this.setState({showCancel: true});

              // this.context.set('loading', true);
              // APIClient.order.cancel({guid: order.guid},
              //     () => this.context.set('loading', false),
              //     () => this.context.set('loading', false))
            }}
            style={{
              alignItems: 'center',
              paddingVertical: 16,
              marginBottom: 48,
            }}>
            <Text style={employeeFoundStyles.cancelText}>
              Cancel with{' '}
              <Text style={{fontWeight: 'bold'}}>
                ${order.cancellation_fee}
              </Text>
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }

  renderWarmUp() {
    let order = this.props.order;
    let employee = order.employee;

    return (
      <SafeAreaView style={warmUpStyles.container}>
      <StatusBar
        barStyle={'light-content'}
        translucent={true}
        backgroundColor={'transparent'}
      />
        <ImageBackground
          source={require('../../../../asset/img/warmUpBackground.png')}
          style={{
            flex: 1,
            paddingHorizontal: 24,
          }}>
          <View style={{flexGrow: 1, justifyContent: 'center'}}>
            <Text style={warmUpStyles.title}>WARMING UP</Text>
            <View style={warmUpStyles.timer}>
              <ProgressCircle
                percent={this.state.percent || 0}
                radius={60}
                borderWidth={8}
                shadowColor="#4D4BD5"
                color="#FAFF00"
                bgColor="#3937D0">
                <Text style={warmUpStyles.timerText}>
                  {(order.meta.remain / 60).toFixed(0)}
                </Text>
                <Text style={warmUpStyles.timerTextMinutes}>min</Text>
              </ProgressCircle>
            </View>
          </View>

          <Text style={warmUpStyles.textDescr}>
            Brush your hair,{'\n'}
            check your lipstick,{'\n'}
            get the spinach{'\n'}
            out of your teeth!{'\n'}
            and don't forget{'\n'}
            to enjoy the process!
          </Text>

          <View style={{flexGrow: 0}}>
            <TouchableOpacity
              onPress={() => {
                this.setState({showCancel: true});
              }}
              style={{
                height: 56,
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 16,
              }}>
              <Text style={warmUpStyles.cancelText}>
                Cancel with
                <Text style={{fontWeight: 'bold'}}>
                  {' '}
                  ${order.cancellation_fee}
                </Text>
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }

  renderProcessing() {
    let order = this.props.order;
    let employee = order.employee;

    return (
      <SafeAreaView style={processingStyles.container}>
        <ImageBackground
          source={require('../../../../asset/img/processingBackground.png')}
          style={{
            flex: 1,
            paddingHorizontal: 24,
          }}>
          <View style={{flexGrow: 1, justifyContent: 'center'}}>
            <Text style={processingStyles.title}>Photoshoot in progress</Text>
            <View style={processingStyles.timer}>
              <ProgressCircle
                percent={this.state.percent || 0}
                radius={60}
                borderWidth={8}
                shadowColor="#33A745"
                color="#FAFF00"
                bgColor="#009218">
                <Text style={processingStyles.timerText}>
                  {(order.meta.remain / 60).toFixed(0)}
                </Text>
                <Text style={processingStyles.timerTextMinutes}>min</Text>
              </ProgressCircle>
            </View>
          </View>

          <View style={processingStyles.descrPhotograph}>
            <View>
              <Image
                style={processingStyles.avatarImg}
                source={{uri: employee.avatar}}
              />
              <ImageBackground
                style={processingStyles.starImage}
                source={require('../../../../asset/img/star.png')}>
                <Text style={processingStyles.ratingText}>4.9</Text>
              </ImageBackground>
            </View>
            <View>
              <Text style={processingStyles.textPhotograph}>
                Your photographer today:
              </Text>
              <Text style={processingStyles.nameTextPhotograph}>
                {employee.username}
              </Text>
            </View>
          </View>

          <View style={{flexGrow: 0}}>
            <TouchableOpacity
              onPress={() => {
                this.setState({showCancel: true});

                // APIClient.order.state({
                //   guid: order.guid,
                //   state: 'COMPLETED',
                // });
              }}
              style={[processingStyles.btnFinish]}>
              <Text style={processingStyles.btnFinishText}>
                Finish photoshoot
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }

  renderCompleted() {
    let order = this.props.order;

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: appTheme['color-info-500'],
        }}>
        <StatusBar
          barStyle={'light-content'}
          translucent={true}
          backgroundColor={'transparent'}
        />
        <ImageBackground
          source={require('../../../../asset/img/completedBg.png')}
          style={{
            flex: 1,
          }}>
          <ScrollView alwaysBounceVertical={false} style={completedStyles.container}>
            <Image
              style={completedStyles.successImage}
              source={require('../../../../asset/img/check.png')}
            />
            <Text style={completedStyles.title}>
              Your photoshoot{'\n'}is completed
            </Text>
            <Text style={completedStyles.subTitle}>
              Total: {parseInt(order.duration) + 3} minutes
            </Text>
            <Text style={[completedStyles.title, {marginBottom: 56}]}>
              Do you want to extend?
            </Text>
            <View styles={completedStyles.btnContainer}>
              <TouchableOpacity
                onPress={() => {
                  let extendConfimed = order.meta.extend_confirmed;

                  switch (extendConfimed) {
                    case 'unknown':
                      APIClient.order.extendConfirm(
                          {
                            guid: order.guid,
                          },
                          () => {
                            AlertHelper.show('success', 'Extending requested');
                          },
                      );
                      break;

                    case 'requested':
                      Alert.alert('Photoshoot extending', 'Already requested', [{text: 'OK'}])
                      break;

                    case 'no':
                      Alert.alert('Photoshoot extending', 'Maybe next time', [{text: 'OK'}])
                      break;
                  }
                }}
                style={[
                  completedStyles.btn,
                  {backgroundColor: appTheme['color-danger-500']},
                ]}>
                <Text style={[completedStyles.btnText, {color: 'white'}]}>
                  Yes, I want more
                </Text>
              </TouchableOpacity>

              <Collapsible
                collapsed={
                  !(order.meta && order.meta.extend_confirmed === 'yes')
                }>
                <View style={completedStyles.containerBtnPrice}>
                  <TouchableOpacity
                    onPress={() => this.extendOrder(10, 10.0)}
                    style={completedStyles.btnPrice}>
                    <Text style={completedStyles.btnTimeText}>10 min</Text>
                    <Text style={completedStyles.btnPriceText}>10 $</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.extendOrder(20, 20.0)}
                    style={completedStyles.btnPrice}>
                    <Text style={completedStyles.btnTimeText}>20 min</Text>
                    <Text style={completedStyles.btnPriceText}>20 $</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.extendOrder(30, 30.0)}
                    style={completedStyles.btnPrice}>
                    <Text style={completedStyles.btnTimeText}>30 min</Text>
                    <Text style={completedStyles.btnPriceText}>30 $</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.extendOrder(40, 40.0)}
                    style={completedStyles.btnPrice}>
                    <Text style={completedStyles.btnTimeText}>40 min</Text>
                    <Text style={completedStyles.btnPriceText}>40 $</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.extendOrder(50, 50.0)}
                    style={completedStyles.btnPrice}>
                    <Text style={completedStyles.btnTimeText}>50 min</Text>
                    <Text style={completedStyles.btnPriceText}>50 $</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.extendOrder(60, 60.0)}
                    style={completedStyles.btnPrice}>
                    <Text style={completedStyles.btnTimeText}>60 min</Text>
                    <Text style={completedStyles.btnPriceText}>60 $</Text>
                  </TouchableOpacity>
                </View>
              </Collapsible>

              <TouchableOpacity
                onPress={() => {
                  this.props.onDone && this.props.onDone();
                }}
                style={{
                  ...completedStyles.btn,
                  backgroundColor: appTheme['color-warning-500'],
                  marginBottom: 24,
                }}>
                <Text style={completedStyles.btnText}>No, complete</Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  if (order.rating === 0) {
                    this.setState({showRating: true})
                  } else {
                    Alert.alert('Order rating', 'Already rated', [{text: 'OK'}])
                  }
                }}
                style={{
                  ...completedStyles.btn,
                  backgroundColor: appTheme['color-danger-500'],
                  marginBottom: 24,
                }}
              >
                <Text>Rate photographer</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </SafeAreaView>
    );
  }

  renderRating() {
    return <RatingView
        onConfirm={rating => {
          APIClient.order.rate({
            guid:this.props.order.guid,
            rating,
          }, () => {
            this.setState({showRating: false})
          })
        }}
        onCancel={() => this.setState({showRating: false})}
    />
  }

  renderCancel() {
    let order = this.props.order;

    return (
      <SafeAreaView
        onLayout={e => {
          this.props.setSnapPoints([height, height], false);
        }}
        style={{
          backgroundColor: '#6430B8',
          height,
          width,
        }}>
        <StatusBar
          barStyle={'light-content'}
          translucent={true}
          backgroundColor={'transparent'}
        />
        <ImageBackground
          source={require('../../../../asset/img/cancelBackground.png')}
          style={{
            flex: 1
          }}>
        <TouchableOpacity
          onPress={() => {
            this.setState({showCancel: false});
          }}
          style={{
            alignItems: 'flex-end',
            paddingHorizontal: 16,
            paddingTop: 24,
          }}>
          <Text
            style={{
              fontSize: 40,
              color: 'white',
            }}>
            &times;
          </Text>
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            paddingHorizontal: 24,
            marginTop: 24,
          }}>
          <View
            style={{
              flexGrow: 1,
            }}>
            {/* <Image
              source={faceSad}
              style={{
                position: 'absolute',
                bottom: 52,
                left: 29,
              }}
            />
            <Image
              source={faceSad2}
              style={{
                position: 'absolute',
                top: 194,
                right: 52,
              }}
            /> */}
            <View
              style={{
                position: 'absolute',
                alignSelf: 'center',
                height: 29,
                top: 23,
                width: 164,
                backgroundColor: appTheme['color-warning-500'],
                transform: [{translateX: 72}],
              }}
            />
            <Text
              style={{
                color: 'white',
                fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
                fontSize: 24,
                lineHeight: 29,
                textTransform: 'uppercase',
                fontWeight: 'bold',
                textAlign: 'center',
                marginBottom: 16,
              }}>
              Are you sure that
              {'\n'}you want{' '}
              <Text
                style={{
                  color: 'black',
                }}>
                to cancel
              </Text>
              {'\n'}the order?
            </Text>
            <Text
              style={{
                fontSize: 34,
                textAlign: 'center',
                color: 'white',
                fontWeight: 'bold',
                marginBottom: 8,
              }}>
              {this.props.order.address.split(',')[0]}
            </Text>
            <Text
              style={{
                fontSize: 24,
                lineHeight: 29,
                textAlign: 'center',
                color: 'white',
                fontWeight: '500',
                marginBottom: 8,
              }}>
              at
            </Text>
            <Text
              style={{
                fontSize: 24,
                lineHeight: 29,
                textAlign: 'center',
                color: 'white',
                fontWeight: '500',
              }}>
              {order.started_at ? formatAMPM(new Date(order.started_at)) : null}
            </Text>
          </View>
          <View
            style={{
              flexGrow: 0,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({showCancel: false});
              }}
              style={{
                height: 56,
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 8,
              }}>
              <Text
                style={{
                  color: appTheme['color-warning-500'],
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                No, we're still on
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                switch (order.state) {
                  case 'READY':
                  case 'WARM_UP':
                  case 'PAID':
                  case 'ASSIGNED':
                    APIClient.order.cancel({guid: order.guid}, () => {
                      this.props.onCancel && this.props.onCancel();
                      this.setState({showCancel: false});
                    });
                    break;

                  case 'PROCESSING':
                    APIClient.order.state({
                        guid: order.guid,
                        state: 'COMPLETED',
                      }, () => {
                        this.setState({showCancel: false});
                      },
                    );
                    break;
                }
              }}
              style={{
                height: 56,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: appTheme['color-warning-500'],
                marginBottom: 16,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 20,
                  fontWeight: 'bold',
                }}>
                Cancel order
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }

  renderLate() {
    let order = this.props.order;

    return <View
        onLayout={e => {
          let h = e.nativeEvent.layout.height;

          this.props.setSnapPoints([h, h], true);
          this.props.open()
        }}
        style={styles.container}
    >
      <View style={{alignItems: 'center'}}>
        <View style={lateStyles.header}>
          <View style={{width: 56}}>
            <Image style={lateStyles.avatarImg} source={require('../../../../asset/img/avatar.png')}/>
            <Image style={lateStyles.imgStar}
                   source={require('../../../../asset/img/star.png')}/>
            <Text style={lateStyles.raiting}>4.9</Text>
          </View>
          <View style={lateStyles.hederTextContainer}>
            <Text style={lateStyles.hederTextContainerDescr}>Meet <Text style={{color: '#FAFF00', fontWeight: 'bold'}}>{order.employee.username}</Text> at:</Text>
            <Text style={{...lateStyles.hederTextContainerDescr, fontWeight: 'bold'}}>She was <Text style={{color: '#8B8989'}}>{Math.max(1, this.props.order.meta.late_minutes)} min late</Text></Text>
          </View>
        </View>
        <View style={{alignItems: 'center', marginVertical: 20}}>
          <Text style={{...lateStyles.qrDescr, fontSize: 25}}>
            We are very sorry, but your photographer is a bit late
          </Text>
        </View>
        <View style={{alignItems: 'center', marginVertical: 20}}>
          <Text style={{...lateStyles.qrDescr, fontSize: 25}}>You will prolongate your photoshoot for 7 min as a gift</Text>
        </View>
        <View style={lateStyles.imgContainer}>
          <View style={lateStyles.imgWrapper}>
            <Image source={require('../../../../asset/img/phone.png')} style={lateStyles.img}/>
            <Text style={lateStyles.imgDescr}>Call your</Text>
            <Text style={lateStyles.imgDescr}>photographer</Text>
          </View>
          <View style={lateStyles.imgWrapper}>
            <Image source={require('../../../../asset/img/smile.png')} style={lateStyles.img}/>
            <Text style={lateStyles.imgDescr}>Talk</Text>
            <Text style={lateStyles.imgDescr}>to us</Text>
          </View>
        </View>
        <TouchableOpacity
            onPress={() => {
              this.setState({showCancel: true});
            }}
            style={lateStyles.CancelForFree}
        >
          <Text style={lateStyles.qrDescr}>Cancel for free</Text>
        </TouchableOpacity>
      </View>
    </View>
  }

  renderNotFound() {
    return <View
        onLayout={e => {
          let h = e.nativeEvent.layout.height;

          this.props.setSnapPoints([h, h], true);
        }}
        style={styles.container}
    >
      <View
          style={{width: '100%', height: 350, alignItems: 'center', justifyContent: 'center'}}
      >
        <Text style={{fontSize: 25, color: 'white'}}>Photographer Not found</Text>
      </View>
    </View>
  }

  render() {
    let order = this.props.order;

    if (this.state.showCancel) {
      return this.renderCancel();
    }

    if (this.state.showRating) {
      return this.renderRating()
    }

    if (this.state.changeOrderDetails || this.state.selectLocation) {
      return <ChangeOrder
          onChangeSave={changedOrder => {
            APIClient.order.change(
              changedOrder,
              () => {
                this.props.onChange()
                this.setState({changeOrderDetails: false, selectLocation: false})
              },
              () => this.setState({changeOrderDetails: false, selectLocation: false}))
          }}
          onChangeCancel={() => {
            this.setState({
              changeOrderDetails: false,
              selectLocation: false,
            })
            AsyncStorage.removeItem('selectedLocation').then()
          }}
          {...this.props}
      />
    }

    switch (order.state) {
      case 'PAID':
      case 'ASSIGNED':
        if (order.meta && order.meta.late) {
          return this.renderLate();
        }

        if (order.meta && order.meta.not_found) {
          return this.renderNotFound();
        }

        return this.renderPaidAssigned();

      case 'READY':
        return this.renderReady();

      case 'WARM_UP':
        return this.renderWarmUp();

      case 'PROCESSING':
        return this.renderProcessing();

      case 'COMPLETED':
        return this.renderCompleted();
    }

    return <View />;
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    backgroundColor: '#000D',
    borderTopLeftRadius: 20,
    borderTopEndRadius: 20,
    paddingVertical: 16,
    paddingHorizontal: 16,
  },
  //   bodyImgWrapper: {
  // 	width: 50,
  // 	height: 50,
  // 	backgroundColor: '#3937D0',
  // 	borderRadius: 100,
  // 	justifyContent: 'center',
  // 	alignItems: 'center',
  //   },
  headerImgWrapper: {
    marginTop: 16,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    width: 64,
    height: 64,
    borderRadius: 32,
    borderWidth: 4,
    borderColor: '#FAFF00',
  },
  headerImg: {
    width: 48,
    height: 48,
    borderRadius: 24,
    backgroundColor: '#fff',
  },
  header: {
    paddingVertical: 16,
    marginBottom: 34,
  },
  titleHeader: {
    color: 'white',
    fontSize: 20,
    paddingBottom: 4,
  },
  headerTextContainer: {
    marginLeft: 78,
  },
  headerAdressTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    paddingBottom: 4,
  },
  headerTitleTime: {
    color: '#8B8A89',
  },
  headerDescr: {
    color: '#C8C8C8',
    fontSize: 16,
  },
  headerBtn: {
    borderRadius: 2,
    width: 34,
    height: 4,
    backgroundColor: '#E5E3E544',
  },
});

const employeeFoundStyles = StyleSheet.create({
  screen: {
    backgroundColor: appTheme['color-danger-500'],
    flex: 1,
    height,
    width,
    position: 'absolute',
  },
  container: {
    backgroundColor: appTheme['color-danger-500'],
    flex: 1,
    height,
    width,
    position: 'absolute',
    paddingHorizontal: Platform.OS === 'ios' ? 0 : 24,
    paddingVertical: 24,
  },
  avatarImg: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  imgStar: {
    width: 56,
    height: 56,
    position: 'absolute',
    right: -16,
    top: -16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rating: {
    fontSize: 20,
    fontWeight: 'bold',
    color: appTheme['color-danger-500'],
    paddingTop: 2,
  },
  mainText: {
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    fontSize: 24,
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
  yelowText: {
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: appTheme['color-warning-500'],
  },
  imgContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  imgWrapper: {
    width: width / 3,
    alignItems: 'center',
  },
  img: {
    width: 48,
    height: 48,
    marginBottom: 16,
  },
  imgDescr: {
    fontSize: 16,
    color: '#9D9CA1',
    textAlign: 'center',
  },
  imgDescrLight: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  },
  cancelText: {
    color: 'white',
    fontSize: 20,
  },
  qrImg: {
    width: 100,
    height: 100,
  },
  qrDescr: {
    fontWeight: '500',
    fontSize: 18,
    textAlign: 'center',
  },
});

const warmUpStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appTheme['color-info-500'],
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    marginBottom: 24,
  },
  timer: {
    alignItems: 'center',
  },
  timerText: {
    color: 'white',
    fontSize: 35,
    fontWeight: 'bold',
  },
  timerTextMinutes: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  cancelText: {
    color: 'white',
    fontSize: 20,
    lineHeight: 32,
  },
  textDescr: {
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    textTransform: 'uppercase',
    flexGrow: 0,
    marginBottom: 32,
  },
});

const processingStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00AC1C',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    marginBottom: 24,
  },
  timer: {
    alignItems: 'center',
  },
  timerText: {
    color: 'white',
    fontSize: 35,
    fontWeight: 'bold',
  },
  timerTextMinutes: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
  },
  descrPhotograph: {
    flexDirection: 'row',
    flexGrow: 0,
    marginBottom: 54,
    alignItems: 'center',
  },
  avatarImg: {
    width: 56,
    height: 56,
    borderRadius: 28,
    marginRight: 12,
  },
  starImage: {
    width: 32,
    height: 32,
    position: 'absolute',
    left: -12,
    top: -12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ratingText: {
    fontSize: 10,
    fontWeight: 'bold',
    color: appTheme['color-danger-500'],
    paddingTop: 1,
  },
  textPhotograph: {
    fontSize: 16,
    color: 'white',
  },
  nameTextPhotograph: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  btnFinishText: {
    color: 'white',
    fontSize: 20,
    lineHeight: 32,
  },
  btnFinish: {
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
    backgroundColor: 'black',
  },
  rating: {
    position: 'absolute',
    color: '#FF3DC8',
    top: -5,
    left: -6,
    fontSize: 10,
    fontWeight: 'bold',
  },
});

const completedStyles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
  },
  successImage: {
    width: 72,
    height: 72,
    alignSelf: 'center',
    marginVertical: 24,
  },
  title: {
    color: 'white',
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    fontSize: 24,
    lineHeight: 29,
    textTransform: 'uppercase',
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 24,
  },
  subTitle: {
    fontSize: 24,
    lineHeight: 29,
    textAlign: 'center',
    color: 'white',
    fontWeight: '500',
    marginBottom: 24,
  },
  btnContainer: {
    marginBottom: 96,
  },
  btn: {
    height: 56,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 8,
  },
  btnText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  containerBtnPrice: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  btnPrice: {
    justifyContent: 'center',
    alignItems: 'center',
    width: (width - 64) / 3,
    backgroundColor: 'white',
    paddingVertical: 8,
    marginBottom: 8,
  },
  btnTimeText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: appTheme['color-info-500'],
    marginBottom: 4,
  },
  btnPriceText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#FF3DC8cc',
    marginBottom: '3%',
  },
});

const lateStyles = StyleSheet.create({
  cancelText: {
    color: 'white',
    fontSize: 20
  },
  CancelForFree: {
    marginTop: 30,
    width: "110%",
    alignItems: 'center',
    height: 100,
    justifyContent: 'center',
    borderTopWidth: 1,
    borderColor: '#4F4E4C'
  },
  qrImg: {
    width: 100,
    height: 100
  },
  qrDescr: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    color: '#B7B7B7'
  },
  imgDescr: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
  },
  raiting: {
    position: 'absolute',
    right: -8,
    top: 0,
    fontSize: 11,
    fontWeight: 'bold',
    color: '#FF3DC8'
  },
  imgStar: {
    position: 'absolute',
    right: -15,
    top: -10,
    width: 32,
    height: 32
  },
  container: {
    display: 'flex',
    justifyContent: 'space-around',
    backgroundColor: '#FF3DC8',
    width: '100%',
    height: '100%',
    paddingHorizontal: 10
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '110%',
    height: 100,
    paddingLeft: 40,
    borderBottomWidth: 1,
    borderColor: '#4F4E4C'
  },
  hederTextContainer: {
    marginLeft: 30,
    height: '70%',
    justifyContent: 'space-around'
  },
  hederTextContainerDescr: {
    color: 'white',
    fontSize: 20
  },
  avatarImg: {
    width: 60,
    height: 60,
    borderRadius: 100,
    borderWidth: 4,
    borderColor: '#514F4D'
  },
  img: {
    width: 48,
    height: 48,
    marginBottom: '5%'
  },
  imgWrapper: {
    width: '33%',
    alignItems: 'center'
  },
  imgContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    marginTop: '5%'
  },
  mainText: {
    fontSize: 24,
    textAlign: 'center',
    paddingHorizontal: 5,
    color: 'white',
    fontWeight: 'bold'
  },
  yelowText: {
    fontSize: 30,
    width: 150,
    textAlign: 'center',
    fontWeight: 'bold',
    backgroundColor: '#FAFF00'
  }
})

export default OrderView;
