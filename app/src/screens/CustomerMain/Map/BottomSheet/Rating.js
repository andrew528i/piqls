import React, {Component} from 'react'
import {View, Text, Image, TextInput, TouchableOpacity, StyleSheet} from 'react-native';


class RatingView extends Component {
    state = {
        value: 0,
    }

    render() {
        let array = [];

        for (let i = 0; i < 5; i += 1) {
            array.push(<TouchableOpacity
                onPress={() => this.setState({value: i + 1})}
                activeOpacity={1}
                style={[styles.imageWrapper, i < this.state.value ? {backgroundColor: '#FF3DC8'} : null]}
                key={i}
            >
                <Image style={{width: 30, height: 30}} source={require('../../../../asset/img/piqls.png')}/>
            </TouchableOpacity>)
        }

        return <View style={styles.container}>
            <View style={styles.main}>
                <View style={styles.textBGColor}/>
                <Text style={styles.title}>Be a sweety, tell us</Text>
                <Text style={styles.title}>about your experience!</Text>
                <Text style={styles.textQuestion}>How did you like the photographer?</Text>
            </View>
            <View style={{alignItems: 'center', marginTop: '5%'}}>
                <Image style={{width: 100, height: 100}} source={require('../../../../asset/img/smile.png')}/>
                <View style={styles.imageListWrapper}>
                    {array}
                </View>
                <TextInput
                    placeholder='Share your thoughts...'
                    multiline={true}
                    numberOfLines={4}
                    style={styles.textInput_style}
                />
                <TouchableOpacity
                    onPress={() => {
                        this.props.onConfirm && this.props.onConfirm(this.state.value)
                    }}
                    style={styles.btn}
                >
                    <Text style={styles.btnText}>Okay</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        this.props.onCancel && this.props.onCancel()
                    }}
                    style={styles.btn}
                >
                    <Text style={styles.btnText}>Cancel</Text>
                </TouchableOpacity>
            </View>
        </View>
    }
}

const styles = StyleSheet.create({
    btnText: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    btn: {
        height: 56,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        width: '90%',
        marginTop: '5%'
    },
    textInput_style: {
        backgroundColor: 'white',
        width: '90%',
        height: '30%',
        textAlignVertical: 'top',
        borderWidth: 2,
        borderColor: '#3937D0',
        paddingLeft: 15,
        fontSize: 20
    },
    imageListWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        paddingHorizontal: 40,
        paddingVertical: '5%'
    },
    imageWrapper: {
        height: 48,
        width: 48,
        borderRadius: 100,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textQuestion: {
        textAlign: 'center',
        marginTop: '5%',
        fontSize: 20
    },
    textBGColor: {
        position: 'absolute',
        width: '28%',
        height: 30,
        backgroundColor: '#4DFF0F',
        right: '15%',
        top: -10
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'center',
        letterSpacing: 2
    },
    main: {
        marginTop: '25%',
    },
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: '#FAFF00'
    }
});

export default RatingView
