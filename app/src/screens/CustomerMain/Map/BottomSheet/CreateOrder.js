import React, {Component} from 'react';
import {
  Platform,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import {formatAMPM} from '../../../../core/time';
import TariffSelection from './TariffSelection';
import {AlertHelper} from '../../../AlertHelper';
import {requestPayment} from '../../../../core/payment';
import APIClient from '../../../../core/api';
import StoreContext, {store} from '../../../../store/store';
import EventBus from '../../../../core/event_bus';
import DatePicker from 'react-native-date-picker';
import {setLoadingState} from '../../../../store/loading/actions';

import {default as appTheme} from '../../../../themes/light.json';
const {width, height} = Dimensions.get('screen');

function getMinimumDate() {
  let minimumDate = new Date();

  minimumDate.setTime(minimumDate.getTime()); // + (3600 * 1000));

  return minimumDate;
}

function getMaximumDate() {
  let maximumDate = new Date();

  maximumDate.setDate(maximumDate.getDate() + 14); // + 1);

  return maximumDate;
}

class CreateOrderView extends Component {
  createdOrder = {};
  snapPoints = [];

  state = {
    startedAt: getMinimumDate(),
    minimumDate: getMinimumDate(),
    maximumDate: getMaximumDate(),
    showTimePicker: false,
    showSuccess: false,
  };

  createOrder(tokenId, tariffGuid) {
    let order = {
      location: this.props.order.location,
      token_id: tokenId,
      tariff_guid: tariffGuid,
      started_at: this.state.startedAt.toISOString(),
    };

    APIClient.order.create(order, resp => {
      this.createdOrder = resp.data;
      this.createdOrder.state = 'PAID';

      // if (this.createdOrder.location && this.createdOrder.location) {
      //     this.createdOrder.location = {
      //         latitude: this.createdOrder.location.coordinates[0],
      //         longitude: this.createdOrder.location.coordinates[1],
      //     };
      // }

      // this.props.onCreate && this.props.onCreate(this.createdOrder)
      this.setState({showSuccess: true});
    });
  }

  componentDidMount() {
    EventBus.addListener(
      'bs_close',
      (this.closeListener = data => {
        this.props.onCreateOrderClose();
      }),
    );
  }

  componentWillUnmount() {
    EventBus.removeListener(this.closeListener);
  }

  renderSuccess() {
    if (
      this.snapPoints.length > 0 &&
      this.props.getSnapPoints() !== this.snapPoints
    ) {
      this.props.setSnapPoints(this.snapPoints, true);
      this.props.open();
    }

    return (
      <SafeAreaView
        onLayout={e => {
          let height = e.nativeEvent.layout.height;
          let snapPoints = [height, height];

          if (this.props.getSnapPoints() !== snapPoints) {
            this.snapPoints = snapPoints;
            this.props.setSnapPoints(snapPoints, true);
          }
        }}
        style={styles.successContainer}>
        <StatusBar
          barStyle={'dark-content'}
          translucent={true}
          backgroundColor={'transparent'}
        />
        <TouchableOpacity
          onPress={() => {
            this.setState({showSuccess: false});
            this.props.onSuccess && this.props.onSuccess(this.createdOrder);
          }}
          style={{
            alignItems: 'flex-end',
            paddingHorizontal: 16,
          }}>
          <Text
            style={{
              fontSize: 40,
              color: 'white',
            }}>
            &times;
          </Text>
        </TouchableOpacity>
        <View style={styles.successWrapper}>
          <Text style={styles.successIcon}>✔︎</Text>
          <Text style={styles.successText}>Success</Text>
        </View>
        <View style={styles.addressWrapper}>
          <Text style={styles.addressTextTop}>{this.state.addressShort}</Text>
          <Text style={styles.addressTextBottom}>{this.state.addressFull}</Text>
        </View>
        <View style={styles.timeWrapper}>
          <Text style={styles.timeTextTop}>
            {formatAMPM(this.state.startedAt)}
          </Text>
          <Text style={styles.timeTextBottom}>
            Photoshoot durating{' '}
            {this.tariffSelection
              ? this.tariffSelection.state.selected.duration
              : 20}{' '}
            min
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={styles.photographer}>
            We will update you about your photographer soon
          </Text>
        </View>
        <View style={{paddingHorizontal: 20, marginTop: '10%'}}>
          <TouchableOpacity
            onPress={() => {
              this.setState({showSuccess: false});
              this.props.onSuccess && this.props.onSuccess(this.createdOrder);
            }}
            style={styles.btn}>
            <Text
              style={{fontSize: 24, textAlign: 'center', fontWeight: 'bold'}}>
              I got it
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }

  renderDatetimePicker() {
    return (
      <View
        onLayout={e => {
          let height = e.nativeEvent.layout.height;
          let snapPoints = [0, height];

          if (this.props.getSnapPoints() !== snapPoints) {
            this.props.setSnapPoints(snapPoints, true);
          }
        }}
        style={styles.container}>
        <DatePicker
          date={this.state.startedAt}
          onDateChange={date => {
            this.setState({startedAt: date});
          }}
          minimumDate={this.state.minimumDate}
          maximumDate={this.state.maximumDate}
          minuteInterval={5}
        />
        <TouchableOpacity
          onPress={() => {
            this.setState({showTimePicker: false});
          }}
          style={styles.btn_wrapper}>
          <View style={styles.btn}>
            <Text style={styles.btn_text}>Confirm</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    let order = this.props.order;
    let addressFull = order.address;
    let addressShort = order.address.split(',')[0];

    if (this.state.showSuccess) {
      return this.renderSuccess();
    }

    if (this.state.showTimePicker) {
      return this.renderDatetimePicker();
    }

    return (
      <View
        onLayout={e => {
          let height = e.nativeEvent.layout.height;
          let snapPoints = [0, height];

          if (this.props.getSnapPoints() !== snapPoints) {
            this.props.setSnapPoints(snapPoints, true);
          }
        }}
        style={styles.container}>
        <Text>Meet photographer at:</Text>
        <Text style={styles.areaTitle}>{addressShort}</Text>
        <Text style={{paddingHorizontal: 15, textAlign: 'center'}}>
          {addressFull}
        </Text>
        <Text style={{marginVertical: 15}}>at</Text>

        <TouchableOpacity
          onPress={() => {
            this.setState({
              showTimePicker: true,
              minimumDate: getMinimumDate(),
              maximumDate: getMaximumDate(),
            });
          }}
          style={{marginBottom: 15}}>
          <Text style={styles.time}>{formatAMPM(this.state.startedAt)}</Text>
        </TouchableOpacity>

        <TariffSelection
          style={{backgroundColor: 'orange'}}
          ref={tariff => (this.tariff = tariff)}
        />

        <TouchableOpacity
          onPress={() => {
            let tariff = this.tariff.state.selected;

            if (!tariff || !tariff.guid) {
              AlertHelper.show(
                'error',
                'Cannot proceed',
                'Please select tariff',
              );
              return;
            }

            requestPayment(
              {
                amount: tariff.amount,
                currency: 'USD',
                description: 'Photoshoot for ' + tariff.duration + ' mins',
              },
              token => {
                this.createOrder(token.tokenId, tariff.guid);
              },
            );
          }}
          style={styles.btn_wrapper}>
          <View style={styles.btn}>
            <Text style={styles.btn_text}>Confirm</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 15,
  },
  areaTitle: {
    fontSize: 34,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 5,
    paddingHorizontal: 15,
  },
  time: {
    fontSize: 24,
    fontWeight: '500',
  },
  btn_wrapper: {
    marginTop: 20,
    paddingHorizontal: 20,
    width: '100%',
  },
  btn_text: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btn: {
    height: 56,
    backgroundColor: '#FF3DC8',
    justifyContent: 'center',
    width: '100%',
    marginBottom: 25,
  },
  successContainer: {
    backgroundColor: appTheme['color-warning-500'],
    flex: 1,
  },

  successText: {
    paddingTop: 10,
    fontFamily: Platform.OS === 'ios' ? 'SF Mono' : 'monospace',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  successIcon: {
    fontSize: 80,
    textAlign: 'center',
  },
  successWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeBtnIcon: {
    fontSize: 40,
  },
  closeBtnWrapper: {
    alignItems: 'flex-end',
    paddingHorizontal: 16,
  },
  addressTextBottom: {
    paddingTop: 5,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  addressTextTop: {
    paddingTop: '10%',
    fontSize: 34,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  addressWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  timeTextBottom: {
    paddingTop: 5,
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'normal',
  },
  timeTextTop: {
    paddingTop: '5%',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: '500',
  },
  timeWrapper: {width: '100%', alignItems: 'center', justifyContent: 'center'},
});

export default CreateOrderView;
