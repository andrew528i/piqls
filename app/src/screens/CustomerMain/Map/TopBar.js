import {Image, TouchableOpacity, View, Keyboard} from 'react-native';
import iconMenu from '../../../asset/icon/menu.png';
import iconLogo from '../../../asset/icon/logo.png';
import {default as appTheme} from '../../../themes/light';
import iconChat from '../../../asset/icon/chat.png';
import React from 'react';

class TopBar extends React.Component {
    render() {
        return <View style={{
            paddingHorizontal: 10,
            flexDirection: 'row',
            flex: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexGrow: 1,
            flexShrink: 1,
        }}>
            <TouchableOpacity
                style={{
                    width: 40,
                    height: 40,
                    margin: 2
                }}
                onPress={() => {
                    Keyboard.dismiss();
                    this.props.navigation.openDrawer();
                }}
            >
                <Image source={iconMenu} style={{
                    height: 24,
                    width: 24,
                    margin: 8
                }}/>
            </TouchableOpacity>
            <View>
                <Image source={iconLogo} style={{
                    height: 28,
                    resizeMode: "contain"
                }}/>
            </View>
            <TouchableOpacity style={{
                width: 40,
                height: 40,
                margin: 2,
                backgroundColor: '#FFF',
                borderRadius: 20,
                //android shadow
                elevation: 0,
                //ios shadow
                shadowColor: appTheme['color-primary-500'],
                shadowOpacity: 0.16,
                shadowRadius: 14,
                shadowOffset: {width:0,height:2}
            }}>
                <Image source={iconChat} style={{
                    height: 24,
                    width: 24,
                    margin: 8
                }}/>
            </TouchableOpacity>
        </View>
    }
}

export default TopBar
