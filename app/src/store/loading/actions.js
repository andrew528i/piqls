export const SET_LOADING = 'SET_LOADING';

export const setLoadingState = state => ({
    type: SET_LOADING,
    payload: state,
})
