import {SET_LOADING} from './actions';
import {combineReducers} from 'redux';

const setLoadingState = (state = false, action) => {
    switch (action.type) {
        case SET_LOADING:
            return action.payload

        default:
            return state
    }
}

export const loadingReducers = combineReducers({
    state: setLoadingState,
})
