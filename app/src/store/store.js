import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist';

import {combineReducers, createStore, applyMiddleware} from 'redux';
import {userReducers} from './user/reducers';
import {createLogger} from 'redux-logger';
import {loadingReducers} from './loading/reducers';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['user'],
    blacklist: ['loading'],
};

let rootReducer = combineReducers({
    user: userReducers,
    loading: loadingReducers,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
    persistedReducer,
    // applyMiddleware(createLogger()),
);

let persistor = persistStore(store);

export {store, persistor};
