export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';
export const UPDATE_USER_AUTH_INFO = 'UPDATE_USER_AUTH_INFO';

export const info = info => ({
    type: UPDATE_USER_INFO,
    payload: info,
})

export const authInfo = info => ({
    type: UPDATE_USER_AUTH_INFO,
    payload: info,
})
