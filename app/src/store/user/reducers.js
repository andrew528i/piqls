import {UPDATE_USER_AUTH_INFO, UPDATE_USER_INFO} from './actions';
import {combineReducers} from 'redux';

const updateUser = (state = {}, action) => {
    let payload = action.payload;

    switch (action.type) {
        case UPDATE_USER_INFO:
            if (payload.csrf) {
                // return payload
            }

            return {...state, ...payload}

        default:
            return state
    }
}

const updateUserAuth = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_USER_AUTH_INFO:
            return {...state, ...action.payload}

        default:
            return state
    }
}

export const userReducers = combineReducers({
    info: updateUser,
    authInfo: updateUserAuth,
})
