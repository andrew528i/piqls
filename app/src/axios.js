import axios from 'axios'
import {AlertHelper} from './screens/AlertHelper';
import APIClient from './core/api/client';
import connectWebsocket from './core/api/socket';
import AsyncStorage from '@react-native-community/async-storage'
import {store} from './store/store'

import config from './core/config'
import {authInfo} from './store/user/actions';


const a = axios.create({
    baseURL: 'http://' + config.api_url + '/',
    withCredentials: true,
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',
    headers: {
        common: {
            accept: 'application/json'
        }
    }
});

a.interceptors.response.use(
    resp => {
        if (resp === undefined) {
            AlertHelper.show('warn', 'Can\'t connect to server')
            throw "cannot connect"
        }

        let data = resp.data;

        if (data.csrf) {
            a.defaults.headers['X-CSRFToken'] = data.csrf
            store.dispatch(authInfo({csrf: data.csrf}))
        }

        if (data.session_id) {
            store.dispatch(authInfo({sessionId: data.session_id}))

            if (APIClient.socket === null) {
                APIClient.socket = connectWebsocket(data.session_id)
            }
        }

        return resp
    },
    err => {
        if (err.response && err.response.data) {
            let data = err.response.data;

            if (data.detail) {
                AlertHelper.show('warn', 'Something goes wrong:', data.detail)
            }
        } else {
            if (err.message) {
                AlertHelper.show('warn', 'Something goes wrong:', err.message)
            } else {
                AlertHelper.show('warn', 'Something goes wrong:', err)
            }
        }

        // console.warn(err.request.data);

        throw err
    }
);

export default a
