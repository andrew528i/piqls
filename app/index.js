/**
 * @format
 */

import {AppRegistry, Platform} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import Snoopy from 'rn-snoopy'
import Geocoder from 'react-native-geocoding';
import messaging from '@react-native-firebase/messaging';

import bars from 'rn-snoopy/stream/bars'
import filter from 'rn-snoopy/stream/filter'
import buffer from 'rn-snoopy/stream/buffer'

import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
import stripe from 'tipsi-stripe';
import config from './src/core/config';

stripe.setOptions({
    publishableKey: config.stripe.public_key,
    merchantId: Platform.OS === 'android' ? config.stripe.android_merchant_id : config.stripe.ios_merchant_id,
    androidPayMode: 'test',
});

Geocoder.init(config.google_api_key, {language : 'en'});

if (__DEV__) {
    const emitter = new EventEmitter();
    const events = Snoopy.stream(emitter);

    bars()(
        buffer()(
            // events,
            filter({ method:'createView' }, false)(events),
            filter((info) => info.method.indexOf('update') > -1, false)(events),
            // filter({}, true)(events),
        )
    ).subscribe()

    // filter({ method:'createView' }, true)(events).subscribe()
    // filter({ method:'updateView' }, true)(events).subscribe()
}

// messaging().setBackgroundMessageHandler(async remoteMessage => {
//     console.log('Message handled in the background!', remoteMessage);
// });

AppRegistry.registerComponent(appName, () => App);

// AppRegistry.registerHeadlessTask('ReactNativeFirebaseMessagingHeadlessTask', () => message => {
//     console.warn("!!!!", message)
// });


