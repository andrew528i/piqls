from rest_framework import serializers


class MediaUploadViewPostSerializer(serializers.Serializer):

    file = serializers.FileField()


class AlbumUploadViewPostSerializer(serializers.Serializer):

    file = serializers.FileField()
