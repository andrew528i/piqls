from rest_framework import serializers

from media.models.album import Photo, Album
from media.models.media import Media


class MediaSerializer(serializers.ModelSerializer):

    class Meta:

        model = Media
        fields = 'guid', 'file'


class PhotoSerializer(serializers.ModelSerializer):

    class Meta:

        model = Photo
        fields = 'guid', 'media'

    media = MediaSerializer()


class AlbumSerializer(serializers.Serializer):

    class Meta:

        model = Album
        fields = 'guid', 'photos', 'created_at', 'updated_at'

    photos = PhotoSerializer(many=True)
