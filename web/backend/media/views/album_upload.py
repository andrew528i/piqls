import io
import zipfile

from django.core.files.base import ContentFile

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from media.models.album import Album, Photo
from media.models.media import Media
from media.serializers.models import MediaSerializer, AlbumSerializer
from order.models import Order
from django.core.files import File


class AlbumUploadView(BaseAPIView):

    def post(self, request):
        archive = request.validated_data['file']
        guid = request.query_params.get('guid')

        # buf = io.BytesIO(.read())
        zf = zipfile.ZipFile(archive.file)

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            # TODO: use common exception from DRF
            raise BaseAPIException('Not found', 'not_found')

        album = Album.get_or_create(order)

        for filename in zf.namelist():
            buf = ContentFile(zf.read(filename), filename)
            media = Media.objects.create(file=buf, type=Media.Type.PHOTO)
            Photo.objects.create(media=media, album=album)

        serializer = AlbumSerializer(album)

        notification_title = 'Your photos were uploaded.'
        notification_body = 'Now you have access to watch them.'
        order.customer.push_notification(notification_title, notification_body)

        return serializer.data
