from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from media.models.media import Media
from media.serializers.models import MediaSerializer

AVAILABLE_MEDIA_TYPES = [c[0] for c in Media.Type.choices]


class MediaUploadView(BaseAPIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        file = request.validated_data['file']
        media_type = request.query_params.get('type')
        phone = request.query_params.get('phone')
        code = request.query_params.get('code')

        if media_type not in AVAILABLE_MEDIA_TYPES:
            raise BaseAPIException('Unknown media type', 'unknown_media_type')

        media = Media.objects.create(file=file, type=media_type)
        serializer = MediaSerializer(media)

        return serializer.data
