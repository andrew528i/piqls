from django.urls import path

from media.views.album_upload import AlbumUploadView
from media.views.upload import MediaUploadView

urlpatterns = [
    path('upload', MediaUploadView.as_view()),
    path('album/upload', AlbumUploadView.as_view()),
]
