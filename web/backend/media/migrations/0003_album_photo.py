# Generated by Django 3.0.4 on 2020-04-16 14:29

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0026_transaction_type'),
        ('media', '0002_remove_media_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('order', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, related_name='album', to='order.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='photos', to='media.Album')),
                ('media', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='media.Media')),
            ],
        ),
    ]
