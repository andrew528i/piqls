import hashlib
import os
import random
import uuid

from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _


def upload_filename(instance, filename):
    _, ext = os.path.splitext(filename)
    h = hashlib.sha256()
    h.update(instance.file.read())
    h.update(bytes(str(random.random()), 'utf-8'))
    filename = h.hexdigest()
    filename = "%s%s" % (filename, ext)

    return os.path.join('media', filename)


class Media(models.Model):

    class Type(models.TextChoices):

        USER_PHOTO = 'USER_PHOTO', _('User photo')
        PHOTO = 'PHOTO', _('Photo')
        ALBUM_ARCHIVE = 'ALBUM_ARCHIVE', _('Album archive')

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    file = models.FileField(upload_to=upload_filename)

    type = models.CharField(max_length=32, choices=Type.choices)
    # user = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
