import uuid

from django.contrib.gis.db import models


class Album(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    order = models.OneToOneField('order.Order', on_delete=models.DO_NOTHING, related_name='album')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def get_or_create(cls, order):
        try:
            album = Album.objects.get(order=order)
        except Album.DoesNotExist:
            album = Album.objects.create(order=order)

        return album


class Photo(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    album = models.ForeignKey('media.Album', on_delete=models.DO_NOTHING, related_name='photos')
    media = models.ForeignKey('media.Media', on_delete=models.DO_NOTHING)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
