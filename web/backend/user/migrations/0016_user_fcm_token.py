# Generated by Django 3.0.5 on 2020-04-25 18:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0015_user_avatar'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='fcm_token',
            field=models.CharField(max_length=512, null=True),
        ),
    ]
