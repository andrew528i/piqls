CAN_CREATE_ORDER = 'can_create_order'
CAN_CREATE_ORDER_DESC = 'Can create order'

CAN_VIEW_ALL_ORDERS = 'can_view_all_orders'
CAN_VIEW_ALL_ORDERS_DESC = 'Can view all orders'

CAN_VIEW_USERS = 'can_view_users'
CAN_VIEW_USERS_DESC = 'Can view all users'

CAN_CREATE_USER = 'can_create_user'
CAN_CREATE_USER_DESC = 'Can create user'

PERMISSIONS = [
    (CAN_CREATE_ORDER, CAN_CREATE_ORDER_DESC),
    (CAN_VIEW_ALL_ORDERS, CAN_VIEW_ALL_ORDERS_DESC),
    (CAN_VIEW_USERS, CAN_VIEW_USERS_DESC),
    (CAN_CREATE_USER, CAN_CREATE_USER_DESC),
]
