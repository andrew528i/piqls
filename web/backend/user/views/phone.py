import random
import string
from datetime import datetime, timedelta

import pytz
from rest_framework.exceptions import APIException
from rest_framework.response import Response

from common.exceptions import BaseAPIException
from common.sms import send_sms_code
from common.views import BaseAPIView
from user.models import PhoneValidation, Phone, User
from user.serializers.views import SMS_CODE_SIGN_UP_ACTION, SMS_CODE_SIGN_IN_ACTION


class TooFastAPIException(BaseAPIException):

    default_detail = 'Too fast'
    default_code = 'too_fast'


class UserPhoneRequestCodeView(BaseAPIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        phone = request.validated_data['phone']
        action = request.validated_data['action']
        user_query = User.objects.filter(phone__number=phone)

        if user_query.count() > 0 and action == SMS_CODE_SIGN_UP_ACTION:
            raise BaseAPIException('User exists', 'user_exists')

        if user_query.count() == 0 and action == SMS_CODE_SIGN_IN_ACTION:
            raise BaseAPIException('User not found', 'user_not_found')

        v = PhoneValidation.objects.filter(phone__number=phone, activated=False)

        if v.count() > 0:
            v = v.first()
            sms_code = v.code
            utc = pytz.UTC
            timeout = utc.localize(datetime.now() + timedelta(minutes=-1))

            if v.updated_at >= timeout:
                raise TooFastAPIException
        else:
            sms_code = self.generate_code()
            phone, _ = Phone.objects.get_or_create(number=phone)
            v = PhoneValidation.objects.create(phone=phone, code=sms_code)

        send_sms_code(phone, sms_code)
        v.sent_count += 1
        v.save()

    @classmethod
    def generate_code(cls):
        return '555555'  # ''.join([random.choice(string.digits) for _ in range(6)])


class UserPhoneCheckCodeView(BaseAPIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        phone = request.validated_data['phone']
        code = request.validated_data['code']

        if not PhoneValidation.check_code(phone, code):
            raise BaseAPIException('Wrong code', 'wrong_code')
