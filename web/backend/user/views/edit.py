from common.views import BaseAPIView
from media.models.media import Media
from user.models import User
from user.serializers.models import UserSerializer


class UserEditView(BaseAPIView):

    def post(self, request):
        data = request.validated_data
        avatar = data.pop('avatar', None)

        if avatar:
            data['avatar'] = Media.objects.get(guid=avatar)

        User.objects.filter(pk=request.user.pk).update(**data)
        user = User.objects.get(pk=request.user.pk)

        return UserSerializer(user).data
