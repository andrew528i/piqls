from django.contrib.auth import login as django_login
from django.contrib.auth.models import Group
from django.db import IntegrityError
from django.utils.crypto import get_random_string

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from media.models.media import Media
from user.groups import CUSTOMER_GROUP, EMPLOYEE_GROUP
from user.models import PhoneValidation, User, Phone
from user.serializers.models import UserSerializer


class UserPhoneSignUpView(BaseAPIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        username = request.validated_data['username']
        password = request.validated_data.get('password')
        email = request.validated_data.get('email') or 'no@email.com'
        first_name = request.validated_data.get('first_name') or ''
        last_name = request.validated_data.get('last_name') or ''
        phone = request.validated_data['phone']
        code = request.validated_data['code']
        group = request.validated_data['group']
        avatar = request.validated_data.get('avatar')
        ip_address = self.get_ip_address(request)

        if password is None:
            password = get_random_string(length=8)

        code_is_valid = PhoneValidation.check_code(phone, code)
        phone, _ = Phone.objects.get_or_create(number=phone)

        if not code_is_valid:
            raise BaseAPIException('Wrong sms code', 'wrong_sms_code')

        if group not in [CUSTOMER_GROUP, EMPLOYEE_GROUP]:
            raise BaseAPIException('Wrong group', 'wrong_group')

        group = Group.objects.get(name=group)

        try:
            new_user = User.objects.create(
                username=username, email=email,
                ip_address=ip_address, phone=phone,
                first_name=first_name, last_name=last_name)
            new_user.groups.add(group)
            new_user.set_password(password)

            if avatar:
                new_user.avatar = Media.objects.get(guid=avatar)

            new_user.save()
        except IntegrityError:
            raise BaseAPIException('Username taken', 'username_taken')

        PhoneValidation.activate(phone.number, code)

        django_login(request, new_user)

        return {
            'user': UserSerializer(new_user).data,
        }
