from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import ListAPIView
from django.db.models import Q

from user.models import User
from user.permissions import CAN_VIEW_USERS
from user.serializers.models import UserSerializer


class UserListView(ListAPIView):

    serializer_class = UserSerializer

    def get_queryset(self):
        user = self.request.user
        params = self.request.query_params
        filters = Q()
        group = params.get('group')
        username = params.get('username')
        phone = params.get('phone')

        if not user.has_perm(CAN_VIEW_USERS):
            raise PermissionDenied

        if group in ['customer', 'employee', 'admin']:
            filters &= Q(groups__name=group)

        if username:
            filters &= Q(username__contains=username)

        if phone:
            filters &= Q(phone__number__contains=phone)

        return User.objects.filter(filters).order_by('-id')
