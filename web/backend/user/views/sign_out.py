from common.views import BaseAPIView

from django.contrib.auth import logout


class UserSignOutView(BaseAPIView):

    def get(self, request):
        logout(request)

        return True
