from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.middleware.csrf import get_token
from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.response import Response

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from user.models import PhoneValidation, User
from user.serializers.models import UserSerializer


class InvalidCredentials(APIException):

    status_code = status.HTTP_403_FORBIDDEN
    default_detail = 'Invalid credentials'
    default_code = 'invalid_credentials'


class UserSignInView(BaseAPIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        username = request.validated_data['username']
        password = request.validated_data['password']
        session_id = request.validated_data.get('session_id')

        if session_id:
            response = Response({
                'csrf': get_token(request),
                'session_id': session_id,
            })
            response.set_cookie('session_id', session_id)

            return response

        ip_address = self.get_ip_address(request)

        user = authenticate(username=username, password=password)

        if user is None:
            raise InvalidCredentials

        user.ip_address = ip_address
        user.save()

        django_login(request, user)
        # channels_login(request, user)

        return {
            'user': UserSerializer(user).data,
            'csrf': get_token(request),
            'session_id': request.session.session_key,
        }


class UserPhoneSignInView(BaseAPIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        phone = request.validated_data['phone']
        code = request.validated_data['code']
        password = request.validated_data.get('password')
        ip_address = self.get_ip_address(request)

        if not PhoneValidation.check_code(phone, code):
            raise BaseAPIException('Wrong code', 'wrong_code')

        try:
            user = User.objects.get(phone__number=phone)
        except User.DoesNotExist:
            raise BaseAPIException('User not found', 'user_not_found')

        if password:
            user.set_password(password)

        user.ip_address = ip_address
        user.save()

        django_login(request, user)
        # channels_login(request, user)

        if not PhoneValidation.activate(phone, code):
            raise BaseAPIException('Code activation error', 'code_activation_error')

        if not request.session.session_key:
            request.session.save()

        return {
            'user': UserSerializer(user).data,
            'csrf': get_token(request),
            'session_id': request.session.session_key,
        }
