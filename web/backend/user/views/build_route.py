from datetime import datetime

import googlemaps
from django.conf import settings
from googlemaps.convert import decode_polyline

from common.exceptions import BaseAPIException
from common.views import BaseAPIView


class BuildRouteView(BaseAPIView):

    def post(self, request):
        from_location = request.validated_data['from_location']
        to_location = request.validated_data['to_location']
        gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
        now = datetime.now()
        result = gmaps.directions(
            (from_location['latitude'], from_location['longitude']),
            (to_location['latitude'], to_location['longitude']),
            mode="walking", departure_time=now)

        if not result or len(result[0]) == 0:
            raise BaseAPIException('Cannot build route', 'cannot_build_route')

        polyline = result[0]['overview_polyline']['points']
        decoded_polyline = decode_polyline(polyline)

        decoded_polyline = [{'latitude': r['lat'], 'longitude': r['lng']} for r in decoded_polyline]

        return {'polyline': decoded_polyline}
