from django.db import IntegrityError
from django.utils.crypto import get_random_string
from rest_framework.exceptions import PermissionDenied

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from user.groups import CUSTOMER_GROUP, EMPLOYEE_GROUP
from user.models import Phone, User
from user.permissions import CAN_CREATE_ORDER, CAN_CREATE_USER
from user.serializers.models import UserSerializer
from django.contrib.auth.models import Group


class UserCreateView(BaseAPIView):

    ALLOWED_GROUPS = CUSTOMER_GROUP, EMPLOYEE_GROUP

    def post(self, request):
        username = request.validated_data['username']
        password = request.validated_data.get('password')
        email = request.validated_data.get('email') or 'no@email.com'
        first_name = request.validated_data.get('first_name') or ''
        last_name = request.validated_data.get('last_name') or ''
        phone = request.validated_data['phone']
        group = request.validated_data['group']
        avatar = request.validated_data.get('avatar')
        ip_address = self.get_ip_address(request)

        if not self.request.user.has_perm(CAN_CREATE_USER):
            raise PermissionDenied

        if password is None:
            password = get_random_string(length=8)

        try:
            phone = Phone.objects.create(number=phone)
        except IntegrityError:
            raise BaseAPIException('Phone taken', 'phone_taken')

        if group not in self.ALLOWED_GROUPS:
            raise BaseAPIException('Wrong group', 'wrong_group')

        group = Group.objects.get(name=group)

        try:
            user = User.objects.create(
                username=username, email=email,
                ip_address=ip_address, phone=phone,
                first_name=first_name, last_name=last_name,
                avatar=avatar)
            user.groups.add(group)
            user.set_password(password)
            user.save()
        except IntegrityError:
            raise BaseAPIException('Username taken', 'username_taken')

        return {'user': UserSerializer(user).data}
