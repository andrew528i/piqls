from common.views import BaseAPIView
from user.serializers.models import UserSerializer
from django.middleware.csrf import get_token


class UserDetailView(BaseAPIView):

    def get(self, request):
        serializer = UserSerializer(request.user)
        data = serializer.data
        data['csrf'] = get_token(request)
        data['session_id'] = request.session.session_key

        return data
