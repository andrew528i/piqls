from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.db.models import Q

from common.fcm import get_push_service
from order.models import Order
from user.groups import CUSTOMER_GROUP, EMPLOYEE_GROUP, ADMIN_GROUP
from user.permissions import PERMISSIONS


class User(AbstractUser):

    class Meta:

        verbose_name = 'user'
        verbose_name_plural = 'users'
        permissions = PERMISSIONS

    REQUIRED_FIELDS = ['ip_address', 'email']

    phone = models.OneToOneField('user.Phone', on_delete=models.DO_NOTHING, null=True)
    avatar = models.OneToOneField('media.Media', on_delete=models.DO_NOTHING, null=True, related_name='user')

    ip_address = models.GenericIPAddressField()
    location = models.PointField(default=Point(0, 0))
    fcm_token = models.CharField(max_length=512, null=True)

    def has_perm(self, perm, obj=None):
        if perm.count('.') == 0:
            perm = 'user.{}'.format(perm)

        return super().has_perm(perm, obj)

    @property
    def channel_group(self):
        return 'user_{}'.format(self.id)

    @property
    def is_admin(self):
        for g in self.groups.all():
            if g.name == ADMIN_GROUP:
                return True

    @property
    def is_customer(self):
        for g in self.groups.all():
            if g.name == CUSTOMER_GROUP:
                return True

    @property
    def is_employee(self):  # TODO: use metaclasses
        for g in self.groups.all():
            if g.name == EMPLOYEE_GROUP:
                return True

    @property
    def group(self):
        for g in self.groups.all():
            return g.name

    @property
    def active_orders(self):
        return Order.objects.filter(
            ~Q(state__in=[Order.State.DONE, Order.State.CANCELLED, Order.State.NOT_PAID]) &
            Q(
                Q(customer=self) | Q(employee=self)))

    @property
    def has_active_orders(self):
        return self.active_orders.count() > 0

    @property
    def avatar_url(self):
        if self.avatar:
            return self.avatar.file

        return 'https://www.wmpg.org/wp-content/uploads/2019/03/default-avatar.jpg'

    def push_notification(self, title, body):
        if not self.fcm_token:
            return

        push_service = get_push_service()
        result = push_service.notify_single_device(registration_id=self.fcm_token, message_title=title, message_body=body)

        return result.get('success') == 1


class Phone(models.Model):

    number = models.CharField(max_length=32)  # TODO: add country and country_code

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class PhoneValidation(models.Model):

    phone = models.ForeignKey('user.Phone', on_delete=models.DO_NOTHING)
    code = models.CharField(max_length=8)
    activated = models.BooleanField(default=False)
    sent_count = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def check_code(cls, phone, code):
        phone, _ = Phone.objects.get_or_create(number=phone)

        try:
            pa = cls.objects.get(phone=phone, activated=False)
        except cls.DoesNotExist:
            return False

        return pa.code == code

    @classmethod
    def activate(cls, phone, code):
        try:
            pa = cls.objects.get(phone__number=phone, code=code, activated=False)
            pa.activated = True
            pa.save()

            return True
        except cls.DoesNotExist:
            return False
