from django.urls import path

from user.views.build_route import BuildRouteView
from user.views.create import UserCreateView
from user.views.detail import UserDetailView
from user.views.edit import UserEditView
from user.views.list import UserListView
from user.views.phone import UserPhoneRequestCodeView, UserPhoneCheckCodeView
from user.views.sign_in import UserSignInView, UserPhoneSignInView
from user.views.sign_out import UserSignOutView
from user.views.sign_up import UserPhoneSignUpView

urlpatterns = [
    path('sign_up/phone', UserPhoneSignUpView.as_view()),
    path('sign_in', UserSignInView.as_view()),
    path('sign_in/phone', UserPhoneSignInView.as_view()),
    path('sign_out', UserSignOutView.as_view()),
    path('detail', UserDetailView.as_view()),
    path('edit', UserEditView.as_view()),
    path('list', UserListView.as_view()),
    path('create', UserCreateView.as_view()),
    path('phone/request_code', UserPhoneRequestCodeView.as_view()),
    path('phone/check_code', UserPhoneCheckCodeView.as_view()),
    path('route/build', BuildRouteView.as_view()),
]
