from rest_framework import serializers

from common.serializers import LocationSerializer
from user.groups import CUSTOMER_GROUP, EMPLOYEE_GROUP

SMS_CODE_SIGN_UP_ACTION = 'sign_up'
SMS_CODE_SIGN_IN_ACTION = 'sign_in'


class UserPhoneSignUpViewPostSerializer(serializers.Serializer):

    username = serializers.CharField()
    password = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    phone = serializers.CharField()
    code = serializers.CharField()
    group = serializers.ChoiceField([CUSTOMER_GROUP, EMPLOYEE_GROUP])
    avatar = serializers.CharField(required=False)


class UserPhoneRequestCodeViewPostSerializer(serializers.Serializer):

    phone = serializers.CharField()
    action = serializers.ChoiceField([
        SMS_CODE_SIGN_UP_ACTION,
        SMS_CODE_SIGN_IN_ACTION,
    ], default=SMS_CODE_SIGN_UP_ACTION)


class UserPhoneCheckCodeViewPostSerializer(serializers.Serializer):

    phone = serializers.CharField()
    code = serializers.CharField()


class UserSignInViewPostSerializer(serializers.Serializer):

    username = serializers.CharField()
    password = serializers.CharField()
    session_id = serializers.CharField(required=False)


class UserPhoneSignInViewPostSerializer(serializers.Serializer):

    phone = serializers.CharField()
    code = serializers.CharField()
    password = serializers.CharField(required=False)


class UserDetailViewGetSerializer(serializers.Serializer):

    ...


class UserSignOutViewGetSerializer(serializers.Serializer):

    ...


class BuildRouteViewPostSerializer(serializers.Serializer):

    from_location = LocationSerializer()
    to_location = LocationSerializer()


class UserEditViewPostSerializer(serializers.Serializer):

    avatar = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    fcm_token = serializers.CharField(required=False)


class UserCreateViewPostSerializer(serializers.Serializer):

    username = serializers.CharField()
    password = serializers.CharField(required=False)
    email = serializers.CharField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    fcm_token = serializers.CharField(required=False)
    phone = serializers.CharField()
    group = serializers.ChoiceField([CUSTOMER_GROUP, EMPLOYEE_GROUP])
    avatar = serializers.ImageField(required=False)
