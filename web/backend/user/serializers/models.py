from rest_framework import serializers

from user.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:

        model = User
        fields = (
            'id', 'username', 'ip_address', 'email',
            'avatar', 'group', 'has_active_orders',
            'phone', 'location', 'first_name', 'last_name')

    phone = serializers.CharField(source='phone.number')
    avatar = serializers.FileField(source='avatar_url')
