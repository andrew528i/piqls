from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.interval import IntervalTrigger

TASKS = []


def from_crontab(expr, timezone=None):
    values = expr.split()
    if len(values) != 6:
        raise ValueError('Wrong number of fields; got {}, expected 6'.format(len(values)))

    return CronTrigger(
        second=values[0], minute=values[1], hour=values[2], day=values[3],
        month=values[4], day_of_week=values[5], timezone=timezone)


def cron(crontab):
    trigger = from_crontab(crontab)
    # trigger = IntervalTrigger.from_crontab(crontab)

    def decorator(func):
        module_path = func.fn.__module__
        func_name = func.fn.__name__
        TASKS.append((trigger, module_path, func_name))

        return func

    return decorator
