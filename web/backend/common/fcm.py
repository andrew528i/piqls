from django.conf import settings
from pyfcm import FCMNotification


def get_push_service():
    return FCMNotification(api_key=settings.FCM_API_KEY)
