import requests

from common.settings import NEXMO_API_KEY, NEXMO_SECRET


def send_sms_code(to, code):
    try:
        request_url = 'https://rest.nexmo.com/sc/us/2fa/json?api_key={}&api_secret={}&to={}&pin={}'.format(
            NEXMO_API_KEY, NEXMO_SECRET, to, code)
        print(requests.get(request_url).content)
    except:
        return False
