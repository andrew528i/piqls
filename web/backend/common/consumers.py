import uuid
from datetime import datetime

from asgiref.sync import async_to_sync
from channels.exceptions import StopConsumer
from channels.generic.websocket import JsonWebsocketConsumer
from django.contrib.gis.geos import Point
from django.contrib.sessions.models import Session
import redis_lock
from rest_framework.utils import json

from common.redis import connect_redis
from order.models import Order
from user.groups import EMPLOYEE_GROUP
from user.models import User
from user.serializers.models import UserSerializer


class GlobalConsumer(JsonWebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    def connect(self):
        session_id = self.scope['url_route']['kwargs'].get('session_id')

        if session_id is None:
            return

        try:
            session = Session.objects.get(session_key=session_id)
            decoded = session.get_decoded()
            user_id = decoded.get('_auth_user_id')
            self.user = User.objects.get(pk=user_id)

            async_to_sync(self.channel_layer.group_add)(
                self.user.channel_group,
                self.channel_name)

            if self.user.is_employee:
                async_to_sync(self.channel_layer.group_add)(EMPLOYEE_GROUP, self.channel_name)

            self.accept()
        except (Session.DoesNotExist, User.DoesNotExist) as e:
            print('Exception in websockt auth: {}'.format(str(e)))
            return None
            pass  # Send not authorized

    def disconnect(self, code):
        if self.user:
            async_to_sync(self.channel_layer.group_discard)(
                self.user.channel_group,
                self.channel_name)

            if self.user.is_employee:
                async_to_sync(self.channel_layer.group_discard)(
                    EMPLOYEE_GROUP,
                    self.channel_name)

        raise StopConsumer()

    def receive_json(self, content, **kwargs):
        packet_type = content.get('type')

        if packet_type == 'update_location':
            location = content.get('location')
            location = Point(location['latitude'], location['longitude'])

            User.objects.filter(pk=self.user.pk).update(location=location)
            self.send_update_location()

        if packet_type == 'ack':
            rc = connect_redis()
            lock_key = 'ws:lock:{}'.format(self.user.channel_group)
            lock = redis_lock.Lock(rc, lock_key, expire=10)

            lock.acquire()
            content_type = content.get('data_type', 'no_type')
            msg_key = 'ws:message:{}:{}'.format(self.user.channel_group, content_type)
            rc.delete(msg_key)
            lock.release()

    def order_state(self, event):
        self.send_json(event)

    def new_order(self, event):
        if self.user.is_employee:
            self.send_json(event)

    def cancel_order(self, event):
        if self.user.is_employee:
            self.send_json(event)

    def resend(self, event):
        data = event['content']
        print('resend for', self.user.username, data.get('guid'), data)

        self.send_json(data)

    def send_update_location(self):
        user_ids = []

        for order in self.user.active_orders:
            if order.state in [
                Order.State.CANCELLED,
                Order.State.NOT_PAID,
            ]:
                continue

            user_ids.append(order.customer_id)

            if order.employee_id:
                user_ids.append(order.employee_id)

        if not user_ids:
            return

        user_query = list(User.objects.filter(id__in=user_ids))
        users = UserSerializer(user_query, many=True).data

        for user in user_query:
            filtered_users = list(filter(lambda u: u['id'] != self.user.pk, users))
            event = {'type': 'update_location', 'users': filtered_users}
            async_to_sync(self.channel_layer.group_send)(user.channel_group, event)

    def update_location(self, event):
        self.send_json(event)

    def send_json(self, content, close=False):
        ts = int(datetime.now().timestamp())
        content['ts'] = ts

        if 'guid' not in content:
            content['guid'] = str(uuid.uuid4())
            rc = connect_redis()
            lock_key = 'ws:lock:{}'.format(self.user.channel_group)
            lock = redis_lock.Lock(rc, lock_key, expire=10)
            content_type = content.get('type', 'no_type')

            if content_type != 'update_location':
                lock.acquire()

                try:
                    msg_key = 'ws:message:{}:{}'.format(self.user.channel_group, content_type)
                    cached_content = rc.get(msg_key)
                    save = False

                    if cached_content:
                        cached_content = json.loads(cached_content.decode())
                        cached_ts = cached_content.get('ts', 0)

                        if ts >= cached_ts:
                            save = True

                    if save:
                        rc.set(msg_key, json.dumps(content).encode())
                        rc.expire(msg_key, 30)
                finally:
                    lock.release()

        super().send_json(content, close=False)


