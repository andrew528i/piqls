import json
import signal
from builtins import print

import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from common import cron

from apscheduler.schedulers.blocking import BlockingScheduler
from django.core.management.base import BaseCommand
from django_dramatiq.management.commands.rundramatiq import Command as RunDramatiqCommand

from common.redis import connect_redis


def websocket_sender():
    rc = connect_redis()
    keys = rc.keys('ws:message:*')
    channel_layer = get_channel_layer()

    for k in keys:
        data = rc.get(k)
        k = k.decode()

        if k.count(':') < 2:
            continue

        channel_name = k.split(':')[2]

        if not data:
            continue

        data = json.loads(data.decode())

        lock_key = 'ws:lock:{}'.format(channel_name)
        lock = redis_lock.Lock(rc, lock_key, expire=10)

        if lock.acquire(blocking=False):
            event = {'type': 'resend', 'content': data}
            print('Resend ', k)
            async_to_sync(channel_layer.group_send)(channel_name, event)
            lock.release()

        # group_name = k.replace('ws:message:', '')
        # lock = redis_lock.Lock(rc, k.replace(':messages:', ':lock:'), expire=10)
        #
        # if lock.acquire(blocking=False):
        #     d = rc.hgetall(k)
        #
        #     for k, v in d.items():
        #         event = {'type': 'resend', 'content': v.decode()}
        #         print('Resend ', k)
        #         async_to_sync(channel_layer.group_send)(group_name, event)
        #
        #     lock.release()


    # lock = redis_lock.Lock(rc, 'ws:lock:{}'.format(self.user.channel_group), expire=10)
    #
    # lock.acquire()
    # d = rc.hgetall('ws:{}'.format(self.user.channel_group))
    # d[guid] = json.dumps(content)
    # rc.hmset('ws:{}'.format(self.user.channel_group), d)
    # rc.expire('ws:{}'.format(self.user.channel_group), 900)
    # lock.release()
    #
    # rc.keys('ws_messages:')


class Command(BaseCommand):
    help = 'Starts dramatiq cron'

    def handle(self, *args, **options):
        scheduler = BlockingScheduler()

        tasks = RunDramatiqCommand().discover_tasks_modules()
        import importlib

        for t in tasks:
            importlib.import_module(t)

        for trigger, module_path, func_name in cron.TASKS:
            job_path = f"{module_path}:{func_name}.send"
            job_name = f"{module_path}.{func_name}"
            scheduler.add_job(job_path, trigger=trigger, name=job_name)

        def shutdown(signum, frame):
            scheduler.shutdown()

        # ws sender
        scheduler.add_job(websocket_sender, 'interval', seconds=1.5)

        signal.signal(signal.SIGINT, shutdown)
        signal.signal(signal.SIGTERM, shutdown)

        scheduler.start()

        return 0
