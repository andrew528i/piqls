import redis

from common.settings import REDIS_HOST, REDIS_PORT, REDIS_DATABASE, REDIS_PASSWORD

pool = None

if pool is None:
    pool = redis.ConnectionPool(
        max_connections=100,
        host=REDIS_HOST,
        port=REDIS_PORT,
        db=REDIS_DATABASE,
        password=REDIS_PASSWORD)


def connect_redis():
    global pool

    return redis.Redis(connection_pool=pool)
