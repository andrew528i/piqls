from django.urls import path

from order.views.area import OrderAreaListView, OrderAreaCreateView, OrderAreaEditView
from order.views.cancel import CancelOrderView
from order.views.change import ChangeOrderView
from order.views.checkout import OrderCheckoutView, OrderSuccessCheckoutView, OrderCancelCheckoutView
from order.views.create import CreateOrderView
from order.views.extend import ExtendOrderView, ConfirmExtendView
from order.views.list import OrderListView
from order.views.rate import RateOrderView
from order.views.state import OrderStateView
from order.views.tariff import OrderTariffListView, OrderTariffEditView, OrderTariffCreateView

urlpatterns = [
    path('area/list', OrderAreaListView.as_view()),
    path('area/create', OrderAreaCreateView.as_view()),
    path('area/edit', OrderAreaEditView.as_view()),

    path('list', OrderListView.as_view()),
    path('create', CreateOrderView.as_view()),
    path('change', ChangeOrderView.as_view()),
    path('cancel', CancelOrderView.as_view()),
    path('extend', ExtendOrderView.as_view()),
    path('extend/confirm', ConfirmExtendView.as_view()),
    path('rate', RateOrderView.as_view()),
    path('state', OrderStateView.as_view()),
    path('state/<str:guid>', OrderStateView.as_view()),

    path('tariff/list', OrderTariffListView.as_view()),
    path('tariff/create', OrderTariffCreateView.as_view()),
    path('tariff/edit', OrderTariffEditView.as_view()),

    path('checkout/<str:guid>', OrderCheckoutView.as_view()),
    path('checkout/success/<str:checkout_secret>', OrderSuccessCheckoutView.as_view()),
    path('checkout/cancel/<str:checkout_secret>', OrderCancelCheckoutView.as_view()),
]
