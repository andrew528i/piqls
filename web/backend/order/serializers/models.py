from rest_framework import serializers

from media.serializers.models import AlbumSerializer
from order.models import OrderArea, Order, Currency, Transaction
from order.models.tariff import Tariff
from user.serializers.models import UserSerializer


class OrderAreaSerializer(serializers.ModelSerializer):

    class Meta:

        model = OrderArea
        fields = 'guid', 'polygon', 'wait_time', 'created_at', 'updated_at'


class TransactionSerializer(serializers.ModelSerializer):

    class Meta:

        model = Transaction
        fields = 'guid', 'duration', 'amount', 'currency', 'state', 'payment_url', 'created_at', 'updated_at'


class OrderSerializer(serializers.ModelSerializer):

    class Meta:

        model = Order
        fields = (
            'guid', 'area_id',
            'customer', 'employee',
            'location', 'state', 'duration',
            'amount', 'currency', 'address',
            'meta', 'cancellation_fee',
            'transactions', 'album', 'rating',
            'created_at', 'updated_at', 'started_at')

    customer = UserSerializer()
    employee = UserSerializer()
    album = AlbumSerializer()
    transactions = TransactionSerializer(many=True, source='cached_transactions')


class CurrencySerializer(serializers.ModelSerializer):

    class Meta:

        model = Currency
        fields = 'code', 'rate'


class TariffSerializer(serializers.ModelSerializer):

    class Meta:

        model = Tariff
        fields = 'guid', 'duration', 'amount', 'currency', 'name'

    currency = CurrencySerializer()
