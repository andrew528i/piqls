from rest_framework import serializers

from common.serializers import LocationSerializer
from order.models import Order


class CreateOrderViewPostSerializer(serializers.Serializer):

    location = LocationSerializer()
    tariff_guid = serializers.UUIDField()
    token_id = serializers.RegexField('tok_([a-zA-Z0-9]+)', required=False)
    started_at = serializers.DateTimeField()
    customer_id = serializers.IntegerField(required=False)
    employee_id = serializers.IntegerField(required=False)


class ConfirmExtendViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    state = serializers.ChoiceField(['yes', 'no'], required=False)


class ChangeOrderViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    started_at = serializers.DateTimeField(required=False)
    location = LocationSerializer(required=False)


class RateOrderViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    rating = serializers.IntegerField(min_value=1, max_value=5)


class ExtendOrderViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    token_id = serializers.RegexField('tok_([a-zA-Z0-9]+)')
    duration = serializers.IntegerField(min_value=10)
    currency_code = serializers.CharField(default='USD')


class OrderStateViewGetSerializer(serializers.Serializer):

    ...


class OrderStateViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    state = serializers.ChoiceField([c[0] for c in Order.State.choices])


class CancelOrderViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()


class OrderCheckoutViewGetSerializer(serializers.Serializer):

    ...


class OrderSuccessCheckoutViewGetSerializer(serializers.Serializer):

    ...


class OrderCancelCheckoutViewGetSerializer(serializers.Serializer):

    ...


class OrderTariffCreateViewPostSerializer(serializers.Serializer):

    name = serializers.CharField(max_length=256)
    duration = serializers.IntegerField()
    amount = serializers.IntegerField()
    currency = serializers.CharField(max_length=8)


class OrderTariffEditViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    name = serializers.CharField(max_length=256, required=False)
    duration = serializers.IntegerField(required=False)
    amount = serializers.IntegerField(required=False)
    currency = serializers.CharField(max_length=8, required=False)


class OrderAreaCreateViewPostSerializer(serializers.Serializer):

    polygon = LocationSerializer(many=True)
    wait_time = serializers.IntegerField(default=60)


class OrderAreaEditViewPostSerializer(serializers.Serializer):

    guid = serializers.UUIDField()
    polygon = LocationSerializer(many=True, required=False)
    wait_time = serializers.IntegerField(required=False)
