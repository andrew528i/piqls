from django.contrib.gis.geos import Point
from django.db import IntegrityError
from rest_framework.exceptions import PermissionDenied, NotFound

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from order.models import Order, Tariff
from order.serializers.models import OrderSerializer
from user.groups import CUSTOMER_GROUP, EMPLOYEE_GROUP
from user.models import User
from user.permissions import CAN_CREATE_ORDER


class CreateOrderView(BaseAPIView):

    def post(self, request):
        user = request.user
        location = request.validated_data['location']
        location = Point(location['latitude'], location['longitude'])
        tariff_guid = request.validated_data['tariff_guid']
        token_id = request.validated_data.get('token_id')
        started_at = request.validated_data['started_at']
        customer_id = request.validated_data.get('customer_id')
        employee_id = request.validated_data.get('employee_id')
        base_url = request._current_scheme_host

        if not user.is_admin and not token_id:
            raise BaseAPIException('token_id required', 'token_id_required')

        if not user.has_perm(CAN_CREATE_ORDER):
            raise PermissionDenied

        if not Tariff.exists(tariff_guid):
            raise BaseAPIException('Unknown tariff', 'unknown_tariff')

        if not Order.check_location(location):
            raise BaseAPIException('Unknown order area', 'unknown_order_area')

        if not user.is_admin and request.user.has_active_orders:
            raise BaseAPIException('Not closed orders', 'not_closed_orders')

        order = Order.create(location, request.user, tariff_guid, token_id, started_at, base_url)

        if user.is_admin:
            try:
                if customer_id:
                    customer = User.objects.get(pk=customer_id, groups__name=CUSTOMER_GROUP)
                    order.customer = customer

                if employee_id:
                    employee = User.objects.get(pk=customer_id, groups__name=EMPLOYEE_GROUP)
                    order.employee = employee

                order.save()
            except IntegrityError:
                raise NotFound

        serializer = OrderSerializer(order)

        return serializer.data
