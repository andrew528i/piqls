from datetime import datetime, timedelta

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from pytz import utc
from rest_framework.exceptions import NotAuthenticated

from common.exceptions import BaseAPIException
from common.redis import connect_redis
from common.views import BaseAPIView
from order.models import Order
from order.serializers.models import OrderSerializer
from order.tasks import watch_order


class ConfirmExtendView(BaseAPIView):

    def post(self, request):
        guid = request.validated_data['guid']
        state = request.validated_data.get('state')

        try:
            order = Order.objects.get(guid=guid)
            order_users = [order.customer_id]

            if order.employee_id:
                order_users.append(order.employee_id)

            if request.user.id not in order_users:
                raise Order.DoesNotExist

        except Order.DoesNotExist:
            raise BaseAPIException('Order not found', 'order_not_found')

        rc = connect_redis()
        key = 'order:extend:confirm:{}'.format(guid)

        if order.customer_id == request.user.id:
            rc.set(key, b'requested')

            if order.employee_id:
                notification_title = 'User wants more'
                notification_body = '{} wants to extend photoshoot!'.format(request.user.username)
                order.employee.push_notification(notification_title, notification_body)

        if order.employee_id == request.user.id and state:
            rc.set(key, state)

            notification_title = 'Photoshoot extending'
            notification_body = '{} declined photoshoot extending :('.format(request.user.username)

            if state == 'yes':
                notification_body = '{} accepted photoshoot extending :)'.format(request.user.username)

            order.customer.push_notification(notification_title, notification_body)

        channel_layer = get_channel_layer()
        serializer = OrderSerializer(order)
        event = {'type': 'order_state', 'order': serializer.data}
        async_to_sync(channel_layer.group_send)('user_{}'.format(order.customer_id), event)

        if order.employee_id:
            async_to_sync(channel_layer.group_send)('user_{}'.format(order.employee_id), event)

        rc.expire(key, 600)

        return True


class ExtendOrderView(BaseAPIView):

    def post(self, request):
        guid = request.validated_data['guid']
        duration = request.validated_data['duration']
        token_id = request.validated_data['token_id']
        # TODO: process different currencies
        currency_code = 'USD'  # request.validated_data['currency_code']
        rc = connect_redis()
        key = 'order:extend:confirm:{}'.format(guid)

        if duration % 10 != 0:
            raise BaseAPIException('Wrong duration', 'wrong_duration')

        amount = (duration / 10) * 20  # NOTE: in USD

        try:
            order = Order.objects.get(guid=guid, customer=request.user)
            state = rc.get(key)

            if not state or state.decode() != 'yes':
                raise NotAuthenticated

            order.extend(duration, amount, currency_code, token_id)
            order.state = Order.State.WARM_UP
            order.warm_up_started_at = utc.localize(datetime.now() + timedelta(hours=-1))
            order.save()

            rc = connect_redis()
            rc.delete('order:stop:{}'.format(order.guid))
            watch_order.send(str(order.guid))
            rc.delete(key)
        except Order.DoesNotExist:
            raise BaseAPIException('Order not found', 'order_not_found')

        serializer = OrderSerializer(order)

        # channel_layer = get_channel_layer()
        # event = {'type': 'order_state', 'order': serializer.data}
        # async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)
        #
        # if order.employee:
        #     async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

        return serializer.data
