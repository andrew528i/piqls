import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from rest_framework.exceptions import NotFound, PermissionDenied

from common.redis import connect_redis
from common.views import BaseAPIView
from order.models import Order
from order.serializers.models import OrderSerializer


class RateOrderView(BaseAPIView):

    def post(self, request):
        guid = request.validated_data['guid']
        rating = request.validated_data['rating']
        user = request.user

        if not request.user.is_customer:
            raise PermissionDenied

        rc = connect_redis()
        lock_key = 'order:lock:{}'.format(guid)
        lock = redis_lock.Lock(rc, lock_key, expire=10)

        lock.acquire()

        try:
            order = Order.objects.get(guid=guid, customer_id=user.id, rating=0)
            order.rating = rating
            order.save()

            serializer = OrderSerializer(order)
            channel_layer = get_channel_layer()

            event = {'type': 'order_state', 'order': serializer.data}
            async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

            if order.employee:
                async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

            return serializer.data
        except:
            raise NotFound
        finally:
            lock.release()
