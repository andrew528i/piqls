from common.views import BaseAPIView
from order.serializers.models import OrderFullSerializer


class UserDetailView(BaseAPIView):

    def get(self, request):
        serializer = OrderFullSerializer(request.user)

        return serializer.data
