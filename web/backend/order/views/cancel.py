import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from rest_framework.exceptions import NotFound

from common.exceptions import BaseAPIException
from common.redis import connect_redis
from common.views import BaseAPIView
from order.models import Order
from order.serializers.models import OrderSerializer
from order.tasks import refund_transaction
from user.groups import EMPLOYEE_GROUP


class CancelOrderView(BaseAPIView):

    def post(self, request):
        guid = request.validated_data['guid']
        channel_layer = get_channel_layer()
        user = request.user

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            raise NotFound

        rc = connect_redis()
        lock_key = 'order:lock:{}'.format(order.guid)
        lock = redis_lock.Lock(rc, lock_key, expire=10)

        lock.acquire()
        rc.set('order:stop:{}'.format(order.guid), b'1')
        rc.expire('order:stop:{}'.format(order.guid), 900)

        try:
            order = Order.objects.get(guid=guid)

            if user.is_customer:
                if order.state not in [Order.State.PAID, Order.State.ASSIGNED, Order.State.READY, Order.State.WARM_UP]: # and order.cancellation_fee == .0:
                    raise BaseAPIException('Not PAID state', 'not_paid_state')

                if order.transactions.count() > 1:
                    raise BaseAPIException('More than one transaction', 'more_than_one_transaction')

                transaction = order.transactions.first()
                try:
                    refund_transaction(transaction.id)
                except:
                    print('TODO: process me')  # already refunded in all cases
                order.state = Order.State.CANCELLED
                order.save()

                event = {'type': 'cancel_order', 'order_guid': str(order.guid)}
                async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)
                async_to_sync(channel_layer.group_send)(EMPLOYEE_GROUP, event)

                serializer = OrderSerializer(order)
                event = {'type': 'order_state', 'order': serializer.data}
                async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

                if order.employee:
                    async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

            if user.is_employee:
                if order.state == Order.State.ASSIGNED and order.employee.username == user.username:
                    order.state = Order.State.PAID
                    order.employee = None
                    order.save()
                elif order.state == Order.State.READY and order.employee.username == user.username:
                    # decrease employee internal balance
                    order.state = Order.State.CANCELLED
                    order.employee = None
                    order.save()

                notification_title = 'Photographer canceled the order.'
                notification_body = 'Please see the details.'
                order.customer.push_notification(notification_title, notification_body)

                serializer = OrderSerializer(order)
                event = {'type': 'order_state', 'order': serializer.data}
                async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

                if order.employee:
                    async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

            serializer = OrderSerializer(order)

            return serializer.data
        except:
            return {}
        finally:
            lock.release()
