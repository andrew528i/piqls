from django.core.exceptions import ValidationError
from django.db import transaction
from rest_framework.exceptions import NotFound
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from order.models import Order, Transaction
from order.tasks import update_order_state


class OrderCheckoutView(BaseAPIView):

    renderer_classes = TemplateHTMLRenderer,
    permission_classes = ()
    authentication_classes = ()
    template_name = 'order_checkout.html'

    def get(self, request, **kwargs):
        guid = kwargs.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except (Order.DoesNotExist, ValidationError):
            raise NotFound

        if order.state != Order.State.NOT_PAID:
            raise BaseAPIException('Cannot checkout order', 'cannot_checkout_order')

        checkout_id = ''

        for t in order.transactions.all():
            if t.checkout_id:
                checkout_id = t.checkout_id
                break

        if not checkout_id:
            raise BaseAPIException('Cannot checkout order', 'cannot_checkout_order')

        return Response({'checkout_id': checkout_id})


class OrderSuccessCheckoutView(BaseAPIView):

    permission_classes = ()
    authentication_classes = ()

    def get(self, request, **kwargs):
        checkout_secret = kwargs.get('checkout_secret')

        return Order.process_checkout(checkout_secret, Transaction.State.PAID)


class OrderCancelCheckoutView(BaseAPIView):

    permission_classes = ()
    authentication_classes = ()

    def get(self, request, **kwargs):
        checkout_secret = kwargs.get('checkout_secret')

        return Order.process_checkout(checkout_secret, Transaction.State.CANCELLED)
