from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView
from django.contrib.gis.geos import Polygon

from common.views import BaseAPIView
from order.models import OrderArea
from order.serializers.models import OrderAreaSerializer


class OrderAreaListView(ListAPIView):

    serializer_class = OrderAreaSerializer

    def get_queryset(self):
        return OrderArea.objects.all().order_by('-id')


class OrderAreaCreateView(BaseAPIView):

    def post(self, request):
        polygon = request.validated_data['polygon']
        wait_time = request.validated_data.get('wait_time', 60)

        polygon = Polygon([(c['latitude'], c['longitude']) for c in polygon])
        order_area = OrderArea.objects.create(polygon=polygon, wait_time=wait_time)

        return OrderAreaSerializer(order_area).data


class OrderAreaEditView(BaseAPIView):

    def post(self, request):
        guid = request.validated_data['guid']
        polygon = request.validated_data['polygon']
        wait_time = request.validated_data.get('wait_time')

        try:
            order_area = OrderArea.objects.get(guid=guid)
        except OrderArea.DoesNotExist:
            raise NotFound

        if polygon:
            polygon = Polygon([(c['latitude'], c['longitude']) for c in polygon])
            order_area.polygon = polygon

        if wait_time:
            order_area.wait_time = wait_time

        order_area.save()

        return OrderAreaSerializer(order_area).data
