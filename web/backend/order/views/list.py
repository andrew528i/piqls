from datetime import datetime, timedelta

import pytz
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import ListAPIView
from django.db.models import Sum

from common.exceptions import BaseAPIException
from order.models import Order
from order.serializers.models import OrderSerializer
from user.permissions import CAN_VIEW_ALL_ORDERS
from django.db.models import Q


class OrderListView(ListAPIView):

    serializer_class = OrderSerializer

    def get_queryset(self):
        user = self.request.user
        params = self.request.query_params
        active = 'active' in params
        mine = 'mine' in params

        min_price = params.get('min_price')
        max_price = params.get('max_price')
        customer_username = params.get('customer_username')
        employee_username = params.get('employee_username')
        state = params.get('state')
        address = params.get('address')

        filters = Q()

        if min_price:
            filters &= Q(order_amount__gte=float(min_price))

        if max_price:
            filters &= Q(order_amount__lte=float(max_price))

        if customer_username:
            filters &= Q(customer__username__contains=customer_username)

        if employee_username:
            filters &= Q(employee__username__contains=employee_username)

        if state:
            filters &= Q(state=state)

        if address:
            filters &= Q(address__contains=address)

        if user.is_customer:
            filters = Q(customer=user)

            if active:
                filters &= ~Q(state__in=[Order.State.DONE, Order.State.CANCELLED, Order.State.NOT_PAID])
        elif user.is_employee:
            filters = Q()
            date_from = pytz.UTC.localize(datetime.now())

            if active:
                date_to = date_from + timedelta(hours=4)

                filters &= (
                    Q(employee=user) &
                    (Q(
                        state__in=[Order.State.ASSIGNED, Order.State.COMPLETED],
                        started_at__gte=date_from,
                        started_at__lte=date_to) |
                    Q(
                        state__in=[Order.State.READY, Order.State.WARM_UP, Order.State.PROCESSING, Order.State.COMPLETED],
                    )) &
                    ~Q(state=Order.State.DONE, started_at__lte=date_to))
            elif mine:
                filters &= (
                    Q(employee=user) &
                    ~Q(state=Order.State.CANCELLED) &
                    Q(started_at__gte=date_from))
            else:
                filters &= (
                    (Q(state=Order.State.PAID) |
                    (Q(employee=self.request.user) & Q(state=Order.State.PAID))) &
                    Q(started_at__gte=date_from))
        elif not user.has_perm(CAN_VIEW_ALL_ORDERS):
            raise PermissionDenied

        queryset = Order.objects.annotate(
            order_amount=Sum('transactions__amount')
        ).filter(filters)

        # if user.has_perm(CAN_VIEW_ALL_ORDERS):
        #     queryset = Order.objects.all()
        # else:
        #     active = 'active' in self.request.query_params
        #
        #     if user.is_customer:
        #         if active:
        #             queryset = Order.objects.filter(
        #                 Q(customer=user) &
        #                 ~Q(state__in=[Order.State.DONE, Order.State.CANCELLED, Order.State.NOT_PAID]))
        #         else:
        #             queryset = Order.objects.filter(customer=user)
        #     elif user.is_employee:
        #         mine = 'mine' in self.request.query_params
        #
        #         if active:
        #             date_from = pytz.UTC.localize(datetime.now())
        #             date_to = date_from + timedelta(hours=10)
        #             queryset = Order.objects.filter(
        #                 employee=user,
        #                 state=Order.State.ASSIGNED,
        #                 started_at__gte=date_from,
        #                 started_at__lte=date_to)
        #         elif mine:
        #             queryset = Order.objects.filter(
        #                 Q(employee=user) &
        #                 ~Q(state__in=[
        #                     Order.State.CANCELLED,
        #                 ]))
        #         else:
        #             # && location within radius of photographer
        #             queryset = Order.objects.filter(
        #                 Q(state=Order.State.PAID) |
        #                 (Q(employee=self.request.user) & Q(state=Order.State.PAID)))
        #     else:
        #         queryset = Order.objects.filter(
        #             Q(customer=self.request.user) | Q(employee=self.request.user))

        return queryset.order_by('-id')
