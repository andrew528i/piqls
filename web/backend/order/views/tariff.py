from django.db import IntegrityError
from rest_framework.exceptions import NotFound
from rest_framework.generics import ListAPIView

from common.exceptions import BaseAPIException
from common.views import BaseAPIView
from order.models import Currency
from order.models.tariff import Tariff
from order.serializers.models import TariffSerializer


class OrderTariffListView(ListAPIView):

    serializer_class = TariffSerializer

    def get_queryset(self):
        return Tariff.objects.order_by('amount')


class OrderTariffCreateView(BaseAPIView):

    def post(self, request):
        name = request.validated_data['name']
        duration = request.validated_data['duration']
        amount = request.validated_data['amount']
        currency = request.validated_data['currency']

        try:
            currency = Currency.objects.get(code=currency)
            tariff = Tariff.objects.create(name=name, duration=duration, amount=amount, currency=currency)
        except (IntegrityError, Currency.DoesNotExist):
            raise BaseAPIException('Cannot create tariff', 'cannot_create_tariff')

        return TariffSerializer(tariff).data


class OrderTariffEditView(BaseAPIView):

    def post(self, request):
        data = request.validated_data
        guid = data.pop('guid')
        currency = data.pop('currency')

        try:
            tariff = Tariff.objects.get(guid=guid)
        except Tariff.DoesNotExist:
            raise NotFound

        if currency:
            try:
                currency = Currency.objects.get(code=currency)
                tariff.currency = currency
                tariff.save()
            except Currency.DoesNotExist:
                raise NotFound

        for k, v in data.items():
            setattr(tariff, k, v)

        if data:
            tariff.save()

        return TariffSerializer(tariff).data
