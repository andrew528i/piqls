from datetime import datetime

import pytz
import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from rest_framework.exceptions import NotFound, PermissionDenied

from common.exceptions import BaseAPIException
from common.fcm import get_push_service
from common.redis import connect_redis
from common.views import BaseAPIView
from order.models import Order
from order.serializers.models import OrderSerializer
from order.tasks.watch_order_task import watch_order


class OrderStateView(BaseAPIView):

    def get(self, request, **kwargs):
        guid = kwargs.get('guid')

        if not guid:
            raise NotFound

        try:
            order = Order.objects.get(guid=guid)
            order_users = [order.customer_id]

            if order.employee_id:
                order_users.append(order.employee_id)

            if order.state != Order.State.PAID:
                if request.user.id not in order_users and not request.user.is_admin:
                    raise NotFound

            serializer = OrderSerializer(order)

        except Order.DoesNotExist:
            raise NotFound

        return serializer.data

    def post(self, request):
        guid = request.validated_data['guid']
        state = request.validated_data['state']
        user = request.user
        utc = pytz.UTC

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            # TODO: use common exception from DRF
            raise BaseAPIException('Not found', 'not_found')

        rc = connect_redis()
        lock_key = 'order:lock:{}'.format(order.guid)
        lock = redis_lock.Lock(rc, lock_key, expire=10)

        lock.acquire()

        try:
            order = Order.objects.get(guid=guid)

            if request.user.is_employee and order.employee and order.employee.id != user.id:
                raise BaseAPIException('Not employee', 'not_employee')
            elif request.user.is_customer and order.customer.id != user.id:
                raise BaseAPIException('Not customer', 'not_customer')

            # TODO: add redis mutex lock
            if state == Order.State.ASSIGNED:
                if order.state != Order.State.PAID:
                    raise BaseAPIException('Order is taken', 'order_is_taken')

                if not request.user.is_employee:
                    raise PermissionDenied

                if request.user.has_active_orders:
                    raise BaseAPIException('Already has active order', 'has_active_orders')

                order.state = Order.State.ASSIGNED
                order.employee = request.user
                notification_title = 'Your photographer is {}!'.format(request.user.username)
                notification_body = 'She will come to {} at {}'.format(
                    order.address_short, order.started_at.strftime('%H:%M'))
                order.customer.push_notification(notification_title, notification_body)

            if state == Order.State.READY:
                if order.state != Order.State.ASSIGNED:
                    raise BaseAPIException('Cannot start order', 'cannot_start_order')

                order.state = Order.State.READY
                notification_title = 'Your photographer is here!'
                notification_body = 'Lift up your phone so she can find you'
                order.customer.push_notification(notification_title, notification_body)

            if state == Order.State.WARM_UP:
                if order.state != Order.State.READY:
                    raise BaseAPIException('Cannot start order', 'cannot_start_order')

                order.state = Order.State.WARM_UP
                order.warm_up_started_at = utc.localize(datetime.now())
                watch_order.send(str(order.guid))
                rc.expire('order:stop:{}'.format(order.guid), 15)

                notification_title = 'We\'ve started!'
                notification_body = 'You have free 3 min to warm up and prepare for photoshooting.'
                order.customer.push_notification(notification_title, notification_body)
                order.employee.push_notification(notification_title, notification_body)

            if state == Order.State.COMPLETED:
                if order.state not in [Order.State.PROCESSING, Order.State.WARM_UP]:
                    raise BaseAPIException('Cannot complete order', 'cannot_complete_order')

                rc.set('order:stop:{}'.format(order.guid), b'1')
                rc.expire('order:stop:{}'.format(order.guid), 900)
                order.state = Order.State.COMPLETED

                notification_title = 'Your photoshooting has finished.'
                notification_body = 'If you want to continue please choose the option'
                order.customer.push_notification(notification_title, notification_body)

            if state == Order.State.DONE:
                if order.state != Order.State.COMPLETED:
                    raise BaseAPIException('Cannot done order', 'cannot_done_order')

                rc.set('order:stop:{}'.format(order.guid), b'1')
                rc.expire('order:stop:{}'.format(order.guid), 900)
                order.state = Order.State.DONE

                notification_title = 'Your order has been completed. We will inform you when your photos will be uploaded.'
                notification_body = 'If you want to continue please choose the option'
                order.customer.push_notification(notification_title, notification_body)

            order.save()
            channel_layer = get_channel_layer()
            serializer = OrderSerializer(order)

            event = {'type': 'order_state', 'order': serializer.data}
            async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

            if order.employee:
                async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

            return OrderSerializer(order).data
        finally:
            lock.release()
