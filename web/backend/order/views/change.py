import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.gis.geos import Point
from rest_framework.exceptions import NotFound

from common.exceptions import BaseAPIException
from common.redis import connect_redis
from common.views import BaseAPIView
from order.models import Order
from order.serializers.models import OrderSerializer
from user.groups import EMPLOYEE_GROUP


class ChangeOrderView(BaseAPIView):

    def post(self, request):
        guid = request.validated_data['guid']
        started_at = request.validated_data.get('started_at')
        location = request.validated_data.get('location')
        user = request.user
        rc = connect_redis()
        lock_key = 'order:lock:{}'.format(guid)
        lock = redis_lock.Lock(rc, lock_key, expire=10)

        lock.acquire()

        try:
            order = Order.objects.get(guid=guid, customer_id=user.id)

            if order.state != Order.State.PAID:
                raise BaseAPIException('Wrong order state', 'wrong_order_state')

            if started_at:
                order.started_at = started_at

            if location:
                location = Point(location['latitude'], location['longitude'])

                if not Order.check_location(location):
                    raise BaseAPIException('Unknown order area', 'unknown_order_area')

                order.location = location
                order.refresh_address()

            channel_layer = get_channel_layer()
            serializer = OrderSerializer(order)

            event = {'type': 'order_state', 'order': serializer.data}
            async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

            async_to_sync(channel_layer.group_send)(EMPLOYEE_GROUP, event)

            order.save()
            serializer = OrderSerializer(order)

            return serializer.data
        except:
            raise NotFound
        finally:
            lock.release()
