# Generated by Django 3.0.3 on 2020-03-02 18:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0010_auto_20200229_1430'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='token_id',
            field=models.CharField(default='', max_length=512),
            preserve_default=False,
        ),
    ]
