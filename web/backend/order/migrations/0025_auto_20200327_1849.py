# Generated by Django 3.0.4 on 2020-03-27 18:49

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0024_orderarea_guid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderarea',
            name='guid',
            field=models.UUIDField(default=uuid.uuid4, editable=False, unique=True),
        ),
    ]
