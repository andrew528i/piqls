# Generated by Django 3.0.3 on 2020-02-29 12:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_tariff'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='tariff',
            unique_together={('duration', 'amount', 'currency')},
        ),
    ]
