# Generated by Django 3.0.3 on 2020-03-02 18:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0011_transaction_token_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='state',
            field=models.CharField(choices=[('NOT_PAID', 'Not paid'), ('PAID', 'Paid'), ('CANCELLED', 'Cancelled'), ('QUEUED', 'Queued'), ('ASSIGNED', 'Assigned'), ('PROCESSING', 'Processing'), ('DONE', 'Done')], default='NOT_PAID', max_length=32),
        ),
    ]
