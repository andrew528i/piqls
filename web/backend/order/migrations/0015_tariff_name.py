# Generated by Django 3.0.4 on 2020-03-11 09:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0014_order_address'),
    ]

    operations = [
        migrations.AddField(
            model_name='tariff',
            name='name',
            field=models.CharField(default='Express', max_length=32),
            preserve_default=False,
        ),
    ]
