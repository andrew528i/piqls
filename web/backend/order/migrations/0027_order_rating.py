# Generated by Django 3.0.5 on 2020-05-04 18:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0026_transaction_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='rating',
            field=models.IntegerField(default=0),
        ),
    ]
