import uuid

import stripe
from django.contrib.gis.db import models
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _
from django.apps import apps

from common.settings import STRIPE_SECRET_KEY


class Transaction(models.Model):

    class State(models.TextChoices):

        NOT_PAID = 'NOT_PAID', _('Not paid')
        CANCELLED = 'CANCELLED', _('Cancelled')
        PENDING = 'PENDING', _('Pending')
        PAID = 'PAID', _('Paid')
        REFUNDED = 'REFUNDED', _('Refunded')
        ERROR = 'ERROR', _('Error')

    class Type(models.TextChoices):

        STRIPE = 'STRIPE', _('Stripe')
        SYSTEM = 'SYSTEM', _('System')

    order = models.ForeignKey('order.Order', on_delete=models.DO_NOTHING, related_name='transactions')
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    state = models.CharField(max_length=32, choices=State.choices, default=State.NOT_PAID)
    type = models.CharField(max_length=32, choices=Type.choices, default=Type.STRIPE)
    duration = models.IntegerField(blank=False)  # in minutes

    amount = models.FloatField()
    currency = models.ForeignKey('order.Currency', on_delete=models.DO_NOTHING)
    token_id = models.CharField(max_length=512, null=True)
    checkout_id = models.CharField(max_length=512, null=True)
    checkout_secret = models.CharField(max_length=512, null=True)
    charge_id = models.CharField(max_length=512)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def payment_url(self):
        if self.checkout_id and self.state == self.State.NOT_PAID:
            return f'/order/checkout/{self.order.guid}'

    @classmethod
    def create_from_tariff(cls, order, guid, token_id):
        tariff_model = apps.get_model('order', 'Tariff')
        tariff = tariff_model.objects.get(guid=guid)

        return cls.objects.create(
            order=order, duration=tariff.duration, amount=tariff.amount, currency=tariff.currency, token_id=token_id)

    @classmethod
    def create(cls, order, guid, base_url):
        tariff_model = apps.get_model('order', 'Tariff')
        tariff = tariff_model.objects.get(guid=guid)

        line_items = [{
            'name': 'PIQLS Photoshoot',
            'description': f'{tariff.duration} mins',
            'amount': int(tariff.amount * 100),
            'currency': tariff.currency.code,
            'quantity': 1,
        }]

        checkout_secret = get_random_string(length=64)
        success_url = f'{base_url}/order/checkout/success/{checkout_secret}'
        cancel_url = f'{base_url}/order/checkout/cancel/{checkout_secret}'

        checkout = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=line_items,
            success_url=success_url,
            cancel_url=cancel_url,
            api_key=STRIPE_SECRET_KEY)

        return cls.objects.create(
            order=order, duration=tariff.duration, amount=tariff.amount,
            currency=tariff.currency, checkout_id=checkout.id, checkout_secret=checkout_secret)
