import uuid

from django.contrib.gis.db import models


class OrderArea(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    polygon = models.PolygonField()
    wait_time = models.IntegerField(default=60)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
