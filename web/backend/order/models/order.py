import uuid
from datetime import datetime
from typing import Optional

import googlemaps
import pytz
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.db import transaction as db_transaction, transaction
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import NotFound

from common.exceptions import BaseAPIException
from common.redis import connect_redis
from order.tasks import charge_transaction, update_order_state


class Order(models.Model):

    WARM_UP_TIMEOUT = 60 * 4

    class State(models.TextChoices):

        NOT_PAID = 'NOT_PAID', _('Not paid')
        PAID = 'PAID', _('Paid')
        CANCELLED = 'CANCELLED', _('Cancelled')
        QUEUED = 'QUEUED', _('Queued')
        ASSIGNED = 'ASSIGNED', _('Assigned')
        READY = 'READY', _('Ready')
        WARM_UP = 'WARM_UP', _('Warm up')
        PROCESSING = 'PROCESSING', _('Processing')
        COMPLETED = 'COMPLETED', _('Completed')
        DONE = 'DONE', _('Done')

    area = models.ForeignKey('order.OrderArea', on_delete=models.DO_NOTHING)
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    address = models.CharField(max_length=1024, null=True)
    rating = models.IntegerField(default=0)
    customer = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, related_name='customer_orders')
    employee = models.ForeignKey('user.User', on_delete=models.DO_NOTHING, related_name='employee_orders', null=True)

    location = models.PointField(default=Point(0, 0))
    state = models.CharField(max_length=32, choices=State.choices, default=State.NOT_PAID)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    started_at = models.DateTimeField(null=True)
    warm_up_started_at = models.DateTimeField(null=True)
    processing_started_at = models.DateTimeField(null=True)

    _transactions = None

    @property
    def cached_transactions(self):
        if self._transactions is None:
            self._transactions = list(self.transactions.all())

        return self._transactions

    @property
    def address_short(self):
        if self.address:
            return self.address.split(',')[0].strip()

        return ''

    @property
    def duration(self):
        duration = 0

        for t in self.cached_transactions:
            duration += t.duration

        return duration

    @property
    def amount(self):
        amount = .0

        for t in self.cached_transactions:
            amount += t.amount

        return amount

    @property
    def currency(self):
        currency = set([])

        for t in self.cached_transactions:
            currency.add(t.currency.code)

        if len(currency) > 1:
            raise BaseAPIException('Different currencies', 'different_currencies')

        currency = list(currency)

        if len(currency) == 0:
            return 'USD'

        return currency[0]

    @property
    def meta(self):
        now = pytz.UTC.localize(datetime.now()).timestamp()
        state_started_at = 0
        meta = {}
        limit = 1

        if self.state == Order.State.WARM_UP:
            state_started_at = self.warm_up_started_at.timestamp()
            limit = self.WARM_UP_TIMEOUT

        if self.state == Order.State.PROCESSING:
            state_started_at = self.processing_started_at.timestamp()
            limit = self.duration * 60

        limit = 120  # TOOD: remove me

        if state_started_at > 0:
            delta = max(0, int(now) - int(state_started_at))
            meta['percent'] = int((delta / limit) * 100)
            meta['remain'] = max(0, limit - delta)
            meta['delta'] = (1 / limit) * 100

        # extend confirmation
        if self.state == Order.State.COMPLETED:
            rc = connect_redis()
            key = 'order:extend:confirm:{}'.format(self.guid)
            state = rc.get(key)

            meta['extend_confirmed'] = 'unknown'

            if state:
                meta['extend_confirmed'] = state.decode()

        if self.state == Order.State.ASSIGNED:
            started_at = self.started_at.timestamp()
            now = pytz.UTC.localize(datetime.now()).timestamp()

            meta['late'] = False

            if now > started_at:
                meta['late'] = True
                meta['late_minutes'] = (now - started_at) // 60

        if self.state == Order.State.PAID:
            started_at = self.started_at.timestamp()
            now = pytz.UTC.localize(datetime.now()).timestamp()

            meta['not_found'] = False

            if now > started_at:
                meta['not_found'] = True

        return meta

    @property
    def cancellation_fee(self):
        if self.state in [
            self.State.ASSIGNED,
            self.State.PROCESSING,
            self.State.WARM_UP,
            self.State.READY,
        ]:
            return round(self.amount * 0.33, 2)

        # raise BaseAPIException('Wrong order state', 'wrong_order_state')

        return .0

    @classmethod
    def check_location(cls, location: Point) -> bool:
        order_area_model = apps.get_model('order', 'OrderArea')
        order_area_query = order_area_model.objects.filter(polygon__intersects=location)

        return order_area_query.count() != 0

    def refresh_address(self, location=None):
        location = location or self.location
        gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
        result = gmaps.reverse_geocode((location.x, location.y))
        self.address = result[0]['formatted_address']

        return self.address

    @classmethod
    def create(
        cls,
        location: Point,
        customer: AbstractUser,
        tariff_guid: str,
        token_id: str,
        started_at: datetime,
        base_url: Optional[str] = None,
    ):
        order_area_model = apps.get_model('order', 'OrderArea')
        transaction_model = apps.get_model('order', 'Transaction')

        # 1: check location
        order_area_query = order_area_model.objects.filter(polygon__intersects=location)

        if order_area_query.count() == 0:
            return

        order_area = order_area_query.first()

        # 2: create order
        gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)
        result = gmaps.reverse_geocode((location.x, location.y))
        address = result[0]['formatted_address']

        with db_transaction.atomic():
            order = cls.objects.create(
                area=order_area, customer=customer,
                location=location, address=address,
                started_at=started_at)

            # 3: create transaction from tariff
            if token_id:
                transaction = transaction_model.create_from_tariff(order, tariff_guid, token_id)
                charge_transaction(transaction.id)
            else:
                transaction_model.create(order, tariff_guid, base_url)

        return order

    @classmethod
    def process_checkout(cls, checkout_secret, transaction_state):
        try:
            order = Order.objects.get(transactions__checkout_secret=checkout_secret)
        except Order.DoesNotExist:
            raise NotFound

        for t in order.transactions.all():
            if t.checkout_secret == checkout_secret and t.state != transaction_state:
                with transaction.atomic():
                    t.state = transaction_state
                    t.save()

                    update_order_state(order.id)

                return True

            return False

    def extend(self, duration: int, amount: float, currency_code: str, token_id: str):
        transaction_model = apps.get_model('order', 'Transaction')
        currency_model = apps.get_model('order', 'Currency')

        currency = currency_model.from_code(currency_code)
        transaction = transaction_model.objects.create(
            duration=duration, amount=amount, currency=currency,
            token_id=token_id, order=self)
        charge_transaction(transaction.id)

# @receiver(post_save, sender=Order)
# def order_update_handler(sender, instance, **kwargs):
#     update_fields = kwargs.get('update_fields') or []
#
#     if 'state' in update_fields:
#         channel_layer = get_channel_layer()
#
#         async_to_sync(channel_layer.group_send)(instance.customer.channel_group, event)
#
#         if instance.employee is not None:
#             async_to_sync(channel_layer.group_send)(instance.employee.channel_group, event)
