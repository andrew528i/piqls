import uuid

from django.contrib.gis.db import models


class Tariff(models.Model):

    class Meta:

        unique_together = 'duration', 'amount', 'currency'

    name = models.CharField(max_length=32)
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    duration = models.IntegerField(blank=False)  # in minutes

    amount = models.FloatField(blank=False)
    currency = models.ForeignKey('order.Currency', on_delete=models.DO_NOTHING)

    @classmethod
    def exists(cls, guid):
        tariff_query = cls.objects.filter(guid=guid)

        return tariff_query.count() != 0
