from django.contrib.gis.db import models


class CurrencyRate(models.Model):

    currency = models.ForeignKey('order.Currency', on_delete=models.DO_NOTHING, related_name='rates')
    rate = models.FloatField(blank=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
