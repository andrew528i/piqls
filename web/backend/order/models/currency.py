from django.contrib.gis.db import models


class Currency(models.Model):

    code = models.CharField(max_length=8)

    @property
    def rate(self):
        if self.rates.count() == 0:
            return .0

        currency_rate = self.rates.order_by('-updated_at')[0]

        return currency_rate.rate

    @classmethod
    def from_code(cls, code):
        return cls.objects.get(code=code.upper())
