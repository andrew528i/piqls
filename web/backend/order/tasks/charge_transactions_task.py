from datetime import timedelta, datetime

import dramatiq
import pytz
from django.apps import apps
from dramatiq.rate_limits import ConcurrentRateLimiter
from dramatiq.results import Results

from common.cron import cron
from order.tasks import charge_transaction
from dramatiq.rate_limits.backends import RedisBackend



def get_redis_backend():
    for m in dramatiq.get_broker().middleware:
        if isinstance(m, Results):
            return m.backend


redis_backend = get_redis_backend()
distributed_mutex = ConcurrentRateLimiter(
    RedisBackend(client=redis_backend.client), "watch_transactions_mutex", limit=1, ttl=30000)


@cron('*/5 * * * * *')
@dramatiq.actor(max_retries=0, max_age=5000)
def charge_transactions():
    transaction_model = apps.get_model('order', 'Transaction')
    timeout = pytz.UTC.localize(datetime.now() + timedelta(seconds=-5))

    transaction_query = transaction_model.objects.filter(
        state__in=[transaction_model.State.NOT_PAID, transaction_model.State.PENDING],
        updated_at__lte=timeout,
    ).exclude(token_id__isnull=True)

    with distributed_mutex.acquire():
        print('charge_transactions: {}'.format(transaction_query.count()))

        for t in transaction_query:
            charge_transaction.send(t.id)
