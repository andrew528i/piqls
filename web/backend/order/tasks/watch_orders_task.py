from datetime import datetime, timedelta

import dramatiq
import pytz
from dramatiq.rate_limits import ConcurrentRateLimiter
from dramatiq.results import Results

from common.cron import cron
from django.apps import apps
from order.tasks import watch_order
from dramatiq.rate_limits.backends import RedisBackend


def get_redis_backend():
    for m in dramatiq.get_broker().middleware:
        if isinstance(m, Results):
            return m.backend


redis_backend = get_redis_backend()
distributed_mutex = ConcurrentRateLimiter(
    RedisBackend(client=redis_backend.client), "watch_orders_mutex", limit=1, ttl=90000)


@cron('*/5 * * * * *')
@dramatiq.actor(max_retries=0, max_age=5000)
def watch_orders():
    utc = pytz.UTC
    timeout = utc.localize(datetime.now() + timedelta(seconds=-5))
    order_model = apps.get_model('order', 'Order')

    with distributed_mutex.acquire():
        orders = list(order_model.objects.filter(
            state__in=[order_model.State.WARM_UP, order_model.State.PROCESSING],
            updated_at__lte=timeout))

    for o in orders:
        watch_order.send(str(o.guid))

    print(f'watch_orders: {len(orders)}')
