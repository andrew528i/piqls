from datetime import datetime, timedelta
import time

import dramatiq
import pytz
import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from django.apps import apps

from common.redis import connect_redis


@dramatiq.actor(max_retries=0, time_limit=60 * 60 * 2 * 1000, max_age=5000)
def watch_order(guid):
    from order.serializers.models import OrderSerializer

    channel_layer = get_channel_layer()
    order_model = apps.get_model('order', 'Order')
    utc = pytz.UTC

    order = order_model.objects.get(guid=guid)
    rc = connect_redis()
    lock_key = 'order:lock:{}'.format(order.guid)
    lock = redis_lock.Lock(rc, lock_key, expire=10)

    while 1:
        lock.acquire()

        try:
            if rc.exists('order:stop:{}'.format(order.guid)):
                break

            order = order_model.objects.get(guid=guid)
            order.updated_at = utc.localize(datetime.now())

            if order.state not in [order_model.State.WARM_UP, order_model.State.PROCESSING]:
                break

            if order.state == order_model.State.WARM_UP:
                if order.meta.get('remain') == 0:
                    order.state = order_model.State.PROCESSING
                    order.processing_started_at = utc.localize(datetime.now())  # + timedelta(minutes=order.duration)

                    notification_title = 'Your photoshoot has started!'
                    notification_body = 'Enjoy and smile a lot.'
                    order.customer.push_notification(notification_title, notification_body)

            if order.state == order_model.State.PROCESSING:
                if order.meta.get('remain') == 0:
                    order.state = order_model.State.COMPLETED
                    key = 'order:extend:confirm:{}'.format(order.guid)
                    rc.delete(key)

                    notification_title = 'Your photoshooting has finished.'
                    notification_body = 'If you want to continue please choose the option'
                    order.customer.push_notification(notification_title, notification_body)

            order.save()

            serializer = OrderSerializer(order)
            event = {'type': 'order_state', 'order': serializer.data}
            async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

            if order.employee:
                async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

            print('watch_order: {} {}'.format(order.customer.channel_group, serializer.data))
        except Exception as e:
            print('watch order exception occured: ', order.guid, str(e))
        finally:
            lock.release()

        time.sleep(5)
