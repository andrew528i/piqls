from datetime import datetime

import dramatiq
import pytz
import stripe
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.apps import apps

from common.settings import STRIPE_SECRET_KEY
from order.tasks import update_order_state
from user.groups import EMPLOYEE_GROUP


@dramatiq.actor(max_retries=0)
def charge_transaction(transaction_id: int):
    from order.serializers.models import OrderSerializer

    print('charge_transaction')

    transaction_model = apps.get_model('order', 'Transaction')
    transaction = transaction_model.objects.get(pk=transaction_id)
    transaction.updated_at = pytz.UTC.localize(datetime.now())
    transaction.state = transaction_model.State.PENDING
    transaction.save()

    token_id = transaction.token_id
    amount = int(transaction.amount * 100)
    description = 'Photoshoot for {} mins by {} {}'.format(
        transaction.duration, transaction.amount, transaction.currency.code)

    try:
        charge = stripe.Charge.create(
            amount=amount, currency=transaction.currency.code,
            description=description, source=token_id, api_key=STRIPE_SECRET_KEY)
        resp = stripe.Charge.retrieve(id=charge.stripe_id, api_key=STRIPE_SECRET_KEY)

        status = resp.get('status')
        transaction.charge_id = charge.stripe_id

        if status == 'succeeded':
            transaction.state = transaction_model.State.PAID

        transaction.save()
        update_order_state(transaction.order_id)

        serializer = OrderSerializer(transaction.order)

        if transaction.order.transactions.count() == 1:
            channel_layer = get_channel_layer()
            event = {'type': 'new_order', 'order': serializer.data}
            async_to_sync(channel_layer.group_send)(EMPLOYEE_GROUP, event)

        return str(resp)

    except Exception:
        transaction.state = transaction_model.State.ERROR
        transaction.save()

        raise
