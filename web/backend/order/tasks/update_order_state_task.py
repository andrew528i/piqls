from collections import defaultdict
import time
from datetime import datetime

import dramatiq
import pytz
import redis_lock
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.apps import apps

from common.redis import connect_redis


@dramatiq.actor(max_retries=15)
def update_order_state(order_id: int):
    from order.serializers.models import OrderSerializer

    order_model = apps.get_model('order', 'Order')
    transaction_model = apps.get_model('order', 'Transaction')
    order = order_model.objects.get(pk=order_id)
    rc = connect_redis()
    lock_key = 'order:lock:{}'.format(order.guid)
    lock = redis_lock.Lock(rc, lock_key, expire=10)
    states = defaultdict(int)

    # lock.acquire()
    # order = order_model.objects.get(pk=order_id)
    for t in order.transactions.all():
        states[t.state] += 1

    state_before = order.state

    if order.state in [
        order_model.State.PAID,
        order_model.State.ASSIGNED,
        order_model.State.WARM_UP,
        order_model.State.PROCESSING,
    ] and states[transaction_model.State.REFUNDED] > 0 and len(states) == 1:
        order.state = order_model.State.CANCELLED

    # TODO: refact order state logic
    if order.state != order_model.State.COMPLETED and states[transaction_model.State.PAID] > 0 and len(states) == 1:
        order.state = order_model.State.PAID

    if order.state == order_model.State.COMPLETED and states[transaction_model.State.PAID] == order.transactions.count():
        from order.tasks.watch_order_task import watch_order

        order.state = order_model.State.PROCESSING
        order.processing_started_at = pytz.UTC.localize(datetime.now())
        order.save()
        watch_order.send(str(order.guid))

    order.save()
    # state_after = order.state

    # if state_before != state_after:
    channel_layer = get_channel_layer()
    serializer = OrderSerializer(order)
    print('update_order_state: {}'.format(serializer.data))

    event = {'type': 'order_state', 'order': serializer.data}
    async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

    if order.employee:
        async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)
    # lock.release()
