from datetime import datetime

import dramatiq
import pytz
import stripe
from django.apps import apps

from common.settings import STRIPE_SECRET_KEY
from order.tasks import update_order_state


@dramatiq.actor(max_retries=5)
def refund_transaction(transaction_id: int):
    print('refund_transaction')

    transaction_model = apps.get_model('order', 'Transaction')
    transaction = transaction_model.objects.get(pk=transaction_id)
    transaction.updated_at = pytz.UTC.localize(datetime.now())
    transaction.save()

    try:
        cancellation_fee = transaction.order.cancellation_fee
        refund_kwargs = {
            'charge': transaction.charge_id,
            'api_key': STRIPE_SECRET_KEY,
        }

        if cancellation_fee > 0:
            refund_kwargs['amount'] = int(cancellation_fee * 100)

        refund = stripe.Refund.create(**refund_kwargs)
        resp = stripe.Refund.retrieve(id=refund.stripe_id, api_key=STRIPE_SECRET_KEY)
        status = resp.get('status')

        if status == 'succeeded':
            transaction.state = transaction_model.State.REFUNDED

        transaction.save()
        update_order_state(transaction.order_id)

        return str(resp)

    except Exception:
        transaction.state = transaction_model.State.ERROR
        transaction.save()

        raise
