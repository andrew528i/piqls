from datetime import timedelta, datetime

import dramatiq
from django.apps import apps
from pytz import utc

from common.cron import cron


@cron('*/30 * * * * *')
@dramatiq.actor(max_retries=0, max_age=5000)
def update_completed_orders():
    order_model = apps.get_model('order', 'Order')
    timeout = utc.localize(datetime.now() + timedelta(minutes=-5))

    order_model.objects.filter(
        state=order_model.State.COMPLETED,
        updated_at__lte=timeout,
    ).update(state=order_model.State.DONE)
