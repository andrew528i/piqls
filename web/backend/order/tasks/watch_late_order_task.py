from datetime import timedelta, datetime

import dramatiq
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.apps import apps
from pytz import utc

from common.cron import cron
from order.tasks import refund_transaction


@cron('*/5 * * * * *')
@dramatiq.actor(max_retries=0, max_age=5000)
def watch_late_order():
    from order.serializers.models import OrderSerializer

    channel_layer = get_channel_layer()
    order_model = apps.get_model('order', 'Order')
    now = utc.localize(datetime.now())

    late_orders = order_model.objects.filter(
        state__in=[order_model.State.PAID, order_model.State.ASSIGNED],
        started_at__lte=now)

    print('late_orders: {}'.format(late_orders.count()))

    for order in late_orders:
        serializer = OrderSerializer(order)

        event = {'type': 'order_state', 'order': serializer.data}
        async_to_sync(channel_layer.group_send)(order.customer.channel_group, event)

        if order.employee:
            async_to_sync(channel_layer.group_send)(order.employee.channel_group, event)

        if order.state == order_model.State.PAID:
            notification_title = 'We so sorry :('
            notification_body = 'We did\'t found a photographer so we cancel your payment'.format(
                order.address_short, order.started_at.strftime('%H:%M'))
            order.customer.push_notification(notification_title, notification_body)

            transaction = order.transactions.first()
            refund_transaction(transaction.id)
            order_model.objects.filter(
                guid=order.guid
            ).update(state=order_model.State.CANCELLED)
