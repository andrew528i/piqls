import React from "react";
import {Route, Redirect, Switch} from 'react-router-dom';
import Login from "../pages/login";

const LoginRouters = () => {

    return (
        <Switch>
            <Route path="/login">
                <Login/>
            </Route>
            <Redirect to={"/login"}/>
        </Switch>
    )
};

export default LoginRouters