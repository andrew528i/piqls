import React from "react";
import {Route, Switch, Redirect} from 'react-router-dom';
import Dashboard from "../pages/main";

const DashboardRouters = () => {

    return (
        <Switch>
            <Route path="/login">
                <Redirect to={"/"}/>
            </Route>
            <Route path={"/"}>
                <Dashboard/>
            </Route>
        </Switch>
    )
};

export default DashboardRouters