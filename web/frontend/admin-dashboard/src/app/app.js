import React from 'react';
import {connect} from "react-redux";
import './app.module.scss';
import LoginRouters from "../routers/login";
import DashboardRouters from "../routers/dashboard";

function App({statusCode}) {

    return (
        <>
            {
                statusCode === "403" || statusCode === null ?
                    <>
                        <LoginRouters/>
                    </> :
                    <>
                        <DashboardRouters/>
                    </>
            }
        </>
    )
}

const mapStateToProps = ({statusCode}) => ({statusCode});

export default connect(mapStateToProps)(App);
