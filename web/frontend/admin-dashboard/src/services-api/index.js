class API {
    constructor(url) {
        // this.url = `http://173.249.45.177:8000${url}`;
        // this.url = `http://192.168.0.21:8000${url}`;
        this.url = `http://10.128.32.16:8000${url}`;
        this.headers = {
            "Content-Type": "application/json",
            "X-CSRFToken": localStorage.getItem("X-CSRF") || ''
        };
        this.method = {
            POST: "POST",
            GET: "GET"
        };
    }

    async authorization(data) {
        let response;
        try {
            response = await fetch(this.url, {
                method: this.method.POST,
                headers: this.headers,
                credentials: 'include',
                body: JSON.stringify(data)
            });
            const obj = await response.json();
            localStorage.setItem("X-CSRF", obj.csrf);
            localStorage.setItem("STATUS-CODE", '' + response.status);
            return obj;
        } catch (e) {
            throw new Error(e);
        }
    }

    async get() {
        let response;
        try {
            response = await fetch(this.url, {
                method: this.method.GET,
                headers: this.headers,
                credentials: 'include'
            });
            const obj = await response.json();
            localStorage.setItem("X-CSRF", obj.csrf);
            localStorage.setItem("STATUS-CODE", '' + response.status);
            return obj
        } catch (e) {
            throw new Error(e);
        }
    }


}

export default API;
