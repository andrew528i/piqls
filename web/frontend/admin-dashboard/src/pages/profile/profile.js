import React from "react";
import {connect} from 'react-redux'
import {Box} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Avatar from "@material-ui/core/Avatar";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles(theme => ({
    largeImage: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    textProfile: {
        marginBottom: 10
    }
}));

const Profile = ({user: {user}}) => {

    const classes = useStyles();

    return user ? (
        <>
            <Box mb={3}>
                <Typography variant="h5" component="h2" gutterBottom>
                    Profile
                </Typography>
            </Box>
            <Box mb={3}>
                {
                    user.avatar ?
                        <Avatar className={classes.largeImage} alt={user.username} src={user.avatar}/> :
                        <Avatar className={classes.largeImage}>{user.username[0]}</Avatar>
                }
            </Box>
            <Typography className={classes.textProfile} variant="h5" component="div">
                ID: {user.id}
            </Typography>
            <Typography className={classes.textProfile} variant="h5" component="div">
                Username: {user.username}
            </Typography>
            <Typography className={classes.textProfile} variant="h5" component="div">
                Email: {user.email}
            </Typography>
            <Typography className={classes.textProfile} variant="h5" component="div">
                Phone: {user.phone}
            </Typography>
        </>
    ) : null

};

const mapStateToProps = ({user}) => ({user});

export default connect(mapStateToProps)(Profile)
