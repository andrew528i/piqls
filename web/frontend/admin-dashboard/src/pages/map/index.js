import React from "react";
import {compose, withProps} from "recompose";
import {connect} from "react-redux";
import {getPolygon} from "../../redux/actions/polygon";
import {getOrderList} from "../../redux/actions/orderList";
import {GoogleMap, Marker, Polygon, withGoogleMap, withScriptjs} from "react-google-maps";
import {useRequest} from "../../hooks/hooks";

const Map = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI",
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: `calc(100vh - 65px)`}}/>,
        mapElement: <div style={{height: `100%`}}/>,
    }),
    withScriptjs,
    withGoogleMap
)((props) =>
    <GoogleMap defaultZoom={12} defaultCenter={{lat: 37.743356438019994, lng: -122.45512403547762}}>
        {props.coords && props.coords.length && props.coords.map((element, i) => {
            return <Polygon
                path={element}
                key={i + "polygon"}
                options={{
                    fillColor: "#000",
                    fillOpacity: 0.4,
                    strokeColor: "#000",
                    strokeOpacity: 1,
                    strokeWeight: 1
                }}/>
        })}
        {
            props.orderPoint.length ?
                props.orderPoint.map((element, i) => <Marker key={i + 'point'} position={{lat: element.location.coordinates[0], lng: element.location.coordinates[1]}}/>) :
                null
        }


    </GoogleMap>
);

const coordinates = (polygon) => {
    function coordsObj(allCordinates, arr = []) {
        for (let value in allCordinates) {
            arr.push([]);
            allCordinates[value].forEach(element => {
                arr[value].push({lat: element[0], lng: element[1]});
            })
        }
        return arr
    }

    if (polygon.results) {
        const allPolygon = [...polygon.results];
        const allCordinates = allPolygon.length && allPolygon.map(element => (element.polygon.coordinates).flat());
        return coordsObj(allCordinates);
    }
};

const GoogleMaps = ({polygon, getPolygon, getOrderList, results}) => {
    useRequest(getPolygon, getOrderList);
    const coords = coordinates(polygon);
    return <Map polygon={polygon} coords={coords} orderPoint={results}/>
};

GoogleMaps.defaultProps = {
    results: []
};

const mapStateToProps = ({polygon, orderList: {results}}) => ({polygon, results});

const mapDispatchToProps = {getPolygon, getOrderList};

export default connect(mapStateToProps, mapDispatchToProps)(GoogleMaps);
