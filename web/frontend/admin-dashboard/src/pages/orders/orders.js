import React, {useEffect, useState} from "react";
import {connect} from 'react-redux'
import {getOrderList} from "../../redux/actions/orderList";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import FormGroup from '@material-ui/core/FormGroup';
import SimpleExpansionPanel from "../../components/expansionPanel/expansionPanel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import 'date-fns';
import {makeStyles} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Pagination from '@material-ui/lab/Pagination';
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import AlertDialog from "../../components/alert/alert";
import HorizontalLinearStepper from './createOrder'
import {useRequest} from "../../hooks/hooks";

const useStyles = makeStyles(theme => ({
    pagination: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '30px'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    createOrderContainer: {
        width: 550,
        minHeight: 300
    }
}));

const OrdersPage = ({getOrderList, orderList}) => {
    const classes = useStyles();
    const [ordersList, setOrdersList] = useState([]);
    const [stateChecked, setStateChecked] = React.useState({
        checked: false
    });

    useEffect(() => {
        setOrdersList(orderList.results);
    }, [orderList]);

    useRequest(getOrderList);

    const handleChange = event => {
        setStateChecked({...stateChecked, [event.target.name]: event.target.checked});
    };

    return (
        <>
            <Box mb={2}>
                <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                >
                    <Typography variant="h5" component="h2">
                        Orders
                    </Typography>
                    <AlertDialog textBtn={'Create order'}>
                        <Box className={classes.createOrderContainer}>
                            <HorizontalLinearStepper/>
                        </Box>
                    </AlertDialog>
                </Grid>
            </Box>

            <Box mb={1}>
                <FormGroup row>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={stateChecked.checked}
                                onChange={handleChange}
                                name="checked"
                                color="primary"
                            />
                        }
                        label="Primary"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={stateChecked.checked}
                                onChange={handleChange}
                                name="checked"
                                color="primary"
                            />
                        }
                        label="Primary"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={stateChecked.checked}
                                onChange={handleChange}
                                name="checked"
                                color="primary"
                            />
                        }
                        label="Primary"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={stateChecked.checked}
                                onChange={handleChange}
                                name="checked"
                                color="primary"
                            />
                        }
                        label="Primary"
                    />
                    <TextField
                        id="datetime-local"
                        label="Next appointment"
                        type="datetime-local"
                        defaultValue="2017-05-24T10:30"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField
                        id="datetime-local"
                        label="Next appointment"
                        type="datetime-local"
                        defaultValue="2017-05-24T10:30"
                        className={classes.textField}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <TextField
                        id="outlined-full-width"
                        label="Label"
                        style={{marginTop: 30, marginBottom: 0}}
                        placeholder="Placeholder"
                        helperText="Full width!"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                    />
                    <Button style={{marginLeft: 'auto', marginBottom: 10}} variant="contained" color="primary"
                            disableElevation>
                        Search
                    </Button>
                </FormGroup>
            </Box>
            <SimpleExpansionPanel list={ordersList}/>
            <Pagination className={classes.pagination} count={10} color="primary"/>
        </>
    )
};

const mapStateToProps = ({orderList}) => ({orderList});
const mapDispatchToProps = {getOrderList};

export default connect(mapStateToProps, mapDispatchToProps)(OrdersPage)


