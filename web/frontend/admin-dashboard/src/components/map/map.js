import React, {useEffect} from "react";
import {compose, withProps} from "recompose";
import {connect} from "react-redux";
import {getPolygon} from "../../redux/actions/polygon";
import {GoogleMap, Marker, Polygon, withGoogleMap, withScriptjs} from "react-google-maps";

const Map = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI",
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: `500px`, width: '550px'}}/>,
        mapElement: <div style={{height: `100%`}}/>,
    }),
    withScriptjs,
    withGoogleMap
)((props) => {
        console.log(props.mainLocation);
        return (
            <GoogleMap defaultZoom={12} defaultCenter={{lat: props.mainLocation[0], lng: props.mainLocation[1]}}>
                <Marker position={{lat: props.mainLocation[0], lng: props.mainLocation[1]}}/>
                {props.coords && props.coords.length && props.coords.map((element, i) => {
                    return <Polygon
                        path={element}
                        key={i + "polygon"}
                        options={{
                            fillColor: "#000",
                            fillOpacity: 0.4,
                            strokeColor: "#000",
                            strokeOpacity: 1,
                            strokeWeight: 1
                        }}/>
                })}

            </GoogleMap>
        )
    }
);

const coordinates = (polygon) => {
    function coordsObj(allCordinates, arr = []) {
        for (let value in allCordinates) {
            arr.push([]);
            allCordinates[value].forEach(element => {
                arr[value].push({lat: element[0], lng: element[1]});
            })
        }
        return arr
    }

    if (polygon.results) {
        const allPolygon = [...polygon.results];
        const allCordinates = allPolygon.length && allPolygon.map(element => (element.polygon.coordinates).flat());
        return coordsObj(allCordinates);
    }
};

const GoogleMaps = ({polygon, getPolygon, mainLocation}) => {
    console.log(mainLocation);
    useEffect(() => {
        getPolygon()
    }, [getPolygon]);

    const coords = coordinates(polygon);

    return <Map mainLocation={mainLocation} polygon={polygon} coords={coords}/>
};

const mapStateToProps = ({polygon}, ownProps) => ({polygon, ...ownProps});

const mapDispatchToProps = {getPolygon};

export default connect(mapStateToProps, mapDispatchToProps)(GoogleMaps);
