import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import SimpleCard from "../card/card";
import {Box} from "@material-ui/core";
import AlertDialog from "../alert/alert";
import Map from '../map/map'

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        minHeight: '30vh'
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    backDropStyle: {
        backgroundColor: 'white'
    },
    circularProgressStyle: {
        color: '#3f51b5'
    },
    cardWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        maxWidth: '600px'
    }
}));

export default function SimpleExpansionPanel({list}) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Backdrop className={classes.backDropStyle} open={!!(!list.length)}>
                <CircularProgress className={classes.circularProgressStyle}/>
            </Backdrop>
            {
                list && list.length ? list.map((element, i) => {
                        const createdDate = new Date(element.created_at);
                        const updatedDate = new Date(element.updated_at);
                        const startedDate = new Date(element.started_at);
                        const {customer, employee} = element;

                        const options = {
                            era: 'long',
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric',
                            weekday: 'long',
                            timezone: 'UTC',
                            hour: 'numeric',
                            minute: 'numeric',
                            second: 'numeric'
                        };
                        return (
                            <ExpansionPanel key={i}>
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon/>}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <div>
                                        <Typography className={classes.heading}>
                                            <span style={{fontWeight: 'bold'}}>Address: </span>
                                            {element.address}
                                        </Typography>
                                        <Typography className={classes.heading}>
                                            <span style={{fontWeight: 'bold'}}>Create date:</span>
                                            {` ${createdDate.toLocaleString("en-US", options)}`}
                                        </Typography>
                                        <Typography className={classes.heading}>
                                            <span style={{fontWeight: 'bold'}}>State:</span>
                                            {` ${element.state}`}
                                        </Typography>
                                    </div>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <div style={{width: '100%'}}>
                                        <Typography>
                                            <span style={{fontWeight: 'bold'}}>Amount:</span>
                                            {` ${element.amount} ${element.currency}.`}
                                        </Typography>
                                        <Typography>
                                            <span style={{fontWeight: 'bold'}}>Duration:</span>
                                            {` ${element.duration} min.`}
                                        </Typography>
                                        <Typography>
                                            <span style={{fontWeight: 'bold'}}>Update date:</span>
                                            {` ${updatedDate.toLocaleString("en-US", options)}`}
                                        </Typography>
                                        <Typography>
                                            <span style={{fontWeight: 'bold'}}>Started date:</span>
                                            {` ${startedDate.toLocaleString("en-US", options)}`}
                                        </Typography>
                                        <Box mt={2}>
                                            <AlertDialog textBtn={'OPEN MAP'}>
                                                <Box style={{width: '100%'}}>
                                                    <Map mainLocation={element.location.coordinates} isMarkerShown={true}/>
                                                </Box>
                                            </AlertDialog>
                                        </Box>
                                        <Box className={classes.cardWrapper} mt={2}>
                                            {customer ?
                                                <div>
                                                    <Typography mb={1} variant={'h6'}>
                                                        Customer
                                                    </Typography>
                                                    <SimpleCard {...customer}/>
                                                </div> :
                                                null
                                            }
                                            {employee ?
                                                <div>
                                                    <Typography mb={1} variant={'h6'}>
                                                        Employee
                                                    </Typography>
                                                    <SimpleCard {...employee}/>
                                                </div> :
                                                null
                                            }
                                        </Box>
                                    </div>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        )
                    }) :
                    null
            }
        </div>
    );
}

SimpleExpansionPanel.defaultProps = {
    list: []
};
