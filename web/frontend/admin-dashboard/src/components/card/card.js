import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import {Box} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
    flexBox: {
        display: 'flex',
        justifyContent: 'space-between',
        paddingBottom: 10
    }
}));

export default function SimpleCard({id, username, avatar, email, phone}) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Box className={classes.flexBox}>
                    {avatar ? <Avatar alt="avatar" src={avatar} className={classes.large} /> : null}
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        {`ID: ${id}`}
                    </Typography>
                </Box>
                <Typography variant="h5" component="h2">
                    {username}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    <span style={{fontWeight: 'bold'}}>E-MAIL: </span>{email}
                </Typography>
                <Typography variant="body2" component="p">
                    <span style={{fontWeight: 'bold'}}>PHONE: </span>{phone}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Learn More</Button>
            </CardActions>
        </Card>
    );
}
