import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import MapIcon from '@material-ui/icons/Map';
import BarChartIcon from '@material-ui/icons/BarChart';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import PinDropIcon from '@material-ui/icons/PinDrop';
import {Link} from "react-router-dom";

const LinkWrapper = {
        color: "rgba(0, 0, 0, 0.87)",
        textDecoration: "none"
    };


export const mainListItems = (
    <div>
        <Link to={"/"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <DashboardIcon />
                </ListItemIcon>
                <ListItemText primary="Dashboard" />
            </ListItem>
        </Link>
        <Link to={"/orders"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <ShoppingCartIcon />
                </ListItemIcon>
                <ListItemText primary="Orders" />
            </ListItem>
        </Link>
        <Link to={"/map"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <MapIcon />
                </ListItemIcon>
                <ListItemText primary="Map" />
            </ListItem>
        </Link>
        <Link to={"/photographers"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <PhotoCameraIcon />
                </ListItemIcon>
                <ListItemText primary="Photographers" />
            </ListItem>
        </Link>
        <Link to={"/clients"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <AssignmentIndIcon />
                </ListItemIcon>
                <ListItemText primary="Clients" />
            </ListItem>
        </Link>
        <Link to={"/plans"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <BarChartIcon />
                </ListItemIcon>
                <ListItemText primary="Plans" />
            </ListItem>
        </Link>
        <Link to={"/locations"} style={LinkWrapper}>
            <ListItem button>
                <ListItemIcon>
                    <PinDropIcon />
                </ListItemIcon>
                <ListItemText primary="Locations" />
            </ListItem>
        </Link>
        <ListItem button>
            <ListItemIcon>
                <LayersIcon />
            </ListItemIcon>
            <ListItemText primary="News" />
        </ListItem>
    </div>
);

export const secondaryListItems = (
    <div>
        <ListSubheader inset>Saved reports</ListSubheader>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary="Current month" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary="Last quarter" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary="Year-end sale" />
        </ListItem>
    </div>
);
