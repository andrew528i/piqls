import API from "../../services-api";
import {loading, status} from "./authorization";
import {POLYGON} from "../constants";

export const getPolygon = () => (dispatch) => {
    dispatch(loading());
    const request = async () => {
        const api = new API("/order/area/list");
        const response = await api.get();
        dispatch({type: POLYGON, payload: response});
        dispatch(status(localStorage.getItem("STATUS-CODE")));
        dispatch(loading());
    };
    request();
};
