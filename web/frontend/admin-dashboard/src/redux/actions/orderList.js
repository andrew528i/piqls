import API from "../../services-api";
import {loading, status} from "./authorization";
import {ORDER_LIST} from "../constants";

export const getOrderList = () => (dispatch) => {
    dispatch(loading());
    const request = async () => {
        const api = new API("/order/list");
        const response = await api.get();
        dispatch({type: ORDER_LIST, payload: response});
        dispatch(status(localStorage.getItem("STATUS-CODE")));
        dispatch(loading());
    };
    request();
};
