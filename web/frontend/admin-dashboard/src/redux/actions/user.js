import API from "../../services-api";
import {loading, status, loginForm} from "./authorization";

export const getUserDetails = () => (dispatch) => {
    dispatch(loading());
    const request = async () => {
        const api = new API("/user/detail");
        const response = await api.get();
        const obj = {
            user: response
        };
        dispatch(loginForm(obj));
        dispatch(status(localStorage.getItem("STATUS-CODE")));
        dispatch(loading());
    };
    request();
};
