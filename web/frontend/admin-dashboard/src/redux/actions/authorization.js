import {LOADING, LOGIN, STATUS} from "../constants";
import API from "../../services-api";

export const loading = () => {
    return {
        type: LOADING
    }
};

export const loginForm = (payload) => {
    return {
        type: LOGIN,
        payload
    }
};

export const status = (payload) => {
    return {
        type: STATUS,
        payload
    }
};


export const login = (data) => (dispatch) => {
    dispatch(loading());
    const request = async () => {
        const api = new API("/user/sign_in");
        const response = await api.authorization(data);
        dispatch(loginForm(response));
        dispatch(status(localStorage.getItem("STATUS-CODE")));
        dispatch(loading());
    };
    request();
};

