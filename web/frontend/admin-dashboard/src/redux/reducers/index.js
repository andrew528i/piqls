import {combineReducers} from "redux";
import {user, loading, statusCode} from "./authorization";
import {polygon} from "./polygon";
import {orderList} from './orderList'

const rootReducer = combineReducers({
    user,
    loading,
    statusCode,
    polygon,
    orderList
});

export default rootReducer
