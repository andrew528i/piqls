import {ORDER_LIST} from "../constants";

const initialStateOrderList = {};

export const orderList = function (state = initialStateOrderList, {type, payload}) {
    switch (type) {
        case ORDER_LIST:
            return {
                ...state,
                ...payload
            };
        default:
            return {
                ...state
            }
    }
};


