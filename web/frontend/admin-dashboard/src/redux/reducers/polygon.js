import {POLYGON} from "../constants";

const initialStateUser = {};

export const polygon = function (state = initialStateUser, {type, payload}) {
    switch (type) {
        case POLYGON:
            return payload;

        default:
            return state
    }
};