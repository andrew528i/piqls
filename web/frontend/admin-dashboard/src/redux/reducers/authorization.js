import {LOADING, LOGIN, STATUS} from "../constants";

const initialStateUser = {};

export const user = function (state = initialStateUser, {type, payload}) {
    switch (type) {
        case LOGIN:
            return {
                ...state,
                ...payload
            };
        default:
            return {
                ...state
            }
    }
};

const initialStateLoading = false;

export const loading = function (state = initialStateLoading, {type}) {
    switch (type) {
        case LOADING:
            return !state;
        default:
            return state;
    }
};

const initialStateStatusCode = localStorage.getItem("STATUS-CODE");

export const statusCode = (state = initialStateStatusCode, {type, payload} ) => {

    switch (type) {
        case STATUS:
            return payload;
        default:
            return state;
    }
};


