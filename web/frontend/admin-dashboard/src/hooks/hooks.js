import {useEffect, useRef} from "react";

export const useRequest = (...args) => {

    const memoObject = useRef();
    memoObject.current = args;

    useEffect(() => {
        memoObject.current.forEach(el => el())
    }, [memoObject]);

};

export const useMemoObject = (obj) => {

    const useMemoObject = useRef();

    return useMemoObject.current = obj;
};


