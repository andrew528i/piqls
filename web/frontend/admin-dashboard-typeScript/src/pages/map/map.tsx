import React, {FC} from "react";
import GoogleMaps from "../../components/google-map/google-map";

const Map: FC = () => {

    return (
        <GoogleMaps
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI&libraries=geometry,drawing,places"
            loadingElement={<div style={{height: `100%`}}/>}
            containerElement={<div style={{height: `calc(100vh - 64px)`}}/>}
            mapElement={<div style={{height: `100%`}}/>}
        />
    )

}

export default Map
