import React, {ChangeEvent, FC, FormEvent, useEffect, useState} from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import {CircularProgress, Container, Fade, Link} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {getOrdersListAction} from '../../redux/orders/orders-actions';
import {NavLink, useHistory} from "react-router-dom";
import OrdersSearchForm from '../../components/orders-search-form/orders-search-form';
import {Pagination} from '@material-ui/lab';
import AlertDialog from "../../components/create-order/create-order";
import {getTariffListAction} from "../../redux/tariff-list/tariff-list-actions";
import {RootState} from "../../redux";

const formatDate = (param: string): string => {
    if (!param.length) return ''
    const date = new Date(param)
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        timezone: 'UTC',
        hour: 'numeric',
        minute: 'numeric'
    };
    return date.toLocaleString("en-US", options)
}

const useRowStyles = makeStyles((theme: Theme) => (
    createStyles({
        root: {
            '& > *': {
                borderBottom: 'unset',
            },
        },
        container: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4),
        },
    })
));

interface IRow {
    guid: string,
    loading: null | boolean
    created_at: string
    started_at: string
    updated_at: string
    state: string
    address: string
    duration: string
    currency: string
    amount: string
    customer: {
        username: string
        first_name: string
        last_name: string
        email: string
    }
}

function Row(props: { row: IRow }) {
    const {row} = props;
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();

    return (
        <React.Fragment>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.created_at}
                </TableCell>
                <TableCell align="left">{row.customer.username}</TableCell>
                <TableCell align="left">{row.customer.email}</TableCell>
                <TableCell align="left"><strong>{row.state}</strong></TableCell>
                <TableCell align="right">
                    <Link component={NavLink} to={`/order/details/${row.guid}`}>
                        Orders detail
                    </Link>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                {row.address}
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Date</TableCell>
                                        <TableCell>Customer</TableCell>
                                        <TableCell align="right">Duration</TableCell>
                                        <TableCell align="right">Total price ($)</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    <TableRow>
                                        <TableCell component="th" scope="row">
                                            <strong style={{display: "inline-block", width: 90}}>Create
                                                date:</strong> {row.created_at ? row.created_at : "-"} <br/>
                                            <strong style={{display: "inline-block", width: 90}}>Update
                                                date:</strong> {row.updated_at ? row.updated_at : "-"} <br/>
                                            <strong style={{display: "inline-block", width: 90}}>Start
                                                date:</strong> {row.started_at ? row.started_at : "-"} <br/>
                                        </TableCell>
                                        <TableCell>
                                            <strong style={{
                                                display: "inline-block",
                                                width: 90
                                            }}>Username:</strong> {row.customer.username ? row.customer.username : "-"}
                                            <br/>
                                            <strong style={{display: "inline-block", width: 90}}>First
                                                name:</strong> {row.customer.first_name ? row.customer.first_name : "-"}
                                            <br/>
                                            <strong style={{display: "inline-block", width: 90}}>Last
                                                name:</strong> {row.customer.last_name ? row.customer.last_name : "-"}
                                            <br/>
                                        </TableCell>
                                        <TableCell align="right">
                                            {row.duration} min
                                        </TableCell>
                                        <TableCell align="right">
                                            {row.amount + ' ' + row.currency}
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

interface IDefaultFormValue {
    page: string
    min_price: string
    max_price: string
    customer_username: string
    employee_username: string
    address: string
    state: string

    [key: string]: string;
}

const defaultFormValue: IDefaultFormValue = {
    page: "1",
    min_price: "",
    max_price: "",
    customer_username: "",
    employee_username: "",
    address: "",
    state: ""
}

const Orders: FC = () => {
    const classes = useRowStyles();
    const dispatch = useDispatch();
    const history = useHistory()
    const {csrf, results, loading, count} = useSelector(({user: {csrf}, orders}: RootState) => ({
        ...orders,
        csrf
    }))
    const load = Boolean(csrf)

    const [formValue, setFormValue] = useState(defaultFormValue)


    const pageFilter = (objectForm: IDefaultFormValue): void => {
        let searchUrl: string = "?"
        for (const key in formValue) {
            if (key === "page" && (objectForm[key] === "1")) {
                setFormValue(state => ({...state, [key]: "1"}))
                continue
            }
            searchUrl += objectForm[key].trim().length ? `${key}=${objectForm[key]}&` : ""
        }
        const url: string = searchUrl.slice(0, searchUrl.length - 1)
        history.push('/orders' + url)
    }

    const search = (e: FormEvent): void => {
        e.preventDefault()
        pageFilter({...formValue, page: "1"})
    }

    const inputOnChange = (e: ChangeEvent<HTMLInputElement>): void => {
        const {id, value} = e.target
        setFormValue(state => ({...state, [id]: value}))
    }

    const reset = () => {
        setFormValue(defaultFormValue);
        history.push('/orders')
    }

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setFormValue(state => ({...state, page: value + ""}))
        pageFilter({...formValue, page: value + ""})
    };

    useEffect(() => {
        const searchParams: any = new URLSearchParams(history.location.search)
        let searchUrl = "?"
        for (let p of searchParams) {
            searchUrl += p[0] + '=' + p[1] + '&'
            setFormValue(state => ({...state, [p[0]]: p[1]}))
        }

        const url: string = searchUrl.slice(0, searchUrl.length - 1)
        load && dispatch(getOrdersListAction(url))
    }, [dispatch, load, history.location.search])

    useEffect(() => {
        load && dispatch(getTariffListAction())
    }, [dispatch, load])

    const rows: [IRow] = results && results.map((elem: any) => ({
        ...elem,
        created_at: formatDate(elem.created_at),
        started_at: formatDate(elem.started_at),
        updated_at: formatDate(elem.updated_at)
    }))

    return (
        <Container className={classes.container}>
            <Paper elevation={0} variant="outlined">
                <OrdersSearchForm {...{formValue, inputOnChange, search, reset}}/>
                <AlertDialog/>
                <TableContainer>
                    <Table aria-label="collapsible table">
                        {(!loading || rows) &&
                        <TableHead>
                            <TableRow>
                                <TableCell/>
                                <TableCell>Date</TableCell>
                                <TableCell align="left">Username</TableCell>
                                <TableCell align="left">Email</TableCell>
                                <TableCell align="left">Status</TableCell>
                                <TableCell/>
                            </TableRow>
                        </TableHead>}
                        <TableBody style={loading ? {opacity: .3} : {opacity: 1}}>
                            {!!rows && rows.map((row) => (
                                <Row key={row.guid} row={row}/>
                            ))}
                        </TableBody>
                    </Table>
                    <Fade
                        in={loading}
                        style={rows ? {
                            position: "absolute",
                            top: '50%',
                            left: '50%'
                        } : {
                            display: "block",
                            margin: "20vh auto",
                        }}
                        unmountOnExit
                    >
                        <CircularProgress/>
                    </Fade>
                </TableContainer>
                {
                    rows && (Math.ceil(count / rows.length) > 1) &&
                    <Pagination
                        style={{display: "flex", justifyContent: "center", padding: "2rem 0"}}
                        count={Math.ceil(count / 15)}
                        page={!formValue.page.length ? 1 : +formValue.page}
                        onChange={handleChange} variant="outlined" shape="rounded"
                    />
                }
            </Paper>
        </Container>
    );
}

export default Orders
