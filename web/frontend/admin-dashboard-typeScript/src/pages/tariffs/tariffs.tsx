import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getTariffListAction} from "../../redux/tariff-list/tariff-list-actions";
import {RootState} from "../../redux";

interface ISelector {
    user: {
        csrf: string
    }
}

const Tariffs = () => {
    const dispatch = useDispatch()
    const {csrf} = useSelector(({user: {csrf}}: RootState) => ({
        csrf
    }))
    const load = Boolean(csrf)

    useEffect(() => {
        load && dispatch(getTariffListAction())
    }, [dispatch, load])

    // if (loading) return null

    return <></>
}

export default Tariffs
