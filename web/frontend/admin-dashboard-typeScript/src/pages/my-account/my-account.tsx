import React from "react";
import {CssBaseline, Paper, Avatar, Box, List, ListItem, ListItemAvatar, ListItemText, Grid} from "@material-ui/core";
import {useSelector} from "react-redux";
import {makeStyles, createStyles, Theme} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import MailIcon from '@material-ui/icons/Mail';
import PhoneIcon from '@material-ui/icons/Phone';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import WorkIcon from '@material-ui/icons/Work';
import {RootState} from "../../redux";

const useStyles = makeStyles((theme: Theme) => (
    createStyles({
        root: {
            width: '100%',
            maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
            margin: '-8px 0',
            padding: 0
        },
        container: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4),
        },
        paper: {
            padding: theme.spacing(2),
            display: 'flex',
            overflow: 'auto',
            flexDirection: 'column',
        },
        fixedHeight: {
            height: 240,
        },
        large: {
            width: theme.spacing(25),
            height: theme.spacing(25),
            marginRight: 50
        },
    })
));

const MyAccount = () => {
    const classes = useStyles()
    const userStore = useSelector(({user}: RootState) => user)

    return (
        <>
            <CssBaseline/>
            <Container maxWidth="lg" className={classes.container}>
                <Paper elevation={0}>
                    <Box p={3}>
                        <Grid container>
                            {/*{userStore.loading && <Avatar variant="rounded" className={classes.large}/>}*/}
                            {userStore.avatar ?
                                <Avatar variant="rounded" alt={userStore.first_name || userStore.username} src={userStore.avatar}
                                        className={classes.large}/> :
                                <Avatar variant="rounded" alt={userStore.first_name || userStore.username} className={classes.large}/>}
                            <List className={classes.root}>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <AccountCircleIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={userStore.first_name || "-"} secondary="First name"/>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <AccountCircleIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={userStore.last_name || "-"} secondary="Last name"/>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <AccountCircleIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={userStore.username || "-"} secondary="Username"/>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <WorkIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={userStore.group || "-"} secondary="Group"/>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <MailIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={userStore.email || "-"} secondary="Email"/>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <PhoneIcon/>
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={userStore.phone || "-"} secondary="Phone"/>
                                </ListItem>
                            </List>
                        </Grid>
                    </Box>
                </Paper>
            </Container>
        </>
    )
}

export default MyAccount
