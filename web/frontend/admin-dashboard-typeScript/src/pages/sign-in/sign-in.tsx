import React, {FC} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Copyright from '../../components/copyright'
import {Formik, Field, FormikProps} from 'formik';
import {TextField} from 'formik-material-ui'
import {useDispatch, useSelector} from 'react-redux';
import {signInAction} from '../../redux/user/user-action';
import Logo from './images/piqls.svg'
import {Alert} from "@material-ui/lab";
import {RootState} from "../../redux";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundImage: `url(${Logo})`,
        backgroundRepeat: 'no-repeat',
        height: '100vh',
        display: 'flex',
        overflow: 'hidden',
        flexDirection: 'column',
        alignItems: 'center',
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'center',
        maxWidth: '390px',
        justifyItems: 'stretch'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const SignIn: FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch()
    const {error} = useSelector(({user}: RootState) => user);

    return (
        <Container className={classes.root} component="main">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar} src={Logo}/>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <Formik
                    initialValues={{username: '', password: ''}}
                    onSubmit={(values) => {
                        dispatch(signInAction(values))
                    }}
                >
                    {(props: FormikProps<any>) => (
                        <form className={classes.form} onSubmit={props.handleSubmit}>
                            <Field
                                component={TextField}
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoComplete="name"
                                autoFocus
                                error={error}
                                disabled={false}
                            />
                            <Field
                                component={TextField}
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                disabled={false}
                                error={error}
                            />
                            {error && <Alert severity="error">This is an error alert — check it out!</Alert>}
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Sign In
                            </Button>
                        </form>
                    )}
                </Formik>
            </div>
            <Box mb={8}>
                <Copyright/>
            </Box>
        </Container>
    );
}

export default SignIn
