import React, {FC, useEffect} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {
    Container,
    Paper,
    Box,
    List,
    ListItemText,
    ListItem,
    Avatar,
    ListItemAvatar,
    CardMedia,
    Typography
} from '@material-ui/core';
import {useDispatch, useSelector} from "react-redux";
import {getOrderDetailAction} from '../../redux/order-details/order-details-actions';
import {useParams} from 'react-router-dom';
import Grid from "@material-ui/core/Grid";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import PhoneIcon from '@material-ui/icons/Phone';
import DnsIcon from '@material-ui/icons/Dns';
import GoogleMaps from "../../components/google-map/google-map";
import {RootState} from "../../redux";

const useRowStyles = makeStyles((theme: Theme) => (
    createStyles({
        root: {
            '& > *': {
                borderBottom: 'unset',
            },
        },
        container: {
            paddingTop: theme.spacing(4),
            paddingBottom: theme.spacing(4),
        },
        title: {
            marginTop: "20px"
        },
        paper: {
            padding: '10px'
        },
        avatar: {
            objectFit: 'contain',
            height: 150,
            maxWidth: '50%',
            width: 'auto',
        },
        list: {
            margin: 0,
            padding: 0,
            width: 250
        },
        listItem: {
            padding: 0
        }
    })
));

const OrderDetails: FC = () => {
    const classes = useRowStyles()
    const dispatch = useDispatch()
    const {id} = useParams()
    const {csrf, orderDetails} = useSelector(({user: {csrf}, orderDetails}: RootState) => ({
        orderDetails,
        csrf
    }))

    const load = Boolean(csrf)

    const {loading, customer, employee, location} = orderDetails

    useEffect(() => {
        load && dispatch(getOrderDetailAction(id))
    }, [dispatch, id, load])

    if (loading) return null

    return (
        <Container className={classes.container}>
            <Typography className={classes.title} variant="h3" component={"h1"}>
                Order details
            </Typography>
            <Grid container justify={"space-between"} spacing={2}>
                <Grid item xs={12}>
                    <Paper elevation={1}>
                        <Grid container justify={"space-between"}>
                            <Grid xs={12} sm={6} item>
                                <Box p={2}>
                                    <Typography variant="h6" component={"h2"}>
                                        Details
                                    </Typography>
                                    <List className={classes.list}>
                                        <ListItem className={classes.listItem}>
                                            <ListItemAvatar>
                                                <Avatar>
                                                    <PermIdentityIcon/>
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={customer.id || "-"} secondary="ID"/>
                                        </ListItem>
                                        <ListItem className={classes.listItem}>
                                            <ListItemAvatar>
                                                <Avatar>
                                                    <AccountCircleIcon/>
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={customer.username || "-"} secondary="Username"/>
                                        </ListItem>
                                        <ListItem className={classes.listItem}>
                                            <ListItemAvatar>
                                                <Avatar>
                                                    <AccountCircleIcon/>
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={customer.first_name || "-"} secondary="First name"/>
                                        </ListItem>
                                        <ListItem className={classes.listItem}>
                                            <ListItemAvatar>
                                                <Avatar>
                                                    <AccountCircleIcon/>
                                                </Avatar>
                                            </ListItemAvatar>
                                            <ListItemText primary={customer.last_name || "-"} secondary="Last name"/>
                                        </ListItem>
                                    </List>
                                </Box>
                            </Grid>
                            <Grid xs={12} sm={6} item>
                                <GoogleMaps
                                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI&libraries=geometry,drawing,places"
                                    loadingElement={<div style={{height: '300px'}}/>}
                                    containerElement={<div style={{height: `300px`}}/>}
                                    mapElement={<div style={{height: `300px`}}/>}
                                    point={location.coordinates}
                                />
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                {customer &&
                <Grid item xs={12} sm={6}>
                    <Paper elevation={1}>
                        <Box p={2}>
                            <Typography variant="h6" component={"h2"}>
                                Customer
                            </Typography>
                            {customer.avatar &&
                            <CardMedia
                                className={classes.avatar}
                                image={customer.avatar}
                                component="img"
                                title="Paella dish"
                            />
                            }
                            <Grid
                                container
                                direction="row"
                                justify="flex-start"
                            >
                                <List className={classes.list}>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <PermIdentityIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.id || "-"} secondary="ID"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.username || "-"} secondary="Username"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.first_name || "-"} secondary="First name"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.last_name || "-"} secondary="Last name"/>
                                    </ListItem>
                                </List>
                                <List className={classes.list}>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AlternateEmailIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.email || "-"} secondary="Email"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <PhoneIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.phone || "-"} secondary="Phone"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <DnsIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.ip_address || "-"} secondary="Ip address"/>
                                    </ListItem>
                                </List>
                            </Grid>
                        </Box>
                    </Paper>
                </Grid>}
                {employee &&
                <Grid item xs={12} sm={6}>
                    <Paper elevation={1}>
                        <Box p={2}>
                            <Typography variant="h6" component={"h2"}>
                                Employee
                            </Typography>
                            {employee.avatar &&
                            <CardMedia
                                className={classes.avatar}
                                image={employee.avatar}
                                component="img"
                                title="Paella dish"
                            />}
                            <Grid
                                container
                                direction="row"
                                justify="flex-start"
                            >
                                <List className={classes.list}>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <PermIdentityIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.id || "-"} secondary="ID"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.username || "-"} secondary="Username"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.first_name || "-"} secondary="First name"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AccountCircleIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.last_name || "-"} secondary="Last name"/>
                                    </ListItem>
                                </List>
                                <List className={classes.list}>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <AlternateEmailIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.email || "-"} secondary="Email"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <PhoneIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.phone || "-"} secondary="Phone"/>
                                    </ListItem>
                                    <ListItem className={classes.listItem}>
                                        <ListItemAvatar>
                                            <Avatar>
                                                <DnsIcon/>
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={employee.ip_address || "-"} secondary="Ip address"/>
                                    </ListItem>
                                </List>
                            </Grid>
                        </Box>
                    </Paper>
                </Grid>}
            </Grid>
        </Container>
    )
}

export default OrderDetails
