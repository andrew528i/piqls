import React, {FC} from 'react'
import {Container, Box, Typography, Link} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import errorAction from "../../redux/error/error-actions";

const useStyles = makeStyles({
    root: {
        width: '100vw',
        height: '100vh',
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    },
    linkButton: {
        fontFamily: 'Nunito,-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"',
        display: "inline-block",
        marginTop: "8px",
        fontSize: "0.875rem",
        fontWeight: 500,
        borderRadius: "4px",
        backgroundColor: "#2196f3",
        color: "white",
        padding: "6px 16px",
        lineHeight: 1.75,
        textDecoration: "none",
        transition: '.3s background-color ease',
        '&:hover': {
            textDecoration: 'none',
            backgroundColor: 'rgb(23, 105, 170)'
        }
    }
});


const Page500: FC = () => {
    const classes = useStyles()
    const dispatch = useDispatch()

    return (
        <Container className={classes.root}>
            <Box style={{textAlign: "center"}}>
                <Typography style={{fontSize: "2rem", fontWeight: 600}} variant="h1" component="h1" gutterBottom>
                    500
                </Typography>
                <Typography style={{fontSize: "1.125rem", fontWeight: 600}} variant="h1" component="h2" gutterBottom>
                    Internal server error.
                </Typography>
                <Typography style={{fontSize: "14px", fontWeight: 400, maxWidth: "470px", lineHeight: 1.5}} variant="h1"
                            component="h2" gutterBottom>
                    The server encountered something unexpected that didn't allow it to complete the request.
                </Typography>
                <Link component={NavLink} to="/" className={classes.linkButton} onClick={() => dispatch(errorAction())}>
                    Return to website
                </Link>
            </Box>
        </Container>
    )
}

export default Page500
