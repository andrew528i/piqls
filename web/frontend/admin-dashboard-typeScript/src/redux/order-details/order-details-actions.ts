import {GET_ORDER_DETAILS, GET} from '../../constants'

export const getOrderDetailAction = (url: string = "") => {
    return {
        type: GET_ORDER_DETAILS,
        payload: {
            method: GET,
            url: '/order/state/' + url
        }
    }
}
