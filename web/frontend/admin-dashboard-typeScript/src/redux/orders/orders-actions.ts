import {GET_ORDERS, GET} from '../../constants'

export const getOrdersListAction = (url: string = "") => {
    return {
        type: GET_ORDERS,
        payload: {
            method: GET,
            url: '/order/list' + url
        }
    }
}
