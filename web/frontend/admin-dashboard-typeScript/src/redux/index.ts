import { combineReducers } from 'redux'
import user from './user/user-reducer'
import orders from "./orders/orders-reducer";
import orderDetails from "./order-details/order-details-reducer";
import error from "./error/error-reducer";
import polygon from "./polygon/polygon-reducer";
import tariffs from "./tariff-list/tariff-list-reducer";
import orderCreated from "./create-order/create-order-reducer";

const rootReducer = combineReducers({
    error,
    user,
    orders,
    orderDetails,
    polygon,
    tariffs,
    orderCreated
})

export type RootState = ReturnType<typeof rootReducer>
export default rootReducer
