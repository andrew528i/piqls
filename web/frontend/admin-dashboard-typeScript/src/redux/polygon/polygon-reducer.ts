import {REQUEST, SUCCESS, GET_POLYGON} from "../../constants";
import {IDispatch} from "../../interfaces";

interface IInitialState {
    detail: null | boolean
    loading: null | boolean
}

const initialState: IInitialState = {
    detail: null,
    loading: true
};

const polygon = (store = initialState, {type, payload}: IDispatch) => {
    switch (type) {
        case REQUEST + GET_POLYGON:
            return {
                ...store,
                ...payload,
                loading: true
            }
        case SUCCESS + GET_POLYGON:
            return {
                ...store,
                ...payload,
                loading: false
            }
        default:
            return store
    }
}

export default polygon
