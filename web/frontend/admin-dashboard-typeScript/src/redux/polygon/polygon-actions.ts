import {GET, GET_POLYGON} from "../../constants";

export const getPolygonListAction = (url: string = "") => {
    return {
        type: GET_POLYGON,
        payload: {
            method: GET,
            url: '/order/area/list' + url
        }
    }
}
