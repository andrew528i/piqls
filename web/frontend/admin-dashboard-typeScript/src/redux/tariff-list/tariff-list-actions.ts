import {GET_TARIFFS, GET} from '../../constants'

export const getTariffListAction = (url: string = "") => {
    return {
        type: GET_TARIFFS,
        payload: {
            method: GET,
            url: '/order/tariff/list' + url
        }
    }
}
