import {GET_TARIFFS, REQUEST, SUCCESS, ERROR} from '../../constants'
import {IDispatch} from '../../interfaces'

interface IInitialState {
    error: null | boolean
    detail: null | boolean
    loading: null | boolean
}

const initialState: IInitialState = {
    error: null,
    detail: null,
    loading: true
};

const tariffs = (state = initialState, {type, payload}: IDispatch) => {

    switch (type) {
        case REQUEST + GET_TARIFFS:
            return {
                ...state,
                loading: true,
                detail: null,
                error: null
            }
        case SUCCESS + GET_TARIFFS:
            return {
                ...state,
                ...payload,
                loading: false,
                detail: null,
                error: null
            }
        case ERROR + GET_TARIFFS:
            return {
                ...state,
                ...payload,
                error: true
            }
        default:
            return state
    }
}

export default tariffs
