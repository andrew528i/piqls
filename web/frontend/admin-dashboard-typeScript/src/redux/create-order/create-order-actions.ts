import {CREATE_ORDER, POST} from '../../constants'

export const createOrderAction = (data: object) => {
    return {
        type: CREATE_ORDER,
        payload: {
            method: POST,
            url: "/order/create",
            body: data
        }
    }
}
