import {CREATE_ORDER, REQUEST, SUCCESS, ERROR} from '../../constants'
import {IDispatch} from '../../interfaces'

interface IInitialState {
    error: null | boolean
    detail: null | boolean
    loading?: null | boolean
}

const initialState: IInitialState = {
    error: null,
    detail: null
};

const orderCreated = (state = initialState, {type, payload}: IDispatch) => {

    switch (type) {
        case REQUEST + CREATE_ORDER:
            return {
                ...state,
                loading: true,
                detail: null,
                error: null
            }
        case SUCCESS + CREATE_ORDER:
            return {
                ...state,
                ...payload,
                loading: false,
                detail: null,
                error: null
            }
        case ERROR + CREATE_ORDER:
            return {
                ...state,
                ...payload,
                error: true
            }
        default:
            return state
    }
}
export default orderCreated

