import {ERROR, GLOBAL_ERROR} from '../../constants'

const errorAction = (errorText: string | null = null, errorStatusCode: number | null = null) => ({
    type: ERROR + GLOBAL_ERROR,
    payload: {
        errorText,
        errorStatusCode
    }
})

export default errorAction
