import {ERROR, GLOBAL_ERROR} from '../../constants'
import {IDispatch} from '../../interfaces'

interface IInitialState {
    errorText: null | string
    errorStatusCode: null | number
}

const initialState: IInitialState = {
    errorText: null,
    errorStatusCode: null
};

const error = (state = initialState, {type, payload}: IDispatch) => {

    switch (type) {
        case ERROR + GLOBAL_ERROR:
            return {
                ...state,
                ...payload
            }
        default:
            return state
    }
}
export default error

