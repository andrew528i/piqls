import {LOGIN, REQUEST, SUCCESS, ERROR, LOGOUT, GET_USER_DETAIL} from '../../constants'
import {IDispatch} from '../../interfaces'

interface IInitialState {
    error: null | boolean
    detail: null | boolean
    loading: null | boolean
    csrf: null | string
}

const initialState: IInitialState = {
    error: null,
    detail: null,
    loading: null,
    csrf: null
};

const user = (state = initialState, {type, payload}: IDispatch) => {
    switch (type) {
        case REQUEST + LOGIN:
        case REQUEST + GET_USER_DETAIL:
            return {
                ...state,
                loading: true,
                detail: null,
                error: null
            }
        case SUCCESS + LOGIN:
        case SUCCESS + GET_USER_DETAIL:
            localStorage.setItem('X-CSRFToken', payload.csrf)
            return {
                ...state,
                ...payload,
                loading: false,
                detail: null,
                error: null,
            }
        case ERROR + LOGIN:
            return {
                ...state,
                ...payload,
                error: true
            }
        case LOGOUT : {
            localStorage.clear()
            return {
                ...payload,
                error: null,
                detail: null,
                loading: null,
                csrf: null
            }
        }
        default:
            return state
    }
}

export default user
