import { IDispatch } from '../../interfaces';
import { Dispatch } from 'react';
import {LOGIN, REQUEST, SUCCESS, ERROR, GET, GET_USER_DETAIL, LOGOUT} from '../../constants'
import Api from '../../services'

const start = () => ({ type: REQUEST + LOGIN })
const end = (payload: object) => ({ type: SUCCESS + LOGIN, payload: payload })
const error = (payload: object) => ({ type: ERROR + LOGIN, payload: payload })

export const signInAction = (params: object) => async (dispatch: Dispatch<IDispatch>) => {
    try {
        dispatch(start())
        const api = new Api()
        const response = await api.signIn(params)
        const obj = await response.json()
        if (response.ok) {
            const {csrf, session_id} = obj
            dispatch(end({csrf, session_id, ...obj.user}))
            return
        }
        dispatch(error(obj))
    } catch (e) {
        throw new Error(e)
    }
}

export const logoutAction = (payload?: object) => ({type: LOGOUT, payload})
export const userDetailAction = () => {
    return {
        type: GET_USER_DETAIL,
        payload: {
            method: GET,
            url: '/user/detail'
        }
    }
}
