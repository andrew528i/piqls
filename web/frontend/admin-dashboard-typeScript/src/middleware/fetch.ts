import {GET, REQUEST, SUCCESS} from '../constants';
import {Dispatch} from 'react';
import Api from "../services";
import {logoutAction} from '../redux/user/user-action';
import errorAction from '../redux/error/error-actions';

interface IDispatch {
    type: string
    payload?: any
}

const fetchMiddleware = () => (next: Dispatch<IDispatch>) => async (
    {type, payload}: { type: string, payload: { body?: object, method?: string, url?: string } }
) => {
    let statusCode: number | null = null
    try {
        if (payload && payload.method) {

            if (!localStorage.getItem('X-CSRFToken')) return next(logoutAction())

            const api = new Api()

            next({type: REQUEST + type, payload: {loading: true}})

            const response = payload.method === GET ?
                await api.methodGet(payload.url) :
                await api.methodPOST(payload.url, payload.body)
            statusCode = response.status;
            if (statusCode === 403) {
                const logoutObject = await response.json()
                next(logoutAction(logoutObject))
            }
            const successObject = await response?.json()
            next({type: SUCCESS + type, payload: successObject})

        } else {
            next({type, payload})
        }
    } catch (error) {
        next(errorAction(error.toString(), statusCode))
        throw new Error(error)
    }
}

export default fetchMiddleware
