import React from 'react';
import {createStyles, Theme, makeStyles} from '@material-ui/core/styles';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoneyIcon from '@material-ui/icons/Money';
import BusinessIcon from '@material-ui/icons/Business';
import NatureIcon from '@material-ui/icons/Nature';
import {Button} from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        marginTop: {
            marginTop: '1rem'
        },
        margin: {
            display: 'flex',
            justifyContent: 'space-between'
        },
        container: {
            margin: 0,
            padding: '1rem',
            paddingTop: 0,
            width: '100%'
        }
    }),
);

interface IDefaultFormValue {
    page: string
    min_price: string
    max_price: string
    customer_username: string
    employee_username: string
    address: string
    state: string
}

const OrdersSearchForm = (
    {formValue, inputOnChange, search, reset}:
        {
            formValue: IDefaultFormValue
            inputOnChange(e: any): void
            search(e: any): void
            reset(): void
        }
) => {
    const classes = useStyles();

    return (
        <form onSubmit={search}>
            <Grid className={classes.container} container spacing={3}>
                <Grid item xs={4}>
                    <TextField
                        className={classes.marginTop}
                        onChange={inputOnChange}
                        id="min_price"
                        label="Min price"
                        value={formValue.min_price}
                        type="number"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <MoneyIcon/>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <TextField
                        className={classes.marginTop}
                        onChange={inputOnChange}
                        id="max_price"
                        label="Max price"
                        value={formValue.max_price}
                        type="number"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <MoneyIcon/>
                                </InputAdornment>
                            ),
                        }}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        className={classes.marginTop}
                        onChange={inputOnChange}
                        id="customer_username"
                        label="Customer username"
                        value={formValue.customer_username}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AccountCircle/>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <TextField
                        className={classes.marginTop}
                        onChange={inputOnChange}
                        id="employee_username"
                        label="Employee username"
                        value={formValue.employee_username}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AccountCircle/>
                                </InputAdornment>
                            ),
                        }}
                    />
                </Grid>
                <Grid item xs={4}>
                    <TextField
                        className={classes.marginTop}
                        onChange={inputOnChange}
                        id="address"
                        label="Address"
                        value={formValue.address}
                        fullWidth
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <BusinessIcon/>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <TextField
                        className={classes.marginTop}
                        onChange={inputOnChange}
                        id="state"
                        label="Status"
                        value={formValue.state}
                        fullWidth
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <NatureIcon/>
                                </InputAdornment>
                            ),
                        }}
                    />
                </Grid>
                <Grid className={classes.margin} item xs={12}>
                    <Button
                        style={{marginLeft: "auto", marginRight: "1rem"}}
                        type="reset"
                        variant="contained"
                        color="primary"
                        onClick={reset}
                    >
                        Reset
                    </Button>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                    >
                        Search
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
}

export default OrdersSearchForm
