import React, { FC } from 'react'
import { Typography, Link } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const Copyright: FC = () => {
    const {location: {pathname}} = useHistory()

    if (pathname === "/map") return null

    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://piqls.com/" target="_blank">PIQLS</Link>
            {' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    )
}

export default Copyright
