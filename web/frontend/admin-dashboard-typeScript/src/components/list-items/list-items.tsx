import React, {FC} from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import LayersIcon from '@material-ui/icons/Layers';
import AssignmentIcon from '@material-ui/icons/Assignment';
import {NavLink} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import MapIcon from '@material-ui/icons/Map';

const useStyles = makeStyles({
    linkNav: {
        color: "inherit",
        textDecoration: "none"
    }
})

export const MainListItems: FC = () => {
    const classes = useStyles()
    return (
        <div>
            <NavLink to="/" className={classes.linkNav}>
                <ListItem button>
                    <ListItemIcon>
                        <DashboardIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Dashboard"/>
                </ListItem>
            </NavLink>
            <NavLink to="/orders" className={classes.linkNav}>
                <ListItem button>
                    <ListItemIcon>
                        <ShoppingCartIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Orders"/>
                </ListItem>
            </NavLink>
            <NavLink to="/customers" className={classes.linkNav}>
                <ListItem button>
                    <ListItemIcon>
                        <PeopleIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Customers"/>
                </ListItem>
            </NavLink>
            <NavLink to="/map" className={classes.linkNav}>
                <ListItem button>
                    <ListItemIcon>
                        <MapIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Map"/>
                </ListItem>
            </NavLink>
            <NavLink to="/tariffs" className={classes.linkNav}>
                <ListItem button>
                    <ListItemIcon>
                        <LayersIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Tariffs"/>
                </ListItem>
            </NavLink>
        </div>
    )
};

export const SecondaryListItems = () => (
    <div>
        <ListSubheader inset>Saved reports</ListSubheader>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Current month"/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Last quarter"/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Year-end sale"/>
        </ListItem>
    </div>
);
