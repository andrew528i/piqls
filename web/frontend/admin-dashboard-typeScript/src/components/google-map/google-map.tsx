import {GoogleMap, Marker, withGoogleMap, withScriptjs, Polygon} from "react-google-maps";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getPolygonListAction} from "../../redux/polygon/polygon-actions";
import {WithGoogleMapProps} from "react-google-maps/lib/withGoogleMap";
import {WithScriptjsProps} from "react-google-maps/lib/withScriptjs";
import {RootState} from "../../redux";

const GoogleMaps: React.ComponentClass<{ readonly point?: any } & WithGoogleMapProps & WithScriptjsProps> = withScriptjs(withGoogleMap(({point = []}) => {
    const dispatch = useDispatch()
    const {results = [], csrf} = useSelector(({user: {csrf}, polygon}: RootState) => ({...polygon, csrf}))
    const load = Boolean(csrf)
    useEffect(() => {
        load && dispatch(getPolygonListAction())
    }, [dispatch, load])

    const pointPosition = point.reduce((acc: [], element: object, i: number) => (!(i % 2) ? [...acc, {
        lat: point[i],
        lng: point[++i]
    }] : acc), [])

    let polygonPosition;

    if (results.length) {
        polygonPosition = results.map((element: any) => element.polygon.coordinates.flat(2)
            .reduce((acc: [], element: object, i: number, arr: []) => !(i % 2) ? [...acc, {lat: arr[i], lng: arr[++i]}] : acc, []))
    }

    return (
        <GoogleMap
            defaultZoom={8}
            defaultCenter={pointPosition[0] || {lat: 37.764379306884855, lng: -122.43037965148687}}
            center={pointPosition[0] || {lat: 37.764379306884855, lng: -122.43037965148687}}
        >
            {pointPosition.map((element: [], i: number) => (
                <Marker
                    position={element}
                    key={i + 'pointPosition'}
                />
            ))}
            {polygonPosition && polygonPosition.map((element: [] , i: number) => (
                <Polygon
                    path={element}
                    key={i + 'polygonPosition'}
                    options={{
                        fillColor: "#000",
                        fillOpacity: 0.4,
                        strokeColor: "#000",
                        strokeOpacity: 1,
                        strokeWeight: 1
                    }}
                    onClick={() => {
                        console.log("ahmet")
                    }}/>
            ))}

        </GoogleMap>
    )
}));

export default GoogleMaps
