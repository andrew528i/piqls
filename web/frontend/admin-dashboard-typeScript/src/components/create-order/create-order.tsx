import React, {FC} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AddIcon from "@material-ui/icons/Add";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import CreateOrderForm from './create-order-form/create-order-form';
import {useSelector} from "react-redux";
import {RootState} from "../../redux";

const useStyles = makeStyles((theme: Theme) => (
    createStyles({
        container: {
            padding: '2rem'
        },
        button: {
            display: 'flex',
            fontWeight: 600,
            marginLeft: 'auto',
            marginRight: '1.7rem',
            width: '100%',
            maxWidth: '11rem'
        },
        buttonSend: {
            display: 'flex',
            marginLeft: 'auto'
        }
    })
));

const AlertDialog: FC = () => {
    const classes = useStyles()
    const [open, setOpen] = React.useState(false);
    const {loading}: { loading: boolean | undefined } = useSelector(({orderCreated}: RootState) => orderCreated)
    // console.log(loading)
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button
                variant="outlined"
                color="primary"
                className={classes.button}
                endIcon={<AddIcon/>}
                onClick={handleClickOpen}
            >
                Create order
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth
                maxWidth={"sm"}
                // className={classes.container}
            >
                <CreateOrderForm/>
            </Dialog>
        </div>
    );
}

export default AlertDialog
