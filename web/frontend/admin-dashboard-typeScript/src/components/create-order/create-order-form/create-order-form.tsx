import React, {ChangeEvent, FormEvent} from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import parse from 'autosuggest-highlight/parse';
import throttle from 'lodash/throttle';
import {Button} from "@material-ui/core";
import Geocode from "react-geocode";
import GoogleMaps from "../../google-map/google-map";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../../redux";
import {createOrderAction} from "../../../redux/create-order/create-order-actions";

Geocode.setApiKey("AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI&libraries");

function loadScript(src: string, position: HTMLElement | null, id: string) {
    if (!position) {
        return;
    }

    const script = document.createElement('script');
    script.setAttribute('async', '');
    script.setAttribute('id', id);
    script.src = src;
    position.appendChild(script);
}

const autocompleteService = {current: null};

const useStyles = makeStyles((theme) => ({
    icon: {
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(2),
    },
    form: {
        padding: '1.5rem',
        width: '100%'
    }
}));

interface PlaceType {
    description: string;
    structured_formatting: {
        main_text: string;
        secondary_text: string;
        main_text_matched_substrings: [
            {
                offset: number;
                length: number;
            },
        ];
    };
}

interface IFormValue {
    coordinates: number[]
    id: string
    tariff: string | undefined
}

export default function CreateOrderForm() {
    const classes = useStyles();
    const dispatch = useDispatch()
    const tariffsList = useSelector((state: RootState) => state.tariffs)
    const [value, setValue] = React.useState<PlaceType | null>(null);
    const [inputValue, setInputValue] = React.useState('');
    const [options, setOptions] = React.useState<PlaceType[]>([]);
    const [formValue, setFormValue] = React.useState<IFormValue>({
        coordinates: [],
        id: '',
        tariff: ''
    })
    const loaded = React.useRef(false);
    const {results}: { results: [] | undefined } = tariffsList

    if (typeof window !== 'undefined' && !loaded.current) {
        if (!document.querySelector('#google-maps')) {
            loadScript(
                'https://maps.googleapis.com/maps/api/js?key=AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI&libraries=places',
                document.querySelector('head'),
                'google-maps',
            );
        }

        loaded.current = true;
    }

    const fetch = React.useMemo(
        () =>
            throttle((request: { input: string }, callback: (results?: PlaceType[]) => void) => {
                (autocompleteService.current as any).getPlacePredictions(request, callback);
            }, 200),
        [],
    );

    React.useEffect(() => {
        let active = true;

        if (!autocompleteService.current && (window as any).google) {

            autocompleteService.current = new (window as any).google.maps.places.AutocompleteService();
        }
        if (!autocompleteService.current) {
            return undefined;
        }

        if (inputValue === '') {
            setOptions(value ? [value] : []);
            return undefined;
        }

        fetch({input: inputValue}, (results?: PlaceType[]) => {
            if (active) {
                let newOptions = [] as PlaceType[];
                if (value) {
                    newOptions = [value];
                }

                if (results) {
                    newOptions = [...newOptions, ...results];
                }

                setOptions(newOptions);
            }
        });

        return () => {
            active = false;
        };
    }, [value, inputValue, fetch]);

    const submitForm = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        const responseObject = {
            location: {
                latitude: formValue.coordinates[0],
                longitude: formValue.coordinates[1]
            },
            tariff_guid: "3a8c929d-3159-48ca-bdc0-397b63d7ec33",
            started_at: new Date().toISOString(),
            customer_id: formValue.id
        }
        dispatch(createOrderAction(responseObject))
    }

    return (
        <form className={classes.form} onSubmit={submitForm}>
            <div style={{display: "flex", justifyContent: "space-between", flexWrap: "wrap"}}>
                <TextField
                    required
                    onChange={
                    (e: ChangeEvent<HTMLInputElement>) => {
                        const {value} = e.target
                        setFormValue(state => ({...state, id: value}))
                    }} label="Customer id" name="customer_id"/>
                <Autocomplete
                    freeSolo
                    id="free-solo-2-demo"
                    disableClearable
                    style={{width: "168px"}}
                    options={results ? results.map((option: { name: string }) => option.name) : []}
                    onChange={(event: any, value: any) => {
                        const resultObject: { guid?: string } | undefined = results?.find((element: { name: string, guid: string }): boolean => element.name === value)
                        setFormValue(state => ({...state, tariff: resultObject ? resultObject!.guid : resultObject}))
                    }}
                    renderInput={(params) => {
                        return (<TextField
                            required
                            {...params}
                            label="Tariffs"
                            InputProps={{...params.InputProps, type: 'search'}}
                        />)
                    }}
                />
            </div>
            <Autocomplete
                id="google-map-demo"
                style={{margin: "1rem 0"}}
                getOptionLabel={(option) => (typeof option === 'string' ? option : option.description)}
                filterOptions={(x) => x}
                options={options}
                autoComplete
                fullWidth
                includeInputInList
                filterSelectedOptions
                value={value}
                onChange={(event: any, newValue: PlaceType | null) => {
                    setOptions(newValue ? [newValue, ...options] : options);
                    setValue(newValue);

                    Geocode.fromAddress(newValue ? newValue.description : "").then(
                        response => {
                            const {lat, lng} = response.results[0].geometry.location;
                            setFormValue(state => ({...state, coordinates: [lat, lng]}))
                        },
                        error => {
                            console.error(error);
                        }
                    );
                }}
                onInputChange={(event, newInputValue) => {
                    setInputValue(newInputValue);
                }}
                renderInput={(params) => (
                    <TextField required {...params} label="Add a location" fullWidth/>
                )}
                renderOption={(option) => {
                    const matches = option.structured_formatting.main_text_matched_substrings;
                    const parts = parse(
                        option.structured_formatting.main_text,
                        matches.map((match: any) => [match.offset, match.offset + match.length]),
                    );

                    return (
                        <Grid container alignItems="center">
                            <Grid item>
                                <LocationOnIcon className={classes.icon}/>
                            </Grid>
                            <Grid item xs>
                                {parts.map((part, index) => (
                                    <span key={index} style={{fontWeight: part.highlight ? 700 : 400}}>
                                    {part.text}
                                </span>
                                ))}
                                <Typography variant="body2" color="textSecondary">
                                    {option.structured_formatting.secondary_text}
                                </Typography>
                            </Grid>
                        </Grid>
                    );
                }}
            />
            <GoogleMaps
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXEAVhGgUQa0QGg_gNr4wnMf28jzrT5xI&libraries=geometry,drawing,places"
                loadingElement={<div style={{height: '300px'}}/>}
                containerElement={<div style={{height: `300px`}}/>}
                mapElement={<div style={{height: `300px`}}/>}
                point={formValue.coordinates}
            />
            <Button style={{display: "block", marginLeft: "auto", marginTop: "1rem"}} type="submit">create</Button>
        </form>
    );
}
