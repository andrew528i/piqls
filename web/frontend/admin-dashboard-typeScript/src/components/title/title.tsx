import React, {FC, PropsWithChildren} from 'react';
import Typography from '@material-ui/core/Typography';

const Title: FC = (props: PropsWithChildren<object>) => {
    return (
        <Typography component="h2" variant="h6" color="primary" gutterBottom>
            {props.children}
        </Typography>
    );
}

export default Title
