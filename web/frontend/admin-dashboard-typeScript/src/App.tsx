import React, {FC, useEffect} from 'react';
import SignIn from './pages/sign-in/sign-in';
import {Switch, Route, Redirect} from 'react-router-dom'
import {useDispatch, useSelector} from "react-redux";
import Layout from "./components/layout/layout";
import Main from "./pages/main/main";
import {userDetailAction} from "./redux/user/user-action";
import MyAccount from "./pages/my-account/my-account";
import Orders from "./pages/orders/orders";
import OrderDetails from "./pages/order-details/order-details";
import Page500 from "./pages/page-500/page-500";
import Map from "./pages/map/map";
import Tariffs from "./pages/tariffs/tariffs";
import {RootState} from "./redux";

const Authorization = () => (
    <Switch>
        <Route path="/sign-in" component={SignIn}/>
        <Redirect to="/sign-in"/>
    </Switch>
)

const App: FC = () => {
    const dispatch = useDispatch()
    const {csrf: token, errorStatusCode} = useSelector(
        ({user: {csrf}, error: {errorStatusCode}}: RootState) => ({csrf, errorStatusCode})
)
    const tokenStorage: boolean = !!localStorage.getItem('X-CSRFToken')

    useEffect(() => {
        if (tokenStorage && !token) {
            dispatch(userDetailAction())
        }
    }, [dispatch, tokenStorage, token])

    if (!tokenStorage) {
        return <Authorization/>
    }

    if (errorStatusCode === 500) {
        return <Page500/>
    }

    return (
        <Layout>
            <Switch>
                <Route path={'/tariffs'} component={Tariffs}/>
                <Route path={'/map'} component={Map}/>
                <Route exact path="/order/details/:id" component={OrderDetails}/>
                <Route exact path="/orders" component={Orders}/>
                <Route exact path="/my-account" component={MyAccount}/>
                <Route exact path="/" component={Main}/>
                <Redirect to="/"/>
            </Switch>
        </Layout>
    );
}

export default App;
