class Api {
    defaultUrl = 'http://173.249.45.177:8000'

    httpHeaders: any
    headers: any

    constructor() {
        this.httpHeaders = {'Content-Type': 'application/json', 'X-CSRFToken': localStorage.getItem('X-CSRFToken')}
        this.headers = new Headers(this.httpHeaders)
    }

    async signIn(data: object): Promise<any> {
        const request = new Request(
            this.defaultUrl + '/user/sign_in',
            {
                method: 'POST',
                credentials: 'include',
                headers: this.headers,
                body: JSON.stringify(data)
            }
        )
        return await fetch(request)
    }

    async methodGet(url: string | undefined) {
        const request = new Request(
            this.defaultUrl + url,
            {
                method: 'GET',
                credentials: 'include',
                headers: this.headers
            }
        )
        return await fetch(request)
    }

    async methodPOST(url = '', data?: object) {
        const request = new Request(
            this.defaultUrl + url,
            {
                method: 'POST',
                credentials: 'include',
                headers: this.headers,
                body: JSON.stringify(data)
            }
        )
        return await fetch(request)
    }
}


export default Api
